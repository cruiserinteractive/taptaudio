# swig.m4
# SWIG GNU Autoconf Macros
# SWIG http://www.swig.org
# Brian Shire <shire@tekrat.com>, Tekrat Labs <http://www.tekrat.com> 
# Some tests are derived from original SWIG configure.in file
# This file is distributed under the terms of the GPL License

# SWIG...
# SWIG_PROG(<if-found>, <if-not-found>)
#  Substitutes: @SWIG@
#-----------------------------------------------------------------------------
AC_DEFUN(SWIG_PROG, [
	# vars
	SWIG=
	SWIG_FAIL=

	# --with-swig=PATH
	AC_ARG_WITH(swig,
		[  --with-swig=PATH             Specify path to swig binary],
		[SWIG="$withval"],
		[AC_PATH_PROGS(SWIG, swig, , $PATH)]
	)

	if test -z $SWIG; then
		AC_MSG_WARN(Could not find swig)
		SWIG_FAIL="Could not find swig in PATH"	
	fi

	# Substitute vars
	AC_SUBST(SWIG)

	# Execute the given success/failure script	
	AS_IF([test -z "$SWIG_FAIL"], [$1], [$2])	
	
]) # SWIG()


# SWIG_GUILE(<if-found>, <if-not-found>)
# Substitutes: 
#  Standard:
#   SWIG_GUILE_FAIL - Reason for failure, or nothing if successfull
#   SWIG_GUILE_PROG - Path to Guile interpreter (if one exists)
#   SWIG_GUILE_CONFIG - Path to guile-config
#   SWIG_GUILE_CPPFLAGS - Includes -I flags
#   SWIG_GUILE_LFLAGS - Linker Flags for libraries 
#   SWIG_GUILE_PREFIX - Guile directory prefix
#   SWIG_GUILE_CFLAGS - C compiler flags
#   SWIG_GUILE_MAJOR - Guile major version x.0.0
#   SWIG_GUILE_MINOR - Guile minor version 0.x.0
#   SWIG_GUILE_MICRO - Guile micro version 0.0.x
#   swigguiledir - Automake, install directory, same as $SWIG_GUILE_EXTDIR 
#  Additional:
#   SWIG_GUILE_PKGINCLUDEDIR - 
#   SWIG_GUILE_PKGLIBDIR - 
#   SWIG_GUILE_PKGDATADIR - 
#   SWIG_GUILE_INLCUDEDIR - 
#   SWIG_GUILE_MANDIR - 
#   SWIG_GUILE_INFODIR - 
#   SWIG_GUILE_LIBDIR - 
#   SWIG_GUILE_LOCALSTATEDIR - 
#   SWIG_GUILE_SHAREDSTATEDIR - 
#   SWIG_GUILE_SYSCONFDIR - 
#   SWIG_GUILE_DATADIR - 
#   SWIG_GUILE_LIBEXECDIR - 
#   SWIG_GUILE_SBINDIR - 
#   SWIG_GUILE_BIDIR - 
#   SWIG_GUILE_EXECPREFIX - 
#   SWIG_GUILE_TOPSRCDIR - 
#   SWIG_GUILE_SRCDIR - 
#-----------------------------------------------------------------------------
AC_DEFUN(SWIG_GUILE, [

	SWIG_GUILE_FAIL=
	SWIG_GUILE_PROG=
	SWIG_GUILE_CONFIG=
	SWIG_GUILE_CPPFLAGS=
	SWIG_GUILE_LFLAGS=
	SWIG_GUILE_PREFIX=
	SWIG_GUILE_CFLAGS=
	SWIG_GUILE_MAJOR=
	SWIG_GUILE_MINOR=
	SWIG_GUILE_MICRO=

	SWIG_GUILE_PKGINCLUDEDIR=
	SWIG_GUILE_PKGLIBDIR=
	SWIG_GUILE_PKGDATADIR=
	SWIG_GUILE_INLCUDEDIR=
	SWIG_GUILE_MANDIR=
	SWIG_GUILE_INFODIR=
	SWIG_GUILE_LIBDIR=
	SWIG_GUILE_LOCALSTATEDIR=
	SWIG_GUILE_SHAREDSTATEDIR=
	SWIG_GUILE_SYSCONFDIR=
	SWIG_GUILE_DATADIR=
	SWIG_GUILE_LIBEXECDIR=
	SWIG_GUILE_SBINDIR=
	SWIG_GUILE_BIDIR=
	SWIG_GUILE_EXECPREFIX=
	SWIG_GUILE_TOPSRCDIR=
	SWIG_GUILE_SRCDIR=

	# --with-guile-config=PATH
	AC_ARG_WITH(guile-config,
		[  --with-guile-config=PATH             Specify path to guile-config],
		[SWIG_GUILE_CONFIG="$withval"],
		[AC_PATH_PROGS(SWIG_GUILE_CONFIG, guile-config, , $PATH)]
	)
	# --with-guile=PATH
	AC_ARG_WITH(guile,
		[  --with-guile=PATH             Specify path to Guile interpretor],
		[SWIG_GUILE_PROG="$withval"],
		[AC_PATH_PROGS(SWIG_GUILE_PROG, guile, , $PATH)]
	)
	# --with-guile-includes=INCLUDES
	AC_ARG_WITH(guile-includes,
		[  --with-guile-includes=PATH    Specify path to Guile inlcude files],
		[SWIG_GUILE_CPPFLAGS="$withval"], [SWIG_GUILE_CPPFLAGS=]
	)
	# --with-guile-lflags=FLAGS
	AC_ARG_WITH(guile-lflags,
		[  --with-guile-lflags=FLAGS    Specify Guile linker flags],
		[SWIG_GUILE_LFLAGS="$withval"], [SWIG_GUILE_LFLAGS=]
	)
	# --with-guile-prefix=PATH
	AC_ARG_WITH(guile-prefix,
		[  --with-guile-prefix           Specify Guile directory prefix],
		[SWIG_GUILE_PREFIX="$withval"], [SWIG_GUILE_PREFIX=]
	)

	if test -z "$SWIG_GUILE_CONFIG"; then
		SWIG_GUILE_FAIL="$SWIG_GUILE_FAIL\nCould not find guile-config in $PATH"
	else
		# Check for Guile prefix 
		AC_MSG_CHECKING(for Guile prefix)
		if test -z "$SWIG_GUILE_PREFIX"; then
			SWIG_GUILE_PREFIX=`$SWIG_GUILE_CONFIG info prefix`
		fi
		AC_MSG_RESULT($SWIG_GUILE_PREFIX)

		# Retrieve the version
		AC_MSG_CHECKING(for Guile version)
		SWIG_GUILE_MAJOR=[`$SWIG_GUILE_CONFIG --version 2>&1 | sed 's/.*\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\1/'`]
		SWIG_GUILE_MINOR=[`$SWIG_GUILE_CONFIG --version 2>&1 | sed 's/.*\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\2/'`]
		SWIG_GUILE_MICRO=[`$SWIG_GUILE_CONFIG --version 2>&1 | sed 's/.*\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`]
		AC_MSG_RESULT($SWIG_GUILE_MAJOR.$SWIG_GUILE_MINOR.$SWIG_GUILE_MICRO)

		# Check for Include paths
		AC_MSG_CHECKING(for Guile includes)
		if test -z "$SWIG_GUILE_CPPFLAGS"; then
			SWIG_GUILE_CPPFLAGS=`$SWIG_GUILE_CONFIG compile`
		fi
		AC_MSG_RESULT($SWIG_GUILE_CPPFLAGS)	

		# Check Headers (GUILE.h)
		guile_tmp=$CPPFLAGS
		CPPFLAGS="$CPPFLAGS $SWIG_GUILE_CPPFLAGS"
		AC_CHECK_HEADER(guile/gh.h, [], 
			[
				AC_MSG_WARN(Could not find GUILE header gh.h.)	
				SWIG_GUILE_FAIL="$SWIG_GUILE_FAIL\nCould not find Guile headers (guile/gh.h) using $SWIG_GUILE_CPPFLAGS"
			]
		)
		CPPFLAGS=$guile_tmp

		# Check for Library flags
		AC_MSG_CHECKING(for Guile linker flags)
		if test -z "$SWIG_GUILE_LFLAGS"; then
			SWIG_GUILE_LFLAGS=`$SWIG_GUILE_CONFIG link`
		fi
		AC_MSG_RESULT($SWIG_GUILE_LFLAGS)
	
		# Setup the swigguiledir for use with Automake
		AC_MSG_CHECKING(for Automake install dir)
		# I'm not sure if this is correct?
		swigguiledir=`$SWIG_GUILE_CONFIG info libexecdir`
		if test -r $swigguiledir; then
			AC_MSG_RESULT($swigguiledir)
		else
			AC_MSG_RESULT(Could not find $swigguiledir.)
			swigguiledir=
		fi

		# Setup the additional variables from guile-config
		AC_MSG_CHECKING(for additional Guile variables (this may take a while))
		SWIG_GUILE_PKGINCLUDEDIR=`$SWIG_GUILE_CONFIG info pkgincludedir`
		SWIG_GUILE_PKGLIBDIR=`$SWIG_GUILE_CONFIG info pkglibdir`
		SWIG_GUILE_PKGDATADIR=`$SWIG_GUILE_CONFIG info pkgdatadir`
		SWIG_GUILE_INCLUDEDIR=`$SWIG_GUILE_CONFIG info includedir`
		SWIG_GUILE_MANDIR=`$SWIG_GUILE_CONFIG info mandir`
		SWIG_GUILE_INFODIR=`$SWIG_GUILE_CONFIG info infodir`
		SWIG_GUILE_LIBDIR=`$SWIG_GUILE_CONFIG info libdir`
		SWIG_GUILE_LOCALSTATEDIR=`$SWIG_GUILE_CONFIG info localstatedir`
		SWIG_GUILE_SHAREDSTATEDIR=`$SWIG_GUILE_CONFIG info sharedstatedir`
		SWIG_GUILE_SYSCONFDIR=`$SWIG_GUILE_CONFIG info sysconfdir`
		SWIG_GUILE_DATADIR=`$SWIG_GUILE_CONFIG info datadir`
		SWIG_GUILE_LIBEXECDIR=`$SWIG_GUILE_CONFIG info libexecdir`
		SWIG_GUILE_SBINDIR=`$SWIG_GUILE_CONFIG info sbindir`
		SWIG_GUILE_BIDIR=`$SWIG_GUILE_CONFIG info bindir`
		SWIG_GUILE_EXECPREFIX=`$SWIG_GUILE_CONFIG info exec_prefix`
		SWIG_GUILE_TOPSRCDIR=`$SWIG_GUILE_CONFIG info top_srcdir`
		SWIG_GUILE_SRCDIR=`$SWIG_GUILE_CONFIG info srcdir`
		AC_MSG_RESULT(done)
			
	fi 

	AC_SUBST(SWIG_GUILE_PROG)
	AC_SUBST(SWIG_GUILE_CPPFLAGS)
	AC_SUBST(SWIG_GUILE_LIBPATH)
	AC_SUBST(SWIG_GUILE_PREFIX)
	AC_SUBST(SWIG_GUILE_EXTDIR)
	AC_SUBST(SWIG_GUILE_CFLAGS)
	AC_SUBST(SWIG_GUILE_MAJOR)
	AC_SUBST(SWIG_GUILE_MINOR)
	AC_SUBST(SWIG_GUILE_MICRO)
	AC_SUBST(swigguiledir)
	AC_SUBST(SWIG_GUILE_PKGINCLUDEDIR)
	AC_SUBST(SWIG_GUILE_PKGLIBDIR)
	AC_SUBST(SWIG_GUILE_PKGDATADIR)
	AC_SUBST(SWIG_GUILE_INCLUDEDIR)
	AC_SUBST(SWIG_GUILE_MANDIR)
	AC_SUBST(SWIG_GUILE_INFODIR)
	AC_SUBST(SWIG_GUILE_LIBDIR)
	AC_SUBST(SWIG_GUILE_LOCALSTATEDIR)
	AC_SUBST(SWIG_GUILE_SHAREDSTATEDIR)
	AC_SUBST(SWIG_GUILE_SYSCONFDIR)
	AC_SUBST(SWIG_GUILE_DATADIR)
	AC_SUBST(SWIG_GUILE_LIBEXECDIR)
	AC_SUBST(SWIG_GUILE_SBINDIR)
	AC_SUBST(SWIG_GUILE_BINDIR)
	AC_SUBST(SWIG_GUILE_EXECPREFIX)
	AC_SUBST(SWIG_GUILE_TOPSRCDIR)
	AC_SUBST(SWIG_GUILE_SRCDIR)

	# Execute the given success/failure script	
	AS_IF([test -z "$SWIG_GUILE_FAIL"], [$1], [$2])	

]) # SWIG_GUILE


# SWIG_JAVA(<if-found>, <if-not-found>)
# Substitutes: 
#  Standard:
#   SWIG_JAVA_FAIL - Reason for failure, or nothing if successfull
#   SWIG_JAVA_PROG - Path to Java interpreter (java)
#   SWIG_JAVA_CPPFLAGS - Includes -I flags
#   SWIG_JAVA_LIBPATH - Path to library directory 
#   SWIG_JAVA_PREFIX - Use SWIG_JAVA_HOME instead 
#   SWIG_JAVA_CFLAGS - C compiler flags
#   SWIG_JAVA_MAJOR - Java major version x.0.0
#   SWIG_JAVA_MINOR - Java minor version 0.x.0
#   SWIG_JAVA_MICRO - Java micro version 0.0.x
#   swigjavadir - Automake, install directory, same as $SWIG_JAVA_PREFIX/lib-dynload
#  Additional:
#   SWIG_JAVA_COMPILER - Path to Java compiler (javac)
#	  SWIG_JAVA_VENDOR - System.getProperty("java.vendor")
#	  SWIG_JAVA_HOME -  System.getProperty("java.home")
#	  SWIG_JAVA_VM_SPECIFICATION_VERSION -  System.getProperty("java.vm.specification.versien")
#	  SWIG_JAVA_VM_SPECIFICATION_VENDOR -  System.getProperty("java.vm.specification.vendor")
#	  SWIG_JAVA_VM_SPECIFICATION_NAME -  System.getProperty("java.vm.specification.name")
#	  SWIG_JAVA_VM_VERSION -  System.getProperty("java.vm.version")
#	  SWIG_JAVA_VM_VENDOR -  System.getProperty("java.vm.vendor")
#	  SWIG_JAVA_VM_NAME -  System.getProperty("java.vm.name")
#	  SWIG_JAVA_SPECIFICATION_VERSION -  System.getProperty("java.specification.version")
#	  SWIG_JAVA_SPECIFICATION_VENDOR -  System.getProperty("java.specification.vendor")
#	  SWIG_JAVA_SPECIFICATION_NAME -  System.getProperty("java.specification.name")
#	  SWIG_JAVA_CLASS_VERSION -  System.getProperty("java.class.version")
#	  SWIG_JAVA_CLASS_PATH -  System.getProperty("java.class.path")
#	  SWIG_JAVA_LIBRARY_PATH -  System.getProperty("java.library.path")
#	  SWIG_EXT_DIRS -  System.getProperty("java.ext.dirs")
#-----------------------------------------------------------------------------
AC_DEFUN(SWIG_JAVA, [

	SWIG_JAVA_PROG=
	SWIG_JAVA_COMPILER=
	SWIG_JAVA_CPPFLAGS=
	SWIG_JAVA_LIBPATH=
	SWIG_JAVA_PREFIX=
	SWIG_JAVA_EXECPREFIX=
	SWIG_JAVA_CFLAGS=
	SWIG_JAVA_MAJOR=
	SWIG_JAVA_MINOR=
	SWIG_JAVA_MICRO=
	# Variables from the System Properties Class
	SWIG_JAVA_VENDOR=
	SWIG_JAVA_HOME=
	SWIG_JAVA_VM_SPECIFICATION_VERSION=
	SWIG_JAVA_VM_SPECIFICATION_VENDOR=
	SWIG_JAVA_VM_SPECIFICATION_NAME=
	SWIG_JAVA_VM_VERSION=
	SWIG_JAVA_VM_VENDOR=
	SWIG_JAVA_VM_NAME=
	SWIG_JAVA_SPECIFICATION_VERSION=
	SWIG_JAVA_SPECIFICATION_VENDOR=
	SWIG_JAVA_SPECIFICATION_NAME=
	SWIG_JAVA_CLASS_VERSION=
	SWIG_JAVA_CLASS_PATH=
	SWIG_JAVA_LIBRARY_PATH=
	SWIG_EXT_DIRS=

	# --with-java=PATH
	AC_ARG_WITH(java,
		[  --with-java=PATH             Specify path to Java interpretor],
		[SWIG_JAVA_PROG="$withval"],
		[AC_PATH_PROGS(SWIG_JAVA_PROG, java kaffe guavac, , $PATH)] 
	)
	# --with-javac=PATH
	AC_ARG_WITH(javac,
		[  --with-javac=PATH            Specify path to Java Compiler],
		[SWIG_JAVA_COMPILER="$withval"],
		[AC_PATH_PROGS(SWIG_JAVA_COMPILER, javac, , $PATH)]
	)
	# --with-java-includes=INCLUDES
	AC_ARG_WITH(java-includes,
		[  --with-java-includes=PATH    Specify path to Java inlcude files],
		[SWIG_JAVA_CPPFLAGS="$withval"], [SWIG_JAVA_CPPFLAGS=]
	)
	# --with-java-libpath=LIBPATH
	AC_ARG_WITH(java-libpath,
		[  --with-java-libpath=PATH     Specify path to Java libraries],
		[SWIG_JAVA_LIBPATH="$withval"], [SWIG_JAVA_LIBPATH=]
	)
	# --with-java-prefix=PATH
	AC_ARG_WITH(java-prefix,
		[  --with-java-prefix           Specify Java directory prefix],
		[SWIG_JAVA_PREFIX="$withval"], [SWIG_JAVA_PREFIX=]
	)
	# --with-java-execprefix=PATH
	AC_ARG_WITH(java-execprefix,
		[  --with-java-execprefix=PATH Specify Java binary directory prefix],
		[SWIG_JAVA_EXECPREFIX="$withval"], [SWIG_JAVA_EXECPREFIX=]
	)

	if test -z "$SWIG_JAVA_PROG" && test -z "$SWIG_JAVA_COMPILER"; then
		SWIG_JAVA_FAIL="$SWIG_JAVA_FAIL\nCould not find Java interpreter/compiler in $PATH"
	else
		# Display found interpreter/compiler
		AC_MSG_CHECKING(for Java Interpreter)
		AC_MSG_RESULT($SWIG_JAVA_PROG)
		AC_MSG_CHECKING(for Java Compiler)
		AC_MSG_RESULT($SWIG_JAVA_COMPILER)

		# Write, compile, and run java program to retrieve system info...
		AC_MSG_NOTICE(Compiling Java class to retrieve system properties...)
#### BEGIN JAVA CODE ####
changequote(|,|)
cat << X > SWIG_JAVA_TMP.java
// SWIG_JAVA_TMP.java
// Auto-generated Java code used by swig.m4 (the SWIG Autoconf Macros)
// http://www.swig.org
//
// This code was probably generated via a 'configure' script, and can be safely removed.
//
// This program is used to retrieve property values from the java.lang.System.getProperties call

public class SWIG_JAVA_TMP {
	public static void main(String args[]) {
		if(args.length == 1) {
			System.out.println(System.getProperty(args[0]));
		} else {
			System.out.println("This program was built for and is used by the SWIG Automake Macros http://www.swig.org");
		}
	}
} 
X
changequote([,])
#### END JAVA CODE ####
		# compile the code...
		$SWIG_JAVA_COMPILER SWIG_JAVA_TMP.java 

		# check for system properties...
		AC_MSG_NOTICE(for Java System Properties (may take a while))

		# Retrieve the version
		version=`$SWIG_JAVA_PROG SWIG_JAVA_TMP java.version`
		SWIG_JAVA_MAJOR=`echo $version | sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\).*/\1/'`
		SWIG_JAVA_MINOR=`echo $version | sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\).*/\2/'`
		SWIG_JAVA_MICRO=`echo $version | sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\).*/\3/'`
		AC_MSG_NOTICE(Version: $SWIG_JAVA_MAJOR.$SWIG_JAVA_MINOR.$SWIG_JAVA_MICRO)

		# Variables from the System Properties Class
		SWIG_JAVA_VENDOR=`$SWIG_JAVA_PROG SWIG_JAVA_TMP java.vendor`
		AC_MSG_NOTICE(java.vendor: $SWIG_JAVA_VENDOR)
		SWIG_JAVA_HOME=`$SWIG_JAVA_PROG SWIG_JAVA_TMP java.home`
		AC_MSG_NOTICE(java.home: $SWIG_JAVA_HOME)
		SWIG_JAVA_VM_SPECIFICATION_VERSION=`$SWIG_JAVA_PROG SWIG_JAVA_TMP java.vm.specification.version`
		AC_MSG_NOTICE(java.vm.specification.version: $SWIG_JAVA_VM_SPECIFICATION_VERSION)
		SWIG_JAVA_VM_SPECIFICATION_VENDOR=`$SWIG_JAVA_PROG SWIG_JAVA_TMP java.vm.specification.vendor`
		AC_MSG_NOTICE(java.vm.specification.vendor: $SWIG_JAVA_VM_SPECIFICATION_VENDOR)
		SWIG_JAVA_VM_SPECIFICATION_NAME=`$SWIG_JAVA_PROG SWIG_JAVA_TMP java.vm.specification.name`
		AC_MSG_NOTICE(java.vm.specification.name: $SWIG_JAVA_VM_SPECIFICATION_NAME)
		SWIG_JAVA_VM_VERSION=`$SWIG_JAVA_PROG SWIG_JAVA_TMP java.vm.version`
		AC_MSG_NOTICE(java.vm.version: $SWIG_JAVA_VM_VERSION)
		SWIG_JAVA_VM_VENDOR=`$SWIG_JAVA_PROG SWIG_JAVA_TMP java.vm.vendor`
		AC_MSG_NOTICE(java.vm.vendor: $SWIG_JAVA_VM_VENDOR)
		SWIG_JAVA_VM_NAME=`$SWIG_JAVA_PROG SWIG_JAVA_TMP java.vm.name`
		AC_MSG_NOTICE(java.vm.name: $SWIG_JAVA_VM_NAME)
		SWIG_JAVA_SPECIFICATION_VERSION=`$SWIG_JAVA_PROG SWIG_JAVA_TMP java.specification.version`
		AC_MSG_NOTICE(java.specification.version: $SWIG_JAVA_SPECIFICATION_VERSION)
		SWIG_JAVA_SPECIFICATION_VENDOR=`$SWIG_JAVA_PROG SWIG_JAVA_TMP java.specification.vendor`
		AC_MSG_NOTICE(java.specification.vendor: $SWIG_JAVA_SPECIFICATION_VENDOR)
		SWIG_JAVA_SPECIFICATION_NAME=`$SWIG_JAVA_PROG SWIG_JAVA_TMP java.specification.name`
		AC_MSG_NOTICE(java.specification.name: $SWIG_JAVA_SPECIFICATION_NAME)
		SWIG_JAVA_CLASS_VERSION=`$SWIG_JAVA_PROG SWIG_JAVA_TMP java.class.version`
		AC_MSG_NOTICE(java.class.version: $SWIG_JAVA_CLASS_VERSION)
		SWIG_JAVA_CLASS_PATH=`$SWIG_JAVA_PROG SWIG_JAVA_TMP java.class.path`
		AC_MSG_NOTICE(java.class.path: $SWIG_JAVA_CLASS_PATH)
		SWIG_JAVA_LIBRARY_PATH=`$SWIG_JAVA_PROG SWIG_JAVA_TMP java.library.path`
		AC_MSG_NOTICE(java.library.path: $SWIG_JAVA_LIBRARY_PATH)
		SWIG_EXT_DIRS=`$SWIG_JAVA_PROG SWIG_JAVA_TMP java.ext.dirs`
		AC_MSG_NOTICE(java.ext.dirs: $SWIG_JAVA_EXT_DIRS)

    # set SWIG_JAVA_PREFIX
		SWIG_JAVA_PREFIX=$SWIG_JAVA_HOME
	
		# remove temporary java files	
		rm -f SWIG_JAVA_TMP.java SWIG_JAVA_TMP.class

		# Setup Include Paths
		AC_MSG_CHECKING(for Java Includes Paths)
		if test -z $SWIG_JAVA_CPPFLAGS; then
      # TODO fix the linux path below
			SWIG_JAVA_CPPFLAGS="-I$SWIG_JAVA_HOME/include -I$SWIG_JAVA_HOME/../include -I$SWIG_JAVA_HOME/../include/linux -I$SWIG_JAVA_HAME/../include/solaris"
		fi
		AC_MSG_RESULT($SWIG_JAVA_CPPFLAGS)

		# Check Headers (jni.h)
		java_tmp=$CPPFLAGS
		CPPFLAGS="$CPPFLAGS $SWIG_JAVA_CPPFLAGS"
		AC_CHECK_HEADER(jni.h, [], 
			[
				AC_MSG_WARN(Could not find Java header jni.h. $CPPFLAGS)
				SWIG_JAVA_FAIL="$SWIG_JAVA_FAIL\nCould not find Java headers (jni.h) using $SWIG_JAVA_CPPFLAGS"
			]
		)
		CPPFLAGS=$java_tmp

		# Setup the swigjavadir for use with Automake
		AC_MSG_CHECKING(for Automake install dir)
		for dir in $SWIG_JAVA_EXT_DIRS; do
			if test -z $swigjavadir && test -r $swigjavadir; then
				$swigjavadir=$dir
			fi
		done
		AC_MSG_RESULT($swigjavadir)
			
	fi 

	AC_SUBST(SWIG_JAVA_PROG)
	AC_SUBST(SWIG_JAVA_CPPFLAGS)
	AC_SUBST(SWIG_JAVA_LIBPATH)
	AC_SUBST(SWIG_JAVA_PREFIX)
	AC_SUBST(SWIG_JAVA_EXECPREFIX)
	AC_SUBST(SWIG_JAVA_CFLAGS)
	AC_SUBST(SWIG_JAVA_MAJOR)
	AC_SUBST(SWIG_JAVA_MINOR)
	AC_SUBST(SWIG_JAVA_MICRO)
	AC_SUBST(swigjavadir)
	AC_SUBST(SWIG_JAVA_VENDOR)
	AC_SUBST(SWIG_JAVA_HOME)
	AC_SUBST(SWIG_JAVA_VM_SPECIFICATION_VERSION)
	AC_SUBST(SWIG_JAVA_VM_SPECIFICATION_VENDOR)
	AC_SUBST(SWIG_JAVA_VM_SPECIFICATION_NAME)
	AC_SUBST(SWIG_JAVA_VM_VERSION)
	AC_SUBST(SWIG_JAVA_VM_VENDOR)
	AC_SUBST(SWIG_JAVA_VM_NAME)
	AC_SUBST(SWIG_JAVA_SPECIFICATION_VERSION)
	AC_SUBST(SWIG_JAVA_SPECIFICATION_VENDOR)
	AC_SUBST(SWIG_JAVA_SPECIFICATION_NAME)
	AC_SUBST(SWIG_JAVA_CLASS_VERSION)
	AC_SUBST(SWIG_JAVA_CLASS_PATH)
	AC_SUBST(SWIG_JAVA_LIBRARY_PATH)
	AC_SUBST(SWIG_EXT_DIRS)
	

	# Execute the given success/failure script	
	AS_IF([test -z "$SWIG_JAVA_FAIL"], [$1], [$2])	

]) # SWIG_JAVA


# SWIG_OCAML(<if-found>, <if-not-found>)
# Substitutes: 
#  Standard:
#   SWIG_OCAML_FAIL - Reason for failure, or nothing if successfull
#   SWIG_OCAML_PROG - Path to Ocaml interpreter 
#   SWIG_OCAML_CPPFLAGS - Includes -I flags
##   SWIG_OCAML_LIBPATH - Path to library directory 
##   SWIG_OCAML_PREFIX - Ocaml directory prefix
##   SWIG_OCAML_CFLAGS - C compiler flags
#   SWIG_OCAML_MAJOR - Ocaml major version x.0.0
#   SWIG_OCAML_MINOR - Ocaml minor version 0.x.0
#   SWIG_OCAML_MICRO - Ocaml micro version 0.0.x
##   swigocamldir - Automake, install directory, same as $SWIG_OCAML_PREFIX/lib-dynload
#  Additional:
#	SWIG_OCAML_COMPILER - Path to Ocaml compiler
#	SWIG_OCAML_DLGEN - Path to ocamldlgen, DL load generator
#	SWIG_OCAML_FIND - Path to ocamlfind, package tool
#	SWIG_OCAML_MKTOP - Path to ocamlmktop, toplevel creator
#-----------------------------------------------------------------------------
AC_DEFUN(SWIG_OCAML, [

	SWIG_OCAML_PROG=
	SWIG_OCAML_CPPFLAGS=
	SWIG_OCAML_LIBPATH=
	SWIG_OCAML_PREFIX=
	SWIG_OCAML_EXECPREFIX=
	SWIG_OCAML_CFLAGS=
	SWIG_OCAML_MAJOR=
	SWIG_OCAML_MINOR=
	SWIG_OCAML_MICRO=
	SWIG_OCAML_COMPILER=
	SWIG_OCAML_DLGEN=
	SWIG_OCAML_FIND=
	SWIG_OCAML_MKTOP=

	# --with-ocaml=PATH
	AC_ARG_WITH(ocaml,
		[  --with-ocaml=PATH             Specify path to Ocaml interpretor],
		[SWIG_OCAML_PROG="$withval"],
		[AC_PATH_PROGS(SWIG_OCAML_PROG, ocaml, , $PATH)]
	)
	# --with-ocamlc=PATH
	AC_ARG_WITH(ocamlc,
		[  --with-ocamlc=PATH             Specify path to Ocaml compiler],
		[SWIG_OCAML_COMPILER="$withval"],
		[AC_PATH_PROGS(SWIG_OCAML_COMPILER, ocamlc, , $PATH)]
	)
	# --with-ocaml-includes=INCLUDES
	AC_ARG_WITH(ocaml-includes,
		[  --with-ocaml-includes=PATH    Specify path to Ocaml inlcude files],
		[SWIG_OCAML_CPPFLAGS="$withval"], [SWIG_OCAML_CPPFLAGS=]
	)
	# --with-ocaml-libpath=LIBPATH
	AC_ARG_WITH(ocaml-libpath,
		[  --with-ocaml-libpath=PATH     Specify path to Ocaml libraries],
		[SWIG_OCAML_LIBPATH="$withval"], [SWIG_OCAML_LIBPATH=]
	)
	# --with-ocaml-prefix=PATH
	AC_ARG_WITH(ocaml-prefix,
		[  --with-ocaml-prefix           Specify Ocaml directory prefix],
		[SWIG_OCAML_PREFIX="$withval"], [SWIG_OCAML_PREFIX=]
	)
	# --with-ocamldlgen=PATH
	AC_ARG_WITH(ocamldlgen,
		[  --with-ocmaldlgen,           Specify path to ocamldlgen],
		[SWIG_OCAML_DLGEN="$withval"], [SWIG_OCAMLFIND=]
	)
	# --with-ocamlfind=PATH
	AC_ARG_WITH(ocamlfind,
		[  --with-ocamlfind,             Specify path to ocamlfind],
		[SWIG_OCAML_FIND="$withval"], [SWIG_OCAML_FIND=]
	)
	# --with-ocamlmktop=PATH
	AC_ARG_WITH(ocamlmktop,
		[  --with-ocamlmktop,            Specify path to ocamlmktop],
		[SWIG_OCAML_MKTOP="$withval"], [SWIG_OCAML_MKTOP=]
	)

	if test -z "$SWIG_OCAML_PROG"; then
		SWIG_OCAML_FAIL="$SWIG_OCAML_FAIL\nCould not find Ocaml interpreter in $PATH"
	else
		# Retrieve the version
		AC_MSG_CHECKING(for Ocaml version)
		SWIG_OCAML_MAJOR=`$SWIG_OCAML_COMPILER -v | sed 's/.*\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\1/'`
		SWIG_OCAML_MINOR=`$SWIG_OCAML_COMPILER -v | sed 's/.*\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\2/'`
		SWIG_OCAML_MICRO=`$SWIG_OCAML_COMPILER -v | sed 's/.*\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`
		AC_MSG_RESULT($SWIG_OCAML_MAJOR.$SWIG_OCAML_MINOR.$SWIG_OCAML_MICRO)

		# Check for Include paths
		AC_MSG_CHECKING(for ocaml includes)
		dirs="/usr/lib/ocaml/caml /usr/local/lib/ocaml/caml"
		for d in $dirs; do
			if test -r $d/mlvalues.h; then
				SWIG_OCAML_CPPFLAGS="-I$d"
			fi
		done
		AC_MSG_RESULT($SWIG_OCAML_CPPFLAGS)

		# Setup the swigocamldir for use with Automake
		#AC_MSG_CHECKING(for Automake install dir)
		#swigocamldir="$SWIG_OCAML_PREFIX/lib/ocaml$ocaml_version/lib-dynload"	
		#if test -r $swigocamldir; then
		#	AC_MSG_RESULT($swigocamldir)
		#else
		#	AC_MSG_RESULT(Could not find $swigocamldir.)
		#	swigocamldir=
		#fi
			
	fi 

	AC_SUBST(SWIG_OCAML_PROG)
	AC_SUBST(SWIG_OCAML_CPPFLAGS)
	AC_SUBST(SWIG_OCAML_LIBPATH)
	AC_SUBST(SWIG_OCAML_PREFIX)
	AC_SUBST(SWIG_OCAML_EXECPREFIX)
	AC_SUBST(SWIG_OCAML_CFLAGS)
	AC_SUBST(SWIG_OCAML_MAJOR)
	AC_SUBST(SWIG_OCAML_MINOR)
	AC_SUBST(SWIG_OCAML_MICRO)
	AC_SUBST(swigocamldir)
	AC_SUBST(SWIG_OCAML_COMPILER)
	AC_SUBST(SWIG_OCAML_DLGEN)
	AC_SUBST(SWIG_OCAML_FIND)
	AC_SUBST(SWIG_OCAML_MKTOP)

	# Execute the given success/failure script	
	AS_IF([test -z "$SWIG_OCAML_FAIL"], [$1], [$2])	

]) # SWIG_OCAML



# SWIG_PERL(<if-found>, <if-not-found>)
# Substitutes: 
#  Standard:
#   SWIG_PERL_FAIL - Reason for failure, or nothing if successfull
#   SWIG_PERL_PROG - Path to Perl interpreter 
#   SWIG_PERL_CPPFLAGS - Includes -I flags
##   SWIG_PERL_LIBPATH - Path to library directory 
#	SWIG_PER_LFLAGS - Linker flags
#   SWIG_PERL_PREFIX - Perl directory prefix
#   SWIG_PERL_CFLAGS - C compiler flags
#   SWIG_PERL_MAJOR - Perl major version x.0.0
#   SWIG_PERL_MINOR - Perl minor version 0.x.0
#   SWIG_PERL_MICRO - Perl micro version 0.0.x
#   swigperldir - Automake, install directory, same as $SWIG_PERL_PREFIX/lib-dynload
#  Additional:
#	SWIG_PERL_PRIVLIB
#	SWIG_PERL_ARCHLIB
#	SWIG_PERL_SITELIB
#	SWIG_PERL_SITEARCH
#-----------------------------------------------------------------------------
AC_DEFUN(SWIG_PERL, [

	SWIG_PERL_PROG=
	SWIG_PERL_CPPFLAGS=
	SWIG_PERL_LFLAGS=
	SWIG_PERL_PREFIX=
	SWIG_PERL_CFLAGS=
	SWIG_PERL_MAJOR=
	SWIG_PERL_MINOR=
	SWIG_PERL_MICRO=
	swigperldir=
	SWIG_PERL_PRIVLIB=
	SWIG_PERL_ARCHLIB=
	SWIG_PERL_SITELIB=
	SWIG_PERL_SITEARCH=

	# --with-perl=PATH
	AC_ARG_WITH(perl,
		[  --with-perl=PATH             Specify path to Perl interpretor],
		[SWIG_PERL_PROG="$withval"],
		[AC_PATH_PROGS(SWIG_PERL_PROG, perl perl5.6.1 perl5.6.0 perl5.004 perl5.003 perl5.002 perl5.001 perl5, , $PATH)]
	)
	# --with-perl-includes=INCLUDES
	AC_ARG_WITH(perl-includes,
		[  --with-perl-includes=PATH    Specify path to Perl inlcude files],
		[SWIG_PERL_CPPFLAGS="$withval"], [SWIG_PERL_CPPFLAGS=]
	)
	# --with-perl-lflags=LFLAGS
	AC_ARG_WITH(perl-lflags,
		[  --with-perl-lflags=PATH    Set Linker Flags],
		[SWIG_PERL_LFLAGS="$withval"], [SWIG_PERL_LFLAGS=]
	)
	# --with-perl-cflags=CFLAGS
	AC_ARG_WITH(perl-cflags,
		[  --with-perl-cflags=PATH    Set Compiler Flags],
		[SWIG_PERL_CFLAGS="$withval"], [SWIG_PERL_CFLAGS=]
	)
	# --with-perl-prefix=PATH
	AC_ARG_WITH(perl-prefix,
		[  --with-perl-prefix           Specify Perl directory prefix],
		[SWIG_PERL_PREFIX="$withval"], [SWIG_PERL_PREFIX=]
	)

	if test -z "$SWIG_PERL_PROG"; then
		SWIG_PERL_FAIL="$SWIG_PERL_FAIL\nCould not find Perl interpreter in $PATH"
	else
		# Check for Perl prefix
		AC_MSG_CHECKING(for Perl prefix)
		if test -z "$SWIG_PERL_PREFIX"; then
			SWIG_PERL_PREFIX=`$SWIG_PERL_PROG -V:prefix | sed "s/.*='\(.*\)';/\1/" 2> /dev/null`
		fi
		AC_MSG_RESULT($SWIG_PERL_PREFIX)

		# Retrieve the version
		AC_MSG_CHECKING(for Perl version)
		ver=`$SWIG_PERL_PROG -e 'use Config; print $Config{version};' 2> /dev/null`
		SWIG_PERL_MAJOR=`echo $ver | sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\1/'`
		SWIG_PERL_MINOR=`echo $ver | sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\2/'`
		SWIG_PERL_MICRO=`echo $ver | sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`
		AC_MSG_RESULT($SWIG_PERL_MAJOR.$SWIG_PERL_MINOR.$SWIG_PERL_MICRO)

		# Get the lib paths...
		AC_MSG_CHECKING(for privlib)
		SWIG_PERL_PRIVLIB=`$SWIG_PERL_PROG -V:privlib | sed "s/.*='\(.*\)';/\1/" 2> /dev/null`
		AC_MSG_RESULT($SWIG_PERL_PRIVLIB)
		AC_MSG_CHECKING(for archlib)
		SWIG_PERL_ARCHLIB=`$SWIG_PERL_PROG -V:archlib | sed "s/.*='\(.*\)';/\1/" 2> /dev/null`
		AC_MSG_RESULT($SWIG_PERL_ARCHLIB)
		AC_MSG_CHECKING(for sitelib)
		SWIG_PERL_SITELIB=`$SWIG_PERL_PROG -V:sitelib | sed "s/.*='\(.*\)';/\1/" 2> /dev/null`
		AC_MSG_RESULT($SWIG_PERL_SITELIB)
		AC_MSG_CHECKING(for sitearch)
		SWIG_PERL_SITEARCH=`$SWIG_PERL_PROG -V:sitearch | sed "s/.*='\(.*\)';/\1/" 2> /dev/null`
		AC_MSG_RESULT($SWIG_PERL_SITEARCH)

		# Check for cppflags 
		AC_MSG_CHECKING(for perl cppflags)
		if test -z "$SWIG_PERL_CPPFLAGS"; then 
			SWIG_PERL_CPPFLAGS=`$SWIG_PERL_PROG -V:cppflags | sed "s/.*='\(.*\)';/\1/" 2> /dev/null`
			SWIG_PERL_CPPFLAGS="$SWIG_PERL_CPPFLAGS -I$SWIG_PERL_ARCHLIB/CORE"
		fi
		AC_MSG_RESULT($SWIG_PERL_CPPFLAGS)	

		# Check for the cflags
		AC_MSG_CHECKING(for cflags)
		if test -z "$SWIG_PERL_CFLAGS"; then
			SWIG_PERL_CFLAGS=`$SWIG_PERL_PROG -V:ccflags | sed "s/.*='\(.*\)';/\1/" 2> /dev/null`
		fi
		AC_MSG_RESULT($SWIG_PERL_CFLAGS)

		# Check for the lflags
		AC_MSG_CHECKING(for lflags)
		if test -z "$SWIG_PERL_LFLAGS"; then
			SWIG_PERL_LFLAGS=`$SWIG_PERL_PROG -V:ldflags | sed "s/.*='\(.*\)';/\1/" 2> /dev/null`
		fi
		AC_MSG_RESULT($SWIG_PERL_LFLAGS)

		# Check Headers (perl.h)
		perl_tmp=$CPPFLAGS
		CPPFLAGS="$CPPFLAGS $SWIG_PERL_CPPFLAGS"
		AC_CHECK_HEADER(perl.h, [], 
			[
				AC_MSG_WARN(Could not find Perl headers.)
				SWIG_PERL_FAIL="$SWIG_PERL_FAIL\nCould not find Perl headers (perl.h) using $SWIG_PERL_CPPFLAGS"
			]
		)
		CPPFLAGS=$perl_tmp
		
		# Setup the swigperldir for use with Automake
		AC_MSG_CHECKING(for Automake install dir)
		swigperldir="$SWIG_PERL_SITEARCH"	
		if test -r $swigperldir; then
			AC_MSG_RESULT($swigperldir)
		else
			AC_MSG_RESULT(Could not find $swigperldir.)
			swigperldir=
		fi
			
	fi 

	AC_SUBST(SWIG_PERL_PROG)
	AC_SUBST(SWIG_PERL_CPPFLAGS)
	AC_SUBST(SWIG_PERL_LFLAGS)
	AC_SUBST(SWIG_PERL_PREFIX)
	AC_SUBST(SWIG_PERL_CFLAGS)
	AC_SUBST(SWIG_PERL_MAJOR)
	AC_SUBST(SWIG_PERL_MINOR)
	AC_SUBST(SWIG_PERL_MICRO)
	AC_SUBST(swigperldir)
	AC_SUBST(SWIG_PERL_PRIVLIB)
	AC_SUBST(SWIG_PERL_ARCHLIB)
	AC_SUBST(SWIG_PERL_SITELIB)
	AC_SUBST(SWIG_PERL_SITEARCH)

	# Execute the given success/failure script	
	AS_IF([test -z "$SWIG_PERL_FAIL"], [$1], [$2])	

]) # SWIG_PERL



# SWIG_RUBY(<if-found>, <if-not-found>)
# Substitutes: 
#  Standard:
#   SWIG_RUBY_FAIL - Reason for failure, or nothing if successfull
#   SWIG_RUBY_PROG - Path to Ruby interpreter 
#   SWIG_RUBY_CPPFLAGS - Includes -I flags
#   SWIG_RUBY_LFLAGS - Linker Flags 
#   SWIG_RUBY_PREFIX - Ruby directory prefix
#   SWIG_RUBY_CFLAGS - C compiler flags
#   SWIG_RUBY_MAJOR - Ruby major version x.0.0
#   SWIG_RUBY_MINOR - Ruby minor version 0.x.0
#   SWIG_RUBY_MICRO - Ruby micro version 0.0.x
#   swigrubydir - Automake, install directory, same as $SWIG_RUBY_PREFIX/lib-dynload
#  Additional:
#	SWIG_RUBY_SITELIBDIR
#	SWIG_RUBY_BINDIR
#	SWIG_RUBY_DATADIR
#	SWIG_RUBY_ARCHDIR
#	SWIG_RUBY_SITEDIR
#	SWIG_RUBY_SHAREDSTATEDIR
#	SWIG_RUBY_LOCALSTATEDIR
#	SWIG_RUBY_LIBEXECDIR
#	SWIG_RUBY_COMPILEDIR
#	SWIG_RUBY_SBINDIR
#	SWIG_RUBY_EXECPREFIX
#	SWIG_RUBY_SYSCONFDIR
#	SWIG_RUBY_RUBYLIBDIR
#	SWIG_RUBY_SITEARCHDIR
#-----------------------------------------------------------------------------
AC_DEFUN(SWIG_RUBY, [

	SWIG_RUBY_PROG=
	SWIG_RUBY_CPPFLAGS=
	SWIG_RUBY_LFLAGS=
	SWIG_RUBY_PREFIX=
	SWIG_RUBY_CFLAGS=
	SWIG_RUBY_MAJOR=
	SWIG_RUBY_MINOR=
	SWIG_RUBY_MICRO=
	swigrubydir=
	SWIG_RUBY_SITELIBDIR=
	SWIG_RUBY_BINDIR=
	SWIG_RUBY_DATADIR=
	SWIG_RUBY_ARCHDIR=
	SWIG_RUBY_SITEDIR=
	SWIG_RUBY_SHAREDSTATEDIR=
	SWIG_RUBY_LOCALSTATEDIR=
	SWIG_RUBY_LIBEXECDIR=
	SWIG_RUBY_COMPILEDIR=
	SWIG_RUBY_SBINDIR=
	SWIG_RUBY_EXECPREFIX=
	SWIG_RUBY_SYSCONFDIR=
	SWIG_RUBY_RUBYLIBDIR=
	SWIG_RUBY_SITEARCHDIR=

	# --with-ruby=PATH
	AC_ARG_WITH(ruby,
		[  --with-ruby=PATH             Specify path to Ruby interpretor],
		[SWIG_RUBY_PROG="$withval"],
		[AC_PATH_PROGS(SWIG_RUBY_PROG, ruby ruby2.4 ruby2.3 ruby2.2 \
			ruby2.1 ruby2.0 ruby1.6 ruby1.5 ruby1.4, , $PATH)]
	)
	# --with-ruby-includes=INCLUDES
	AC_ARG_WITH(ruby-includes,
		[  --with-ruby-includes=PATH    Specify path to Ruby inlcude files],
		[SWIG_RUBY_CPPFLAGS="$withval"], [SWIG_RUBY_CPPFLAGS=]
	)
	# --with-ruby-prefix=PATH
	AC_ARG_WITH(ruby-prefix,
		[  --with-ruby-prefix           Specify Ruby directory prefix],
		[SWIG_RUBY_PREFIX="$withval"], [SWIG_RUBY_PREFIX=]
	)

	if test -z "$SWIG_RUBY_PROG"; then
		SWIG_RUBY_FAIL="$SWIG_RUBY_FAIL\nCould not find Ruby interpreter in $PATH"
	else
		# Check for Ruby prefix 
		AC_MSG_CHECKING(for Ruby prefix)
		if test -z "$SWIG_RUBY_PREFIX"; then
			SWIG_RUBY_PREFIX=`$SWIG_RUBY_PROG -rmkmf -e 'print Config::CONFIG[["prefix"]]' 2> /dev/null`
		fi
		AC_MSG_RESULT($SWIG_RUBY_PREFIX)

		# Retrieve the version
		AC_MSG_CHECKING(for Ruby version)
		SWIG_RUBY_MAJOR=`$SWIG_RUBY_PROG -v | sed 's/ruby \([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\).*/\1/'`
		SWIG_RUBY_MINOR=`$SWIG_RUBY_PROG -v | sed 's/ruby \([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\).*/\2/'`
		SWIG_RUBY_MICRO=`$SWIG_RUBY_PROG -v | sed 's/ruby \([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\).*/\3/'`
		AC_MSG_RESULT($SWIG_RUBY_MAJOR.$SWIG_RUBY_MINOR.$SWIG_RUBY_MICRO)

		# Check for additional vars
		AC_MSG_CHECKING(for sitelibdir)
		SWIG_RUBY_SITELIBDIR=`$SWIG_RUBY_PROG -rmkmf -e 'print Config::CONFIG[["sitelibdir"]]' 2> /dev/null`
		AC_MSG_RESULT($SWIG_RUBY_SITELIBDIR)
		AC_MSG_CHECKING(for bindir)
		SWIG_RUBY_BINDIR=`$SWIG_RUBY_PROG -rmkmf -e 'print Config::CONFIG[["bindir"]]' 2> /dev/null`
		AC_MSG_RESULT($SWIG_RUBY_BINDIR)
		AC_MSG_CHECKING(for datadir)
		SWIG_RUBY_DATADIR=`$SWIG_RUBY_PROG -rmkmf -e 'print Config::CONFIG[["datadir"]]' 2> /dev/null`
		AC_MSG_RESULT($SWIG_RUBY_DATADIR)
		AC_MSG_CHECKING(for archdir)
		SWIG_RUBY_ARCHDIR=`$SWIG_RUBY_PROG -rmkmf -e 'print Config::CONFIG[["archdir"]]' 2> /dev/null`
		AC_MSG_RESULT($SWIG_RUBY_ARCHDIR)
		AC_MSG_CHECKING(for sitedir)
		SWIG_RUBY_SITEDIR=`$SWIG_RUBY_PROG -rmkmf -e 'print Config::CONFIG[["sitedir"]]' 2> /dev/null`
		AC_MSG_RESULT($SWIG_RUBY_SITEDIR)
		AC_MSG_CHECKING(for sharedstatedir)
		SWIG_RUBY_SHAREDSTATEDIR=`$SWIG_RUBY_PROG -rmkmf -e 'print Config::CONFIG[["sharedstatedir"]]' 2> /dev/null`
		AC_MSG_RESULT($SWIG_RUBY_SHAREDSTATEDIR)
		AC_MSG_CHECKING(for localstatedir)
		SWIG_RUBY_LOCALSTATEDIR=`$SWIG_RUBY_PROG -rmkmf -e 'print Config::CONFIG[["localstatedir"]]' 2> /dev/null`
		AC_MSG_RESULT($SWIG_RUBY_LOCALSTATEDIR)
		AC_MSG_CHECKING(for libexecdir)
		SWIG_RUBY_LIBEXECDIR=`$SWIG_RUBY_PROG -rmkmf -e 'print Config::CONFIG[["libexecdir"]]' 2> /dev/null`
		AC_MSG_RESULT($SWIG_RUBY_LIBEXECDIR)
		AC_MSG_CHECKING(for compile_dir)
		SWIG_RUBY_COMPILEDIR=`$SWIG_RUBY_PROG -rmkmf -e 'print Config::CONFIG[["compile_dir"]]' 2> /dev/null`
		AC_MSG_RESULT($SWIG_RUBY_COMPILEDIR)
		AC_MSG_CHECKING(for sbindir)
		SWIG_RUBY_SBINDIR=`$SWIG_RUBY_PROG -rmkmf -e 'print Config::CONFIG[["sbindir"]]' 2> /dev/null`
		AC_MSG_RESULT($SWIG_RUBY_SBINDIR)
		AC_MSG_CHECKING(for execprefix)
		SWIG_RUBY_EXECPREFIX=`$SWIG_RUBY_PROG -rmkmf -e 'print Config::CONFIG[["exec_prefix"]]' 2> /dev/null`
		AC_MSG_RESULT($SWIG_RUBY_EXECPREFIX)
		AC_MSG_CHECKING(for sysconfdir)
		SWIG_RUBY_SYSCONFDIR=`$SWIG_RUBY_PROG -rmkmf -e 'print Config::CONFIG[["sysconfdir"]]' 2> /dev/null`
		AC_MSG_RESULT($SWIG_RUBY_SYSCONFDIR)
		AC_MSG_CHECKING(for rubylibdir)
		SWIG_RUBY_RUBYLIBDIR=`$SWIG_RUBY_PROG -rmkmf -e 'print Config::CONFIG[["rubylibdir"]]' 2> /dev/null`
		AC_MSG_RESULT($SWIG_RUBY_RUBYLIBDIR)
		AC_MSG_CHECKING(for sitearchdir)
		SWIG_RUBY_SITEARCHDIR=`$SWIG_RUBY_PROG -rmkmf -e 'print Config::CONFIG[["sitearchdir"]]' 2> /dev/null`
		AC_MSG_RESULT($SWIG_RUBY_SITEARCHDIR)

		# Check for Include paths
		AC_MSG_CHECKING(for ruby includes)
		if test -z "$SWIG_RUBY_CPPFLAGS"; then
			SWIG_RUBY_CPPFLAGS="-I$SWIG_RUBY_ARCHDIR" 
		fi
		AC_MSG_RESULT($SWIG_RUBY_CPPFLAGS)	

		# Check Headers (ruby.h)
		ruby_tmp=$CPPFLAGS
		CPPFLAGS="$CPPFLAGS $SWIG_RUBY_CPPFLAGS"
		AC_CHECK_HEADER(ruby.h, [], 
			[
				AC_MSG_WARN(Could not find Ruby headers.)
				SWIG_RUBY_FAIL="$SWIG_RUBY_FAIL\nCould not find Ruby headers (ruby.h) using $SWIG_RUBY_CPPFLAGS"
			]
		)
		CPPFLAGS=$ruby_tmp

		# Check for LFLAGS
		AC_MSG_CHECKING(for LFLAGS)
		SWIG_RUBY_LFLAGS=`$SWIG_RUBY_PROG -rmkmf -e 'print Config::CONFIG[["LIBS"]]' 2> /dev/null`
		AC_MSG_RESULT($SWIG_RUBY_LFLAGS)

		# Setup the swigrubydir for use with Automake
		AC_MSG_CHECKING(for Automake install dir)
		swigrubydir="$SWIG_RUBY_RUBYLIBDIR"
		AC_MSG_RESULT($swigrubydir)
			
	fi 

	AC_SUBST(SWIG_RUBY_PROG)
	AC_SUBST(SWIG_RUBY_CPPFLAGS)
	AC_SUBST(SWIG_RUBY_LFLAGS)
	AC_SUBST(SWIG_RUBY_PREFIX)
	AC_SUBST(SWIG_RUBY_CFLAGS)
	AC_SUBST(SWIG_RUBY_MAJOR)
	AC_SUBST(SWIG_RUBY_MINOR)
	AC_SUBST(SWIG_RUBY_MICRO)
	AC_SUBST(swigrubydir)
	AC_SUBST(SWIG_RUBY_SITELIBDIR)
	AC_SUBST(SWIG_RUBY_BINDIR)
	AC_SUBST(SWIG_RUBY_DATADIR)
	AC_SUBST(SWIG_RUBY_ARCHDIR)
	AC_SUBST(SWIG_RUBY_SITEDIR)
	AC_SUBST(SWIG_RUBY_SHAREDSTATEDIR)
	AC_SUBST(SWIG_RUBY_LOCALSTATEDIR)
	AC_SUBST(SWIG_RUBY_LIBEXECDIR)
	AC_SUBST(SWIG_RUBY_COMPILEDIR)
	AC_SUBST(SWIG_RUBY_SBINDIR)
	AC_SUBST(SWIG_RUBY_EXECPREFIX)
	AC_SUBST(SWIG_RUBY_SYSCONFDIR)
	AC_SUBST(SWIG_RUBY_RUBYLIBDIR)
	AC_SUBST(SWIG_RUBY_SITEARCHDIR)

	# Execute the given success/failure script	
	AS_IF([test -z "$SWIG_RUBY_FAIL"], [$1], [$2])	

]) # SWIG_RUBY


# SWIG_TCL(<if-found>, <if-not-found>)
# Substitutes: 
#  Standard:
#   SWIG_TCL_FAIL - Reason for failure, or nothing if successfull
#   SWIG_TCL_PROG - Path to Tcl interpreter 
#   SWIG_TCL_CPPFLAGS - Includes -I flags
#	SWIG_TCL_LFLAGS - Linker flags
#   SWIG_TCL_PREFIX - Tcl directory prefix
#   SWIG_TCL_CFLAGS - C compiler flags
#   SWIG_TCL_MAJOR - Tcl major version x.0.0
#   SWIG_TCL_MINOR - Tcl minor version 0.x.0
##   SWIG_TCL_MICRO - Tcl micro version 0.0.x
#   swigtcldir - Automake, install directory, same as $SWIG_TCL_PREFIX/lib-dynload
#  Additional:
#	SWIG_TCL_CONFIG
#	SWIG_TCL_EXECPREFIX
#-----------------------------------------------------------------------------
AC_DEFUN(SWIG_TCL, [

	SWIG_TCL_PROG=
	SWIG_TCL_CPPFLAGS=
	SWIG_TCL_LFLAGS=
	SWIG_TCL_PREFIX=
	SWIG_TCL_CFLAGS=
	SWIG_TCL_MAJOR=
	SWIG_TCL_MINOR=
	swigtcldir=
	SWIG_TCL_CONFIG=
	SWIG_TCL_EXECPREFIX=

	# --with-tcl=PATH
	AC_ARG_WITH(tcl,
		[  --with-tcl=PATH             Specify path to Tcl interpretor],
		[SWIG_TCL_PROG="$withval"],
		[AC_PATH_PROGS(SWIG_TCL_PROG, tcl tcl2.4 tcl2.3 tcl2.2 \
			tcl2.1 tcl2.0 tcl1.6 tcl1.5 tcl1.4, , $PATH)]
	)
	# --with-tclconfig=PATH
	AC_ARG_WITH(tclconfig,
		[  --with-tclconfig            Specify path to tclConfig.sh],
		[SWIG_TCL_CONFIG="$withval"],
		[
		dirs="/usr/local/lib /usr/lib"
		for d in $dirs; do
			if test -f "$d/tclConfig.sh"; then
				SWIG_TCL_CONFIG="$d/tclConfig.sh"
			fi;
		done;
		]
	)
	# --with-tcl-includes=INCLUDES
	AC_ARG_WITH(tcl-includes,
		[  --with-tcl-includes=PATH    Specify path to Tcl inlcude files],
		[SWIG_TCL_CPPFLAGS="$withval"], [SWIG_TCL_CPPFLAGS=]
	)
	# --with-tcl-prefix=PATH
	AC_ARG_WITH(tcl-prefix,
		[  --with-tcl-prefix           Specify Tcl directory prefix],
		[SWIG_TCL_PREFIX="$withval"], [SWIG_TCL_PREFIX=]
	)

	if test -z "$SWIG_TCL_CONFIG"; then
		SWIG_TCL_FAIL="$SWIG_TCL_FAIL\nCould not find tclConfig.sh"
	else
		# Run tclConfig.sh
		. $SWIG_TCL_CONFIG
	
		# Check for Tcl prefix 
		AC_MSG_CHECKING(for Tcl prefix)
		if test -z "$SWIG_TCL_PREFIX"; then
			SWIG_TCL_PREFIX="$TCL_PREFIX"
		fi
		AC_MSG_RESULT($SWIG_TCL_PREFIX)

		# Check for Tcl exec prefix
		AC_MSG_CHECKING(for Tcl exec-prefix)
		if test -z "$SWIG_TCL_EXECPREFIX"; then 
			SWIG_TCL_EXECPREFIX="$TCL_EXEC_PREFIX"
		fi
		AC_MSG_RESULT($SWIG_TCL_EXECPREFIX)

		# Retrieve the version
		AC_MSG_CHECKING(for Tcl version)
		SWIG_TCL_MAJOR="$TCL_MAJOR_VERSION"
		SWIG_TCL_MINOR="$TCL_MINOR_VERSION"
		AC_MSG_RESULT($SWIG_TCL_MAJOR.$SWIG_TCL_MINOR)

		# Check for Include paths
		AC_MSG_CHECKING(for tcl includes)
		if test -z "$SWIG_TCL_CPPFLAGS"; then 
			SWIG_TCL_CPPFLAGS="-I$TCL_PREFIX/include"
		fi
		AC_MSG_RESULT($SWIG_TCL_CPPFLAGS)	

		# Check Headers (tcl.h)
		tcl_tmp=$CPPFLAGS
		CPPFLAGS="$CPPFLAGS $SWIG_TCL_CPPFLAGS"
		AC_CHECK_HEADER(tcl.h, [], 
			[
				AC_MSG_WARN(Could not find Tcl headers.)
				SWIG_TCL_FAIL="$SWIG_TCL_FAIL\nCould not find Tcl headers (tcl.h) using $SWIG_TCL_CPPFLAGS"
			]
		)
		CPPFLAGS=$tcl_tmp

		# set the Execprefix path
		AC_MSG_CHECKING(for exec prefix)
		SWIG_TCL_EXECPREFIX="$TCL_EXEC_PREFIX"
		AC_MSG_RESULT($SWIG_TCL_EXECPREFIX)

		# Setup the swigtcldir for use with Automake
		AC_MSG_CHECKING(for Automake install dir)
		swigtcldir="$TCL_PACKAGE_PATH"	
		if test -r $swigtcldir; then
			AC_MSG_RESULT($swigtcldir)
		else
			AC_MSG_RESULT(Could not find $swigtcldir.)
			swigtcldir=
		fi
			
	fi 

	AC_SUBST(SWIG_TCL_PROG)
	AC_SUBST(SWIG_TCL_CPPFLAGS)
	AC_SUBST(SWIG_TCL_LFLAGS)
	AC_SUBST(SWIG_TCL_PREFIX)
	AC_SUBST(SWIG_TCL_CFLAGS)
	AC_SUBST(SWIG_TCL_MAJOR)
	AC_SUBST(SWIG_TCL_MINOR)
	AC_SUBST(SWIG_TCL_MICRO)
	AC_SUBST(swigtcldir)
	AC_SUBST(SWIG_TCL_CONFIG)
	AC_SUBST(SWIG_TCL_EXECPREFIX)

	# Execute the given success/failure script	
	AS_IF([test -z "$SWIG_TCL_FAIL"], [$1], [$2])	

]) # SWIG_TCL



# SWIG_PYTHON(<if-found>, <if-not-found>)
# Substitutes: 
#  Standard:
#   SWIG_PYTHON_FAIL - Reason for failure, or nothing if successfull
#   SWIG_PYTHON_PROG - Path to Python interpreter 
#   SWIG_PYTHON_CPPFLAGS - Includes -I flags
#   SWIG_PYTHON_LIBPATH - Path to library directory 
#   SWIG_PYTHON_PREFIX - Python directory prefix
#   SWIG_PYTHON_CFLAGS - C compiler flags
#   SWIG_PYTHON_MAJOR - Python major version x.0.0
#   SWIG_PYTHON_MINOR - Python minor version 0.x.0
#   SWIG_PYTHON_MICRO - Python micro version 0.0.x
#   swigpythondir - Automake, install directory, same as $SWIG_PYTHON_PREFIX/lib-dynload
#  Additional:
#   SWIG_PYTHON_EXECPREFIX - Python executable directory prefix
#   SWIG_PYTHON_LEVEL - Python release level (alpha, beta, candidate, final)
#   SWIG_PYTHON_SERIAL - Python serial #x
#-----------------------------------------------------------------------------
AC_DEFUN(SWIG_PYTHON, [

	SWIG_PYTHON_PROG=
	SWIG_PYTHON_CPPFLAGS=
	SWIG_PYTHON_LIBPATH=
	SWIG_PYTHON_PREFIX=
	SWIG_PYTHON_EXECPREFIX=
	SWIG_PYTHON_CFLAGS=
	SWIG_PYTHON_MAJOR=
	SWIG_PYTHON_MINOR=
	SWIG_PYTHON_MICRO=
	SWIG_PYTHON_LEVEL=
	SWIG_PYTHON_SERIAL=

	# --with-python=PATH
	AC_ARG_WITH(python,
		[  --with-python=PATH             Specify path to Python interpretor],
		[SWIG_PYTHON_PROG="$withval"],
		[AC_PATH_PROGS(SWIG_PYTHON_PROG, python python2.4 python2.3 python2.2 \
			python2.1 python2.0 python1.6 python1.5 python1.4, , $PATH)]
	)
	# --with-python-includes=INCLUDES
	AC_ARG_WITH(python-includes,
		[  --with-python-includes=PATH    Specify path to Python inlcude files],
		[SWIG_PYTHON_CPPFLAGS="$withval"], [SWIG_PYTHON_CPPFLAGS=]
	)
	# --with-python-libpath=LIBPATH
	AC_ARG_WITH(python-libpath,
		[  --with-python-libpath=PATH     Specify path to Python libraries],
		[SWIG_PYTHON_LIBPATH="$withval"], [SWIG_PYTHON_LIBPATH=]
	)
	# --with-python-prefix=PATH
	AC_ARG_WITH(python-prefix,
		[  --with-python-prefix           Specify Python directory prefix],
		[SWIG_PYTHON_PREFIX="$withval"], [SWIG_PYTHON_PREFIX=]
	)
	# --with-python-execprefix=PATH
	AC_ARG_WITH(python-execprefix,
		[  --with-python-execprefix=PATH Specify Python binary directory prefix],
		[SWIG_PYTHON_EXECPREFIX="$withval"], [SWIG_PYTHON_EXECPREFIX=]
	)

	if test -z "$SWIG_PYTHON_PROG"; then
		SWIG_PYTHON_FAIL="$SWIG_PYTHON_FAIL\nCould not find Python interpreter in $PATH"
	else
		# Check for Python prefix 
		AC_MSG_CHECKING(for Python prefix)
		if test -z "$SWIG_PYTHON_PREFIX"; then
			SWIG_PYTHON_PREFIX=`($SWIG_PYTHON_PROG -c "import sys; print sys.prefix") 2>/dev/null`
		fi
		AC_MSG_RESULT($SWIG_PYTHON_PREFIX)

		# Check for Python exec prefix
		AC_MSG_CHECKING(for Python exec-prefix)
		if test -z "$SWIG_PYTHON_EXECPREFIX"; then 
			SWIG_PYTHON_EXECPREFIX=`($SWIG_PYTHON_PROG -c "import sys; print sys.exec_prefix") 2>/dev/null`
		fi
		AC_MSG_RESULT($SWIG_PYTHON_EXECPREFIX)

		# Retrieve the version
		AC_MSG_CHECKING(for Python version)
		changequote({,})
		SWIG_PYTHON_MAJOR=`($SWIG_PYTHON_PROG -c "import sys; print sys.version_info[0]") 2>/dev/null`
		SWIG_PYTHON_MINOR=`($SWIG_PYTHON_PROG -c "import sys; print sys.version_info[1]") 2>/dev/null`
		SWIG_PYTHON_MICRO=`($SWIG_PYTHON_PROG -c "import sys; print sys.version_info[2]") 2>/dev/null`
		changequote([,])
		AC_MSG_RESULT($SWIG_PYTHON_MAJOR.$SWIG_PYTHON_MINOR.$SWIG_PYTHON_MICRO)

		# Check for Include paths
		AC_MSG_CHECKING(for python includes)
		if test -z "$SWIG_PYTHON_CPPFLAGS"; then 
			version="$SWIG_PYTHON_MAJOR.$SWIG_PYTHON_MINOR"
			if test -r "$SWIG_PYTHON_PREFIX/include/python$version/Python.h"; then
				SWIG_PYTHON_CPPFLAGS="-I$SWIG_PYTHON_PREFIX/include/python$version"
			elif test -r "$SWIG_PYTHON_PREFIX/include/Python.h"; then
				SWIG_PYTHON_CPPFLAGS="-I$SWIG_PYTHON_PREFIX/include"
			else
				# older version perhaps???
				SWIG_PYTHON_CPPFLAGS="-I$SWIG_PYTHON_PREFIX/include/Py -I$SWIG_PYTHON_PREFIX/lib/python/lib"
			fi
		fi
		AC_MSG_RESULT($SWIG_PYTHON_CPPFLAGS)	

		# Check Headers (Python.h)
		python_tmp=$CPPFLAGS
		CPPFLAGS="$CPPFLAGS $SWIG_PYTHON_CPPFLAGS"
		AC_CHECK_HEADER(Python.h, [], 
			[
				AC_MSG_WARN(Could not find Python headers.)
				SWIG_PYTHON_FAIL="$SWIG_PYTHON_FAIL\nCould not find Python headers (Python.h) using $SWIG_PYTHON_CPPFLAGS"
			]
		)
		CPPFLAGS=$python_tmp

		# Check for Libarary path
		AC_MSG_CHECKING(for Python library path)
		if test -z "$SWIG_PYTHON_LIBPATH"; then
			python_version="$SWIG_PYTHON_MAJOR.$SWIG_PYTHON_MINOR"
			SWIG_PYTHON_LIBPATH="Not Found"
			# Library directory is supposed to be in prefix + '/lib/pythonversion'
			dirs="$SWIG_PYTHON_PREFIX/lib/python$python_version"
			# Try these as well...
			dirs="$dirs $SWIG_PYTHON_PREFIX/lib/$python_version/config"
			dirs="$dirs $SWIG_PYTHON_PREFIX/lib/$python_version/lib"
			dirs="$dirs python/lib lib"
			for i in $dirs; do
				if test -d $i; then
					SWIG_PYTHON_LIBPATH="$i"
					break
				fi
			done
		fi
		AC_MSG_RESULT($SWIG_PYTHON_LIBPATH)
	
		# Cygwin (Windows) needs the library for dynamic linking
		case $host in 
			*-*-cygwin* | *-*-mingw*) 
				$SWIG_PYTHON_CFLAGS="-DUSE_DL_IMPORT"
				;;
		esac

		# Setup the swigpythondir for use with Automake
		AC_MSG_CHECKING(for Automake install dir)
		swigpythondir="$SWIG_PYTHON_PREFIX/lib/python$python_version/lib-dynload"	
		if test -r $swigpythondir; then
			AC_MSG_RESULT($swigpythondir)
		else
			AC_MSG_RESULT(Could not find $swigpythondir.)
			swigpythondir=
		fi
			
	fi 

	AC_SUBST(SWIG_PYTHON_PROG)
	AC_SUBST(SWIG_PYTHON_CPPFLAGS)
	AC_SUBST(SWIG_PYTHON_LIBPATH)
	AC_SUBST(SWIG_PYTHON_PREFIX)
	AC_SUBST(SWIG_PYTHON_EXECPREFIX)
	AC_SUBST(SWIG_PYTHON_CFLAGS)
	AC_SUBST(SWIG_PYTHON_MAJOR)
	AC_SUBST(SWIG_PYTHON_MINOR)
	AC_SUBST(SWIG_PYTHON_MICRO)
	AC_SUBST(SWIG_PYTHON_LEVEL)
	AC_SUBST(SWIG_PYTHON_SERIAL)
	AC_SUBST(swigpythondir)

	# Execute the given success/failure script	
	AS_IF([test -z "$SWIG_PYTHON_FAIL"], [$1], [$2])	

]) # SWIG_PYTHON


# SWIG_PHP(<if-found>, <if-not-found>)
# Substitutes: 
#  Standard:
#   SWIG_PHP_FAIL - Reason for failure, or nothing if successfull
#   SWIG_PHP_PROG - Path to PHP interpreter (if one exists)
#   SWIG_PHP_CPPFLAGS - Includes -I flags
#   SWIG_PHP_LFLAGS - Linker Flags for libraries 
#   SWIG_PHP_PREFIX - PHP directory prefix
#   SWIG_PHP_CFLAGS - C compiler flags
#   SWIG_PHP_MAJOR - PHP major version x.0.0
#   SWIG_PHP_MINOR - PHP minor version 0.x.0
#   SWIG_PHP_MICRO - PHP micro version 0.0.x
#   swigphpdir - Automake, install directory, same as $SWIG_PHP_EXTDIR 
#  Additional:
#   SWIG_PHP_CONFIG - Path to php-config
#   SWIG_PHP_EXTDIR - PHP extension directory
#-----------------------------------------------------------------------------
AC_DEFUN(SWIG_PHP, [

	SWIG_PHP_FAIL=
	SWIG_PHP_PROG=
	SWIG_PHP_CONFIG=
	SWIG_PHP_CPPFLAGS=
	SWIG_PHP_LFLAGS=
	SWIG_PHP_PREFIX=
	SWIG_PHP_EXTDIR=
	SWIG_PHP_CFLAGS=
	SWIG_PHP_MAJOR=
	SWIG_PHP_MINOR=
	SWIG_PHP_MICRO=

	# --with-php-config=PATH
	AC_ARG_WITH(php-config,
		[  --with-php-config=PATH             Specify path to php-config],
		[SWIG_PHP_CONFIG="$withval"],
		[AC_PATH_PROGS(SWIG_PHP_CONFIG, php-config, , $PATH)]
	)
	# --with-php=PATH
	AC_ARG_WITH(php,
		[  --with-php=PATH             Specify path to PHP interpretor],
		[SWIG_PHP_PROG="$withval"],
		[AC_PATH_PROGS(SWIG_PHP_PROG, php php2.4 php2.3 php2.2 \
			php2.1 php2.0 php1.6 php1.5 php1.4, , $PATH)]
	)
	# --with-php-includes=INCLUDES
	AC_ARG_WITH(php-includes,
		[  --with-php-includes=PATH    Specify path to PHP inlcude files],
		[SWIG_PHP_CPPFLAGS="$withval"], [SWIG_PHP_CPPFLAGS=]
	)
	# --with-php-lflags=FLAGS
	AC_ARG_WITH(php-lflags,
		[  --with-php-lflags=FLAGS    Specify PHP linker flags],
		[SWIG_PHP_LFLAGS="$withval"], [SWIG_PHP_LFLAGS=]
	)

	# --with-php-prefix=PATH
	AC_ARG_WITH(php-prefix,
		[  --with-php-prefix           Specify PHP directory prefix],
		[SWIG_PHP_PREFIX="$withval"], [SWIG_PHP_PREFIX=]
	)
	# --with-php-extdir=PATH
	AC_ARG_WITH(php-extdir,
		[  --with-php-extdir=PATH Specify PHP extension directory],
		[SWIG_PHP_EXTDIR="$withval"], [SWIG_PHP_EXTDIR=]
	)

	if test -z "$SWIG_PHP_CONFIG"; then
		SWIG_PHP_FAIL="$SWIG_PHP_FAIL\nCould not find php-config in $PATH"
	else
		# Check for PHP prefix 
		AC_MSG_CHECKING(for PHP prefix)
		if test -z "$SWIG_PHP_PREFIX"; then
			SWIG_PHP_PREFIX=`$SWIG_PHP_CONFIG --prefix 2>/dev/null`
		fi
		AC_MSG_RESULT($SWIG_PHP_PREFIX)

		# Check for PHP extension directory 
		AC_MSG_CHECKING(for PHP extension directory)
		if test -z "$SWIG_PHP_EXTDIR"; then
			SWIG_PHP_EXTDIR=`$SWIG_PHP_CONFIG --extension-dir 2>/dev/null` 
		fi
		AC_MSG_RESULT($SWIG_PHP_EXTDIR)

		# Retrieve the version
		AC_MSG_CHECKING(for PHP version)
		SWIG_PHP_MAJOR=`$SWIG_PHP_CONFIG --version | sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\1/'`
		SWIG_PHP_MINOR=`$SWIG_PHP_CONFIG --version | sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\2/'`
		SWIG_PHP_MICRO=`$SWIG_PHP_CONFIG --version | sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`
		AC_MSG_RESULT($SWIG_PHP_MAJOR.$SWIG_PHP_MINOR.$SWIG_PHP_MICRO)

		# Check for Include paths
		AC_MSG_CHECKING(for php includes)
		if test -z "$SWIG_PHP_CPPFLAGS"; then
			SWIG_PHP_CPPFLAGS=`$SWIG_PHP_CONFIG --includes` 
		fi
		AC_MSG_RESULT($SWIG_PHP_CPPFLAGS)	

		# Check Headers (PHP.h)
		php_tmp=$CPPFLAGS
		CPPFLAGS="$CPPFLAGS $SWIG_PHP_CPPFLAGS"
		php_headers="php_config.h php_version.h zend.h zend_API.h php.h php_ini.h ext/standard/info.h"
		for h in $php_headers; do
			AC_CHECK_HEADER($h, [], 
				[
					AC_MSG_WARN(Could not find PHP header $h.)	
					SWIG_PHP_FAIL="$SWIG_PHP_FAIL\nCould not find PHP headers ($h) using $SWIG_PHP_CPPFLAGS"
				]
			)
		done
		CPPFLAGS=$php_tmp

		# Check for Libarary path
		AC_MSG_CHECKING(for PHP linker flags)
		if test -z "$SWIG_PHP_LFLAGS"; then
			SWIG_PHP_LFLAGS=`$SWIG_PHP_CONFIG --libs`
		fi
		AC_MSG_RESULT($SWIG_PHP_LFLAGS)
	
		# Setup the swigphpdir for use with Automake
		AC_MSG_CHECKING(for Automake install dir)
		swigphpdir=$SWIG_PHP_EXTDIR
		if test -r $swigphpdir; then
			AC_MSG_RESULT($swigphpdir)
		else
			AC_MSG_RESULT(Could not find $swigphpdir.)
			swigphpdir=
		fi
			
	fi 

	AC_SUBST(SWIG_PHP_PROG)
	AC_SUBST(SWIG_PHP_CPPFLAGS)
	AC_SUBST(SWIG_PHP_LIBPATH)
	AC_SUBST(SWIG_PHP_PREFIX)
	AC_SUBST(SWIG_PHP_EXTDIR)
	AC_SUBST(SWIG_PHP_CFLAGS)
  AC_SUBST(SWIG_PHP_LFLAGS)
	AC_SUBST(SWIG_PHP_MAJOR)
	AC_SUBST(SWIG_PHP_MINOR)
	AC_SUBST(SWIG_PHP_MICRO)
	AC_SUBST(swigphpdir)

	# Execute the given success/failure script	
	AS_IF([test -z "$SWIG_PHP_FAIL"], [$1], [$2])	

]) # SWIG_PHP



