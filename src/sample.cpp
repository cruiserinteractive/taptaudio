/* $Id$ $URL$ */
#include "sample.h"

/**\file sample.cpp
 * Implementation of the ASSample classes
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>
#include <math.h>

#include "sndfile.h"

namespace {

/** libsndfile default format lookups for different encodings from taptaudio.h */
const int DEFAULT_FORMAT_LOOKUP[] = {
    SF_FORMAT_WAV  | SF_FORMAT_PCM_16 | SF_ENDIAN_FILE, ///< AF_ENC_WAV
    SF_FORMAT_RAW  | SF_FORMAT_PCM_16 | SF_ENDIAN_CPU,  ///< AF_ENC_RAW
#ifdef OGGSUPPORT
    SF_FORMAT_OGG  | SF_FORMAT_VORBIS | SF_ENDIAN_FILE, ///< AF_ENC_OGG
    SF_FORMAT_FLAC | SF_FORMAT_VORBIS | SF_ENDIAN_FILE, ///< AF_ENC_FLAC
#else
    0, 0,
#endif
    SF_FORMAT_AU   | SF_FORMAT_PCM_16 | SF_ENDIAN_FILE,  ///< AF_ENC_AU
    SF_FORMAT_AIFF | SF_FORMAT_PCM_16 | SF_ENDIAN_FILE,  ///< AF_ENC_AIFF
#ifdef CAFSUPPORT
    SF_FORMAT_CAF  | SF_FORMAT_PCM_16 | SF_ENDIAN_FILE,  ///< AF_ENC_CAF
#else
    0,
#endif
    0 ///< Dummy
};
const int NUM_DEFAULT_FORMATS = sizeof(DEFAULT_FORMAT_LOOKUP)/sizeof(*DEFAULT_FORMAT_LOOKUP) - 1;

const int HEADER_LOOKUP[] = {
    SF_FORMAT_WAV,  ///< AF_ENC_WAV
    SF_FORMAT_RAW,  ///< AF_ENC_RAW
#ifdef OGGSUPPORT
    SF_FORMAT_OGG,  ///< AF_ENC_OGG
    SF_FORMAT_FLAC, ///< AF_ENC_FLAC
#else
    0, 0,
#endif
    SF_FORMAT_AU,   ///< AF_ENC_AU
    SF_FORMAT_AIFF, ///< AF_ENC_AIFF
#ifdef CAFSUPPORT
    SF_FORMAT_CAF,  ///< AF_ENC_CAF
#else
    0,
#endif
    0 ///< Dummy
};

const int ENCODING_LOOKUP[] = {
    0, //< PCM
    SF_FORMAT_ULAW,
    SF_FORMAT_ALAW,
    0, ///< ADPCM is special -- change to one of SF_FORMAT_G72{2_24, 1_32, 2_40}, based on quality
    0, ///< DWVW  is special -- change to one of SF_FORMAT_DWVW_{12, 16, 24, N},  based on quality
#ifdef OGGSUPPORT
    SF_FORMAT_VORBIS,
    SF_FORMAT_SPEEX,
#else
    0, 0,
#endif
    0 ///< Dummy
};

const int PCM_LOOKUP[] = {
    0, ///< Use audio system's
    SF_FORMAT_PCM_U8,
    SF_FORMAT_PCM_S8,
    SF_FORMAT_PCM_16,
    SF_FORMAT_PCM_24,
    SF_FORMAT_PCM_32,
    SF_FORMAT_FLOAT,
    SF_FORMAT_DOUBLE,
    0 ///< Dummy
};

#ifdef __WIN32__
    const float VU_DECREASE_DECAY = 0.50f;
    const float VU_INCREASE_DECAY = 0.05f;
#else
    const float VU_DECREASE_DECAY = 0.90f;
    const float VU_INCREASE_DECAY = 0.50f;
#endif
    const unsigned VU_SKIP = 1;

}

void PCMSample::inherit_sfinfo(const SF_INFO& info, unsigned auto_sz, AUDIO_FORMAT forceformat) {
    data_sz = auto_sz;
    channels = info.channels;
    rate = info.samplerate;
    nframes = info.frames;
    bytes_size = data_sz*channels*nframes;
    if (forceformat != AF_Default)
        fmt = forceformat;
}

float PCMSample::updateVU(const float *buffer, unsigned long buf_siz, float vu_level) {
    if (!buffer)
        return vu_level * VU_DECREASE_DECAY;

    float sum = 0.0f;
    const float* p = buffer;
    const float* const bend = p + buf_siz;
    float divisor = (bend - p) / VU_SKIP; //do integer division, then cast
    for (; p < bend; p += VU_SKIP) {
        sum += fabsf(*p);
    }
    const float avg = sum / divisor;
    if (avg > vu_level)
        return (vu_level * VU_INCREASE_DECAY) + (1.0f-VU_INCREASE_DECAY)*(sum/ /*count*/ divisor);

    return (vu_level * VU_DECREASE_DECAY) + (1.0f-VU_DECREASE_DECAY)*(sum/ /*count*/ divisor);
}

void PCMSample::mixed(unsigned long xframes,
                      void* handle,
                      int channelOffset,
                      const void *mixdata) {

    if (data_sz < 4)
        return;

    vu = updateVU(static_cast<const float*>(mixdata),
                  xframes,
                  vu);
}

ASSample* PCMSample::destructive_split(unsigned long where) {
    ASSample *lhs, *rhs;
    if (!split(where, &lhs, &rhs))
        return 0;
    delete[] static_cast<Uint8*>(data);
    if (lhs) {
        PCMSample *xlhs = dynamic_cast<PCMSample*>(lhs);
        //assert(xlhs);
        data = xlhs->data;
        xlhs->data = 0;
        tellSize(xlhs->bytes_size);
        delete lhs;
    } else {
        data = 0;
        bytes_size = 0;
    }
    return rhs;
}

bool PCMSample::split(unsigned long where, ASSample **start, ASSample **end) const {
    size_t offset = static_cast<size_t>(where * rate * 0.001) * channels * data_sz;
    DODEBUG(INFO, ("Split offset = lrint(%lu, %f*0.001) * %d * %u = %lu",
                   where, rate, channels, data_sz,
                   (unsigned long)offset));
    if (offset > bytes_size)
        offset = bytes_size;
    size_t remain = bytes_size - offset;
    DODEBUG(FUNCTION, ("Splitting %p at %lums %lu=%lu|%lu",
                       (const void*)this, where, bytes_size,
                       (unsigned long)offset, (unsigned long)remain));

    if (offset && start) {
        PCMSample * lhs = new PCMSample(*this);
        lhs->tellSize(offset);
        lhs->data = new Uint8[offset];
        memcpy(lhs->data, data, offset);
        *start = lhs;
    } else if (start) {
        *start = 0;
    }
    if (remain && end) {
        PCMSample * rhs = new PCMSample(*this);
        rhs->bytes_size = remain;
        rhs->data = new Uint8[remain];
        memcpy(rhs->data, static_cast<Uint8*>(data) + offset, remain);
        *end = rhs;
    } else if (end) {
        *end = 0;
    }
    DODEBUG(FUNCTION, ("Split: start/end = %p/%p -> %p/%p",
                       (void*)start, (void*)end,
                       (void*)(start ? *start : 0),
                       (void*)(end ? *end : 0)));
    return true;
}

template <>
sf_count_t lsf_read(SNDFILE *sndfile, short** data, sf_count_t items) {
    *data = new short[items];
    return sf_read_short(sndfile, *data, items);
}

template <>
sf_count_t lsf_read(SNDFILE *sndfile, int** data, sf_count_t items) {
    *data = new int[items];
    return sf_read_int(sndfile, *data, items);
}
template <>
sf_count_t lsf_read(SNDFILE *sndfile, float** data, sf_count_t items) {
    *data = new float[items];
    return sf_read_float(sndfile, *data, items);
}
template <>
sf_count_t lsf_read(SNDFILE *sndfile, double** data, sf_count_t items) {
    *data = new double[items];
    return sf_read_double(sndfile, *data, items);
}

template <>
sf_count_t lsf_writef(SNDFILE *sndfile, const short* data, sf_count_t items, int channels) {
    DODEBUG(VINFO, ("\tWriting %d frames of shorts from memory", (int)items));
    return sf_writef_short(sndfile, CC(data, short), items);
}

template <>
sf_count_t lsf_writef(SNDFILE *sndfile, const int* data, sf_count_t items, int channels) {
    DODEBUG(VINFO, ("\tWriting %d frames of ints from memory", (int)items));
    return sf_writef_int(sndfile, CC(data, int), items);
}
template <>
sf_count_t lsf_writef(SNDFILE *sndfile, const float* data, sf_count_t items, int channels) {
    DODEBUG(VINFO, ("\tWriting %d frames of floats from memory", (int)items));
    return sf_writef_float(sndfile, CC(data, float), items);
}
template <>
sf_count_t lsf_writef(SNDFILE *sndfile, const double* data, sf_count_t items, int channels) {
    DODEBUG(VINFO, ("\tWriting %d frames of doubles from memory", (int)items));
    return sf_writef_double(sndfile, CC(data, double), items);
}

Recording::Recording(unsigned long initial_size,
                     unsigned chans,
                     AUDIO_FORMAT format,
                     double samplerate)
:
PCMSample(format), reserved(initial_size)
{
    data = new Uint8[reserved];
    channels = chans;
    tellSize(0);
    rate = samplerate;
    //fmt = format;
}
Recording::~Recording() {
    delete[] static_cast<Uint8*>(data);
    data = 0;
}
ASSample *Recording::makePlayable(bool del_if_new) {
    return this;
}

void Recording::fill(const void *more, unsigned long sz, int channelOffset, int bufChannels) {
    if (channelOffset >= 0) {
        const unsigned long BSZ = bSize();
        /* here, more is NOT interleaved (a single channel), we need to interleave it into
         * channelOffset in the recording. In this case, sz == nframes.
         */
        const unsigned long num_bytes =
            (BSZ + sz*channels*data_sz > reserved ? /* are we too full? */
             (reserved-BSZ)/channels     : /* this is how many bytes are left for each channel */
             sz * data_sz);                       /* otherwise go to the end of more */

        interleave(static_cast<Uint8*>(data) + BSZ,
                   static_cast<const Uint8*>(more),
                   num_bytes,
                   data_sz,
                   channels,
                   channelOffset);

        if (channelOffset + 1 == channels)
            tellSize(BSZ + num_bytes*channels);

        return;
    }
    if (bufChannels == channels) {
        const unsigned long BSZ = bSize();
        //fprintf(stderr, "sz = %lu, bytes_size = %lu, reserved = %lu, data_sz = %u, channels = %d\n",
        //        sz, bytes_size, reserved, data_sz, channels);
        if (BSZ + sz > reserved) {
            memcpy(static_cast<Uint8*>(data) + BSZ, more, reserved-BSZ);
            tellSize(reserved);
        } else {
            memcpy(static_cast<Uint8*>(data) + BSZ, more, sz);
            tellSize(BSZ + sz);
        }
        return;
    }
}

void Recording::skipfill(const void *more, unsigned long sz, int channelOffset) {
                                                                                  /*
    sz /= 2;
    unsigned long szavail = bytes_size + sz > reserved ? reserved - bytes_size : sz;
    unsigned long nblocks = szavail / block;
    DODEBUG(INTERRUPT, ("At skipfill(more = %p, sz = %lu, block = %u), szavail = %lu, nblocks = %lu",
                        more, sz, block, szavail, nblocks));
    if (channelOffset >= 0)
        return; ///< \todo Not implemented here -- prolly don't need it
    Uint8 *to = static_cast<Uint8*>(data) + bytes_size;
    const Uint8 *from = static_cast<const Uint8*>(more);
    for (unsigned b = 0; b < nblocks; ++b) {
        memcpy(to, from, block);
        to += block;
        from += 2*block;
    }
    bytes_size += szavail;*/
}

void Recording::trim() {
    const unsigned long BSZ = bSize();
    if (reserved > BSZ) {
        void* new_buf = new Uint8[BSZ];
        memcpy(new_buf, data, BSZ);
        delete[] static_cast<Uint8*>(data);
        data = new_buf;
    }
}

void Recording::fillSilence(unsigned long sz) {
    const unsigned long BSZ = bSize();
    if (BSZ + sz > reserved) {
        memset(static_cast<Uint8*>(data) + BSZ, 0, reserved-BSZ);
        tellSize(reserved);
    } else {
        memset(static_cast<Uint8*>(data) + BSZ, 0, sz);
        tellSize(BSZ + sz);
    }
}

void Recording::append(void *more, unsigned long sz) {
    const unsigned long BSZ = bSize();
    if (BSZ + sz > reserved) {
        do {
            reserved <<= 1;
        } while (BSZ + sz > reserved);
        void* new_buf = new Uint8[reserved];
        memcpy(new_buf, data, BSZ);
        delete[] static_cast<Uint8*>(data);
        data = new_buf;
    }
    memcpy(static_cast<Uint8*>(data) + BSZ, more, sz);
    tellSize(BSZ + sz);
}

void PCMSample::mono2stereo(void *newdata,
                            const void *olddata,
                            unsigned long olddata_bytes,
                            unsigned oldframesize) {
    for (unsigned long i = 0; i < olddata_bytes; i += oldframesize) {
        memcpy(static_cast<Uint8*>(newdata) + 2*i,
               static_cast<const Uint8*>(olddata) + i, oldframesize);
        memcpy(static_cast<Uint8*>(newdata) + 2*i + oldframesize,
               static_cast<const Uint8*>(olddata) + i, oldframesize);
    }
//        for (unsigned b = 0; b < oldframesize; ++b)
//            newdata[2*i + b + oldframesize] = newdata[2*i + b] = olddata[i + b];
}

    /* hmmmm this is very dodgy... we don't use this at the moment */
void PCMSample::stereo2mono(Uint8 *newdata,
                            const Uint8 *olddata,
                            unsigned long olddata_bytes,
                            unsigned framesize) {
    for (unsigned long i = 0; i < olddata_bytes/2; i += framesize) {
        unsigned long accum[2] = {0, 0}; //unsigned long long??
        for (unsigned b = 0; b < framesize; ++b) {
            accum[0] = (accum[0] << 8) + olddata[2*i + b];
            accum[1] = (accum[1] << 8) + olddata[2*i + b + framesize];
        }
        accum[0] =  static_cast<unsigned long>((0.5 + accum[0] + accum[1]) / 2.0);
        for (unsigned b = 0; b < framesize; ++b) {
            newdata[i + framesize - b] = accum[0] & 0xff;
            accum[0] = accum[0] >> 8 & 0x00ffffff;
        }
    }
}

void FileSample::load_sndfile(SNDFILE* sf, SF_INFO &sfinfo, bool forcestereo) {
    DODEBUG(ERROR, ("This function is deprecated and should not be used"));
    unsigned  sz = 0;
    switch (sfinfo.format & SF_FORMAT_SUBMASK) {
        //case SF_FORMAT_DOUBLE: sz = 4;
    case SF_FORMAT_FLOAT:
    case SF_FORMAT_PCM_32: sz++;
    /*case SF_FORMAT_PCM_24:*/ sz++;
    case SF_FORMAT_PCM_16: sz++;
    case SF_FORMAT_PCM_S8:
        /*case SF_FORMAT_PCM_U8:*/ sz++;
    }
    if (sz == 0) {
        throw "Unsupported";
    }

    unsigned frame_sz = sz * sfinfo.channels;
    const unsigned long BSZ = frame_sz * sfinfo.frames;
    channels = sfinfo.channels;
    tellSize(BSZ);
    rate = sfinfo.samplerate;
    data = new Uint8[BSZ];

    sf_count_t numread = 0;

    switch (sfinfo.format & SF_FORMAT_SUBMASK) {
    case SF_FORMAT_FLOAT:
        numread = sf_readf_float(sf, static_cast<float*>(data), sfinfo.frames);
        fmt = AF_Float32;
        break;
    case SF_FORMAT_PCM_32:
        numread = sf_readf_int(sf, static_cast<int*>(data), sfinfo.frames);
        fmt = AF_Int32;
        break;
    case SF_FORMAT_PCM_16:
        numread = sf_readf_short(sf, static_cast<short*>(data), sfinfo.frames);
        fmt = AF_Int16;
        break;
    case SF_FORMAT_PCM_S8:
        numread = sf_read_raw(sf, data, BSZ);
        fmt = AF_Int8;
    }

    if (channels == 1 && forcestereo) {
        Uint8 *newdata = new Uint8[BSZ * 2];
        mono2stereo(newdata, data, BSZ, sz);
        delete[] static_cast<Uint8*>(data);
        data = newdata;
        tellSize(BSZ * 2);
        channels = 2;
    }
}

FileSample::FileSample(const std::string &path, bool forcestereo) {
    SF_INFO sfinfo;
    memset(&sfinfo, 0, sizeof(sfinfo));
    SNDFILE* sf = 0;
    sf = sf_open(path.c_str(), SFM_READ, &sfinfo);
    if (sf) {
        try {
            load_sndfile(sf, sfinfo, forcestereo);
            DODEBUG(FUNCTION, ("Successfully loaded %s", path.c_str()));
        } catch (...) {
            DODEBUG(ERROR, ("Unsupported file format in %s\n",
                            path.c_str()));
            sf_close(sf);
            throw;
        }
        sf_close(sf);
    } else {
        DODEBUG(ERROR, ("Unable to open %s: %s",
                        path.c_str(), sf_strerror(sf)));
        throw "Unable to open file";
    }
}

FileSample::~FileSample() {
    delete[] static_cast<Uint8*>(data);
    data = 0;
}

SNDFILE* PCMSample::open(const std::string &path, FILE_ENCODING enc, float quality, int mode) const {
    SF_INFO sfinfo = {0, 0, 0, 0, 0, 0}; /* frames, samplerate, channels, format, sections, seekable */
    sfinfo.samplerate = static_cast<int>(getSamRate());
    sfinfo.channels = getChannels();

    if (enc >= NUM_DEFAULT_FORMATS) {
        int encflag = ENCODING_LOOKUP[(enc & AF_MASK_ENCODING) >> AF_SHIFT_ENCODING];
        /* more than defaults specified */
        sfinfo.format = HEADER_LOOKUP[(enc & AF_MASK_FILE) >> AF_SHIFT_FILE];
        if (encflag) {
            sfinfo.format |= encflag;
        } else if ((enc & AF_MASK_ENCODING) == AF_ENC_ADPCM) {
            if (enc & AF_MASK_FILE) {
                // ADPCM is special -- change to one of SF_FORMAT_G72{2_24, 1_32, 2_40}, based on quality
                if (fabsf(quality - 40.0f) < 1.0f)
                    sfinfo.format |= SF_FORMAT_G723_40;
                else if (fabsf(quality - 32.0f) < 1.0f)
                    sfinfo.format |= SF_FORMAT_G721_32;
                else
                    sfinfo.format |= SF_FORMAT_G723_24;
            } else /* we are wav or unspecified */ {
                sfinfo.format |= SF_FORMAT_MS_ADPCM;
            }
        } else if ((enc & AF_MASK_ENCODING) == AF_ENC_DWVW) {
            // DWVW  is special -- change to one of SF_FORMAT_DWVW_{12, 16, 24, N},  based on quality
            if (fabsf(quality) < 1.0f)
                sfinfo.format |= SF_FORMAT_DWVW_N;
            else if (fabsf(quality - 12.0f) < 1.0f)
                sfinfo.format |= SF_FORMAT_DWVW_12;
            else if (fabsf(quality - 16.0f) < 1.0f)
                sfinfo.format |= SF_FORMAT_DWVW_16;
            else
                sfinfo.format |= SF_FORMAT_DWVW_24;
        } else /* we are PCM */ {
            sfinfo.format |= PCM_LOOKUP[(enc & AF_MASK_PCM_FORMAT) >> AF_SHIFT_PCM_FORMAT];
        }
        if ((enc & AF_MASK_FILE) == AF_ENC_RAW)
            sfinfo.format |= SF_ENDIAN_CPU;
    } else {
        sfinfo.format = DEFAULT_FORMAT_LOOKUP[enc];
    }

    DODEBUG(FUNCTION, ("Saving to %s in format %d->0x%x and quality %f", path.c_str(), (int)enc, sfinfo.format, quality));
    if (TAPTDBGLEV >= DEBUG_INFO) {
        SF_FORMAT_INFO info;
        info.format = sfinfo.format & SF_FORMAT_TYPEMASK;
        sf_command(0, SFC_GET_FORMAT_INFO, &info, sizeof(info));
        DODEBUG(INFO, ("\tFormat    type: %08x  %s (%s)", info.format, info.name, info.extension));
        info.format = sfinfo.format & SF_FORMAT_SUBMASK;
        sf_command(0, SFC_GET_FORMAT_INFO, &info, sizeof(info));
        DODEBUG(INFO, ("\tFormat subtype: %08x  %s", info.format, info.name));
        DODEBUG(INFO, ("\tThis %s a valid format", sf_format_check(&sfinfo) ? "IS" : "is NOT"));
        DODEBUG(INFO, ("\tWe will write %lu bytes from memory -- %lu frames of %d, %d-byte-sized channels -- to the file",
                       bytes_size, bytes_size / (data_sz*channels), channels, data_sz));
    }
    return sf_open(path.c_str(), mode, &sfinfo);
}

sf_count_t PCMSample::writen(SNDFILE *sf, void* bdata, unsigned long nbytes) const {
    /* use writef to maybe support joint stereo in future, but we need channels in case we are raw */
    if (sf) switch (fmt) {
    case (AF_Float32):
        return lsf_writef(sf, static_cast<const float*>(bdata), nbytes / (data_sz*channels), channels);
    case (AF_Int16):
        return lsf_writef(sf, static_cast<const short*>(bdata), nbytes / (data_sz*channels), channels);
    case (AF_Int32):
        return lsf_writef(sf, static_cast<const   int*>(bdata), nbytes / (data_sz*channels), channels);
    default:
        return lsf_writef(sf, static_cast<const  char*>(bdata), nbytes / (data_sz*channels), channels);
    }
    return 0;
}

long PCMSample::remove_silence(float threshold, unsigned window) {
    DODEBUG(WARNING, ("remove_silence in BETA at this level of generality"));
    if (fmt) switch (fmt) {
    case (AF_Float32):
        return remove_silence(reinterpret_cast<float**>(&data), this, threshold, window);
    case (AF_Int16):
        return remove_silence(reinterpret_cast<short**>(&data), this, threshold, window);
    case (AF_Int32):
        return remove_silence(reinterpret_cast<int**>(&data), this, threshold, window);
    default:
        return remove_silence(reinterpret_cast<char**>(&data), this, threshold, window);
    }
    return -1;
}

bool PCMSample::save(const std::string &path, FILE_ENCODING enc, float quality) {
    SNDFILE *sf = open(path, enc, quality, SFM_WRITE);
    DODEBUG(FUNCTION, ("Calling writen to write %lu bytes of %d-channel, %u-byte data from %p to %s",
                       (unsigned long)bytes_size, channels,
                       (unsigned)data_sz, (void*)data, path.c_str()));
    sf_count_t numwritten = writen(sf, data, bytes_size);

    if (numwritten == 0)
        sf_perror(sf);

    if (sf)
        sf_close(sf);

    return numwritten > 0;
}

#if 0
/* old version -- crappy */
bool Recording::save(const std::string &path, FILE_ENCODING enc, float quality) {
    DODEBUG(ERROR, ("This function is deprecated and should not be used"));
    SNDFILE *sf = 0;
    SF_INFO sfinfo = {0, static_cast<int>(rate), channels, 0, 1, 0};
    sf_count_t numwritten = 0;
    sfinfo.format = FORMAT_LOOKUP[enc];
    DODEBUG(FUNCTION, ("Saving to %s in format %d->0x%x and quality %f", path.c_str(), (int)enc, sfinfo.format, quality));
    switch (fmt) {
    case AF_Float32:
        sfinfo.frames = bytes_size / (4 * channels);
        sfinfo.format |= SF_FORMAT_FLOAT;
        if ((sf = sf_open(path.c_str(), SFM_WRITE, &sfinfo)))
            numwritten = sf_writef_float(sf, static_cast<float*>(data), bytes_size / (4*channels));
        break;
    case AF_Int32:
        sfinfo.frames = bytes_size / (4 * channels);
        sfinfo.format |= SF_FORMAT_PCM_32;
        if ((sf = sf_open(path.c_str(), SFM_WRITE, &sfinfo)))
            numwritten = sf_writef_int(sf, static_cast<int*>(data), bytes_size / (4*channels));
        break;
    case AF_Int16:
        sfinfo.frames = bytes_size / (2 * channels);
        sfinfo.format |= SF_FORMAT_PCM_16;
        if ((sf = sf_open(path.c_str(), SFM_WRITE, &sfinfo)))
            numwritten = sf_writef_short(sf, static_cast<short*>(data), bytes_size / (2 * channels));
        break;
    case AF_Int8:
        sfinfo.frames = bytes_size / 1;
        sfinfo.format |= SF_FORMAT_PCM_S8;
        if ((sf = sf_open(path.c_str(), SFM_WRITE, &sfinfo)))
            numwritten = sf_write_raw(sf, data, bytes_size);
        break;
    default:
        break;
    }
    if (numwritten == 0)
        sf_perror(sf);

    if (sf)
        sf_close(sf);

    return numwritten > 0;
}
#endif
