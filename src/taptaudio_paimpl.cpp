/* $Id$ $URL$ */

/**\file taptaudio_paimpl.cpp
 * Implementation of portaudio implementation of taptaudio
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

#include "taptaudio_paimpl.h"

namespace {
    const int FRAMES_PER_BUFFER = 256;
    volatile bool local_ready = false;

#ifndef PAV19
    int my_callback(void *inputBuffer,
                    void *outputBuffer,
                    unsigned long framesPerBuffer,
                    PaTimestamp /*outTime*/,
                    void *userData) {
        return local_ready
            ? static_cast<PaAudioSystemImpl*>(userData)->
            callback(inputBuffer,
                     outputBuffer,
                     framesPerBuffer/*,
                     outTime*/)
            : 0;
    }
#else
    int my_callback(const void *input,
                    void *output,
                    unsigned long frameCount,
                    const PaStreamCallbackTimeInfo* timeInfo,
                    PaStreamCallbackFlags /*statusFlags*/,
                    void *userData ) {
        return local_ready
            ? static_cast<PaAudioSystemImpl*>(userData)->
            callback(input,
                     output,
                     frameCount/*,
                     timeInfo->outputBufferDacTime*/)
            : paContinue;
    }

    PaHostApiIndex subhost2host_index(int subhost) throw (PaError) { /*FOLD00*/
        PaHostApiIndex host_index;
        switch (subhost) {
        case AF_PORTAUDIO_OSS:
            host_index = Pa_HostApiTypeIdToHostApiIndex(paOSS); break;
        case AF_PORTAUDIO_ALSA_BLOCK:
        case AF_PORTAUDIO_ALSA:
            host_index = Pa_HostApiTypeIdToHostApiIndex(paALSA); break;
        case AF_PORTAUDIO_JACK:
            host_index = Pa_HostApiTypeIdToHostApiIndex(paJACK); break;
        case AF_PORTAUDIO_MME:
            host_index = Pa_HostApiTypeIdToHostApiIndex(paMME); break;
        case AF_PORTAUDIO_ASIO:
            host_index = Pa_HostApiTypeIdToHostApiIndex(paASIO); break;
        case AF_PORTAUDIO_AL:
            host_index = Pa_HostApiTypeIdToHostApiIndex(paAL); break;
        case AF_PORTAUDIO_BEOS:
            host_index = Pa_HostApiTypeIdToHostApiIndex(paBeOS); break;
        case AF_PORTAUDIO_WDMKS:
            host_index = Pa_HostApiTypeIdToHostApiIndex(paWDMKS); break;
        case AF_PORTAUDIO_COREAUDIO:
            host_index = Pa_HostApiTypeIdToHostApiIndex(paCoreAudio); break;
        case AF_PORTAUDIO_SOUNDMANAGER:
            host_index = Pa_HostApiTypeIdToHostApiIndex(paSoundManager); break;
        case AF_PORTAUDIO_DIRECTSOUND:
            host_index = Pa_HostApiTypeIdToHostApiIndex(paDirectSound); break;
        default:
            host_index = 0;
        }
        if (host_index < 0)
            throw (PaError)host_index;
        return host_index;
    } /*FOLD00*/

#endif

}

/****************************************************/
/* AudioSystemImpl class implementation starts here */
/****************************************************/

PaAudioSystemMixer::PaAudioSystemMixer(unsigned inputChannels,
                                       unsigned outputChannels,
                                       double sampleRate,
                                       AUDIO_FORMAT format,
                                       unsigned ttracks,
                                       unsigned rtracks,
                                       unsigned subhost,
                                       int deviceIDin,
                                       int deviceIDout)
:
PaAudioSystemImpl(inputChannels, outputChannels, sampleRate, format, subhost, deviceIDin, deviceIDout),
AudioSystemMixer(this, format, ttracks, rtracks)
{
    DODEBUG(NOTICE, ("Mixing %d tracks into PortAudio", tracks));
}

PaAudioSystemImpl::~PaAudioSystemImpl()
{
    local_ready = false;
    PaError err;
    if (!stopped) {
        DODEBUG(NOTICE, ("PortaudioSystem was not stopped before destructor.. trying to cease()"));
        cease();
    }
    if (!stopped) {
        DODEBUG(NOTICE, ("We did not stop. Disaster may ensue"));
    }
    //Can trigger a symbol lookup error!
    //if (Pa_StreamActive(stream)) {
    //    DODEBUG(NOTICE, ("PortAudio still thinks we are active. Disaster may ensue"));
    //}

    DODEBUG(NOTICE, ("Closing stream (should flush any pending buffers)"));
    err = Pa_CloseStream(stream);
}

void PaAudioSystemImpl::cease() {
    PaError err;
    DODEBUG(INFO, ("Calling Pa_StopStream outside of mutex -- will flush pending audio buffers"));
    err = Pa_StopStream(stream);
}

PaAudioSystemImpl::PaAudioSystemImpl(unsigned inputChannels,
                                     unsigned outputChannels,
                                     double sampleRate,
                                     AUDIO_FORMAT format,
                                     unsigned subhost,
                                     int deviceIDin,
                                     int deviceIDout)
:
AudioSystemImpl(inputChannels, outputChannels, sampleRate, format, deviceIDin, deviceIDout),
stream(0)
{

    PaError err = 0;
    if ((err = Pa_Initialize()) != paNoError)
        throw err;
    if (inputChannels == 1 || inChannels == 1) {
        DODEBUG(WARNING, ("Portaudio currently doesn't seem to work with anything other than 2 input channels\n"
                          "\tsetting inputChannels to 2"));
        inChannels = inputChannels = 2;
        iframe_size *= 2;
    }
    fakeMonoMic = false; /* ?? */

#ifndef PAV19 /*FOLD00*/
    DODEBUG(NOTICE, ("Initializing AudioSystem (PortAudio v18) at %fHz for %d input and %d output channels on device %d/%d...",
                     sampRate, inChannels, outChannels, deviceIDin, deviceIDout));
    DODEBUG(INFO, ("fakeMonoMic = %s, iframe_size = %u, oframe_size = %u, sz = ?.",
                   fakeMonoMic ? "true" : "false", iframe_size, oframe_size));

    if (deviceIDin < 0 && deviceIDout < 0) {
        if ((err = Pa_OpenDefaultStream(&stream,    /* stream */
                                        fakeMonoMic ? 2 : inChannels,
                                        outChannels,
                                        fmt,         /* outputSampleFormat */
                                        sampRate,    /* sampleRate */
                                        FRAMES_PER_BUFFER,        /* frames per buffer -- about 23ms at 44,100 */
                                        0,           /* number of buffers -- use the default minimum */
                                        my_callback,
                                        this)) != paNoError)
            throw err;
    } else {

        if (deviceIDin < 0)
            deviceIDin = 0;
        if (deviceIDout < 0)
            deviceIDout = 0;

        if ((err = Pa_OpenStream(&stream,    /* stream */
                                 deviceIDin, /* inputDeviceID */
                                 fakeMonoMic ? 2 : inChannels,
                                 fmt,        /* inputSampleFormat */
                                 0,          /* inputDriverInfo */
                                 deviceIDout, /* outputDeviceID */
                                 outChannels,
                                 fmt,         /* outputSampleFormat */
                                 0,           /* outputDriverInfo */
                                 sampRate,    /* sampleRate */
                                 FRAMES_PER_BUFFER,        /* frames per buffer -- about 23ms at 44,100 */
                                 0,           /* number of buffers -- use the default minimum */
                                 0,           /* PortAudio Stream Flags */
                                 my_callback,
                                 this)) != paNoError)
            throw err;
    }
#else /*FOLD00*/
    if (subhost != AF_PORTAUDIO && subhost != AF_PORTAUDIO_ABSOLUTE) {
        PaHostApiIndex host_index = subhost2host_index(subhost);
        const PaHostApiInfo* api = Pa_GetHostApiInfo(host_index);
        DODEBUG(NOTICE, ("Finding devices %d/%d on the %s(%d) PortAudio host system",
                         deviceIDin, deviceIDout, api->name, host_index));
        deviceIDin =
            deviceIDin < 0 ?
            api->defaultInputDevice :
            Pa_HostApiDeviceIndexToDeviceIndex(host_index, deviceIDin);
        deviceIDout =
            deviceIDout < 0 ?
            api->defaultOutputDevice :
            Pa_HostApiDeviceIndexToDeviceIndex(host_index, deviceIDout);
        if (deviceIDin < 0)
            throw (PaError)deviceIDin;
        if (deviceIDout < 0)
            throw (PaError)deviceIDout;
    } else if (deviceIDin < 0 != deviceIDout < 0) {
        if (deviceIDin < 0)
            deviceIDin = Pa_GetDefaultInputDevice();
        if (deviceIDout < 0)
            deviceIDout = Pa_GetDefaultOutputDevice();
    }
    DODEBUG(NOTICE, ("Initializing AudioSystem (PortAudio v19) at %fHz for %d input and %d output channels\n\ton absolute device %d/%d...",
                     sampRate, fakeMonoMic ? 2 : inChannels, outChannels, deviceIDin, deviceIDout));
    DODEBUG(INFO, ("fakeMonoMic = %s, iframe_size = %u, oframe_size = %u, sz = ?.",
                   fakeMonoMic ? "true" : "false", iframe_size, oframe_size));

    if (deviceIDin < 0 && deviceIDout < 0) {
        if ((err = Pa_OpenDefaultStream(&stream,
                                        fakeMonoMic ? 2 : inChannels,
                                        outChannels,
                                        fmt,
                                        sampRate,
                                        FRAMES_PER_BUFFER,
                                        my_callback,
                                        this)) != paNoError)
            throw err;
    } else {
        PaStreamParameters in, out;
        in.device = out.device = 0;
        in.channelCount = fakeMonoMic ? 2 : inChannels;
        out.channelCount = outChannels;
        in.sampleFormat = out.sampleFormat = fmt /*| paNonInterleaved*/;
        in.suggestedLatency = out.suggestedLatency = 0.010; //10ms
        in.hostApiSpecificStreamInfo = out.hostApiSpecificStreamInfo = 0;

        if ((err = Pa_OpenStream(&stream,
                                 &in, &out,
                                 sampRate,
                                 FRAMES_PER_BUFFER,  /* frames per buffer */
                                 0,     /* stream flags */
                                 my_callback,
                                 this)) != paNoError)
            throw err;
    }
#endif

    if ((err = Pa_StartStream(stream)) != paNoError)
        throw err;

    /* we are now running... */
    DODEBUG(NOTICE, ("PortAudio Subsystem running OK"));
    local_ready = true;
}

void PaAudioSystemImpl::listDevices() {
    (void)Pa_Initialize();
#ifndef PAV19 /*FOLD00*/
    int numdevices = 1;//Pa_CountDevices();
    int defaultin = 0;//Pa_GetDefaultInputDeviceID();
    int defaultout = 0;//Pa_GetDefaultOutputDeviceID();

    fprintf(stderr, "Counted %d device%s. Device #0 for default (?).\n", numdevices, numdevices == 1 ? "" : "s");
    for (int i = 0; i < numdevices; ++i) {
        const PaDeviceInfo* d = Pa_GetDeviceInfo(i);
        fprintf(stderr,
                "Device #%d%s%s\n"
                "\tstructVersion   : %d\n"
                "\tname            : %s\n"
                "\tmaxInputChannels: %d\n"
                "\tmaxOutputChans  : %d\n"
                "\tsampleFormats   : %lx\n"
                "\tsampleRates     : ",
                i,
                (i == defaultin  ? " (Default Input)"  : ""),
                (i == defaultout ? " (Default Output)" : ""),
                d->structVersion, d->name,
                d->maxInputChannels, d->maxOutputChannels,
                d->nativeSampleFormats);
        if (d->numSampleRates < 0) {
            fprintf(stderr,
                    "%.0f -- %.0f\n",
                    d->sampleRates[0], d->sampleRates[1]);
        } else {
            if (d->numSampleRates > 0)
                fprintf(stderr, "%.0f", d->sampleRates[0]);
            for (int j = 1; j < d->numSampleRates; ++j) {
                fprintf(stderr, ", %.0f", d->sampleRates[j]);
            }
            fprintf(stderr, "\n");
        }
    }
#else /*FOLD00*/
    PaHostApiIndex NUM_APIS = Pa_GetHostApiCount();
    fprintf(stderr, "Getting devices available for PortAudio v19: version string = %s\n", Pa_GetVersionText());
    fprintf(stderr, "There are %d subhost APIs, the default is %d. They are:\n", (int)NUM_APIS, (int)Pa_GetDefaultHostApi());
    for (PaHostApiIndex i = 0; i < NUM_APIS; ++i) {
        const PaHostApiInfo* api = Pa_GetHostApiInfo(i);
        fprintf(stderr,
                "\t#%d: %s(%d), %d devices, default input/output is %d/%d. The devices are:\n",
                (int)i, api->name, (int)api->type,
                api->deviceCount, (int)api->defaultInputDevice, (int)api->defaultOutputDevice);
        for (int j = 0; j < api->deviceCount; ++j) {
            PaDeviceIndex devid = Pa_HostApiDeviceIndexToDeviceIndex(i, j);
            const PaDeviceInfo *dev = Pa_GetDeviceInfo(devid);
            fprintf(stderr,
                    "\t\t#%d => %d, %s, max input/output channels is %d/%d,\n"
                    "\t\t\tdefault samplerate is %fHz, latency [%f,%f]/[%f,%f] seconds\n",
                    j, devid, dev->name, dev->maxInputChannels, dev->maxOutputChannels,
                    dev->defaultSampleRate, dev->defaultLowInputLatency, dev->defaultHighInputLatency,
                    dev->defaultLowOutputLatency, dev->defaultHighOutputLatency);
        }
    }
    fprintf(stderr,
            "The default input/output device (subhost unspecified) is %d/%d\n\n",
            (int)Pa_GetDefaultInputDevice(), (int)Pa_GetDefaultOutputDevice());
#endif
}
