/* $Id$ $URL$ */
#include "taptaudio.h"

/**\file taptaudio_disabled.cpp
 * Implementation of the Audio subsystem that does zippo -- AUDIO is DISABLED!
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

#include "taptdebug.h"

AudioSystem::AudioSystem(AUDIO_FORMAT /*format*/,
                         double /*sampleRate*/,
                         int /*inputChannels*/,
                         int /*outputChannels*/,
                         int /*deviceIDin*/,
                         int /*deviceIDout*/,
                         const AUDIO_BACKEND /*backends*/[]
                        )
    :
    impl(0)
{
    DODEBUG(NOTICE, ("Audio is disabled."));
}

void AudioSystem::waitstop() {
}


void AudioSystem::listDevices() {
}
bool AudioSystem::freeSample(ASSample *) {
    return false;
}
AudioSystem* AudioSystem::get() {
    return 0;
}
void AudioSystem::setDebugLevel(unsigned ) {

}

bool AudioSystem::isRunning() {
    return false;
}


AudioSystem::~AudioSystem() {}

ASSample* AudioSystem::loadSample(const std::string &, bool) {
    return 0;
}
ASSample* AudioSystem::loadSample(const char*, bool) {
    return 0;
}
bool AudioSystem::loopSample(ASSample *, float , unsigned* ) {
    return false;
}
unsigned AudioSystem::stopSample(ASSample *, int) {
    return 0;
}
unsigned AudioSystem::pauseSample(ASSample *, int, bool) {
    return 0;
}
unsigned AudioSystem::unpauseSample(ASSample *, int ) {
    return 0;
}

unsigned AudioSystem::setVolume(ASSample *, float, int) {
    return 0;
}

ASSample* AudioSystem::loadRawSample(const std::string &, void* , unsigned long ) {
    return 0;
}

bool AudioSystem::mixSample(ASSample * /*samp*/, bool /*record_after*/,
               double /*record_size = 30.0*/, float /*vol = 1.0f*/,
               unsigned* /*trackno = 0*/) {
    return 0;
}

bool AudioSystem::mixSample(ASSample * /*samp*/, float /*vol = 1.0f*/, unsigned* /*trackno = 0*/) {
    return 0;
}


bool mixSample(ASSample * /*samp*/, bool /*record_after*/, double /*record_size = 30.0*/,
               float /*vol = 1.0f*/, unsigned* /*trackno = 0*/) {
    return 0;
}

bool AudioSystem::startRec(double, unsigned int*, ASSample** ) {
    return 0;
}

bool AudioSystem::startRec(unsigned int*, ASSample** ) {
    return 0;
}


bool AudioSystem::streamRec(const char *, const RecordOpts &, unsigned *, ASSample **) {
    return 0;
}

bool AudioSystem::streamRec(const char *, FILE_ENCODING , float , unsigned *, ASSample **) {
    return 0;
}

bool AudioSystem::streamRec(const char *, FILE_ENCODING , unsigned *, ASSample **) {
    return 0;
}

bool AudioSystem::streamRec(const std::string &, const RecordOpts &, unsigned *, ASSample **) {
    return 0;
}
bool AudioSystem::streamRec(const std::string &, FILE_ENCODING , float , unsigned *, ASSample **) {
    return 0;
}

bool AudioSystem::streamRec(const std::string &, FILE_ENCODING , unsigned *, ASSample **) {
    return 0;
}

ASSample* AudioSystem::stopRec(const char* , bool , bool , FILE_ENCODING, float) {
    return 0;
}

ASSample* AudioSystem::stopRec(const char* , ASSample *, int , const AudioSystem::RecordOpts &) {
    return 0;
}

ASSample* AudioSystem::stopRec(const char*, ASSample *, int , bool , bool , FILE_ENCODING , float ) {
    return 0;
}

ASSample* AudioSystem::stopRec(const std::string &, bool , bool , FILE_ENCODING, float) {
    return 0;
}

ASSample* AudioSystem::stopRec(const std::string &, ASSample *, int , const AudioSystem::RecordOpts &) {
    return 0;
}

ASSample* AudioSystem::stopRec(const std::string &, ASSample *, int , bool , bool , FILE_ENCODING , float ) {
    return 0;
}

bool AudioSystem::isRecording() {
    return 0;
}

bool AudioSystem::map(const char* , ASSample *) {
    return 0;
}

bool AudioSystem::map(ASSample *) {
    return 0;
}

bool AudioSystem::map(const std::string &, ASSample *) {
    return 0;
}

void AudioSystem::stop() {
}
/*
unsigned long AudioSystem::dataSize(ASSample* ) {
    return 0;
}

const void* AudioSystem::getData(ASSample* ) {
    return 0;
}
*/
float AudioSystem::inputVU() const {
    return 0.0f;
}

bool AudioSystem::isPlaying(ASSample *, int) const {
    return 0;
}

bool AudioSystem::canPlay() const {
    return 0;
}

bool AudioSystem::canRecord() const {
    return 0;
}


void ASSample::mixed(unsigned long,
                       void*,
                       int,
					   const void *){}
float ASSample::getVU(){return 0;}
long ASSample::msDuration() const {return 0;}
double ASSample::getDuration() const{return 0;}
bool ASSample::split(unsigned long, ASSample **, ASSample **) const {return 0;}
ASSample* ASSample::destructive_split(unsigned long) {return 0;}
long ASSample::remove_silence(float, unsigned) {return 0;}
bool ASSample::save(const char*, FILE_ENCODING, float) {return 0;}
bool ASSample::save(const std::string &, FILE_ENCODING, float) {return 0;}

unsigned AudioSystem::AUDIO_TRACKS = 0;
AudioSystem* AudioSystem::instance = 0;

