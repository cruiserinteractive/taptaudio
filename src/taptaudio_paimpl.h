/* $Id$ $URL$ */
#ifndef TAPTAUDIO_PAIMPL_DOT_AITCH
#define TAPTAUDIO_PAIMPL_DOT_AITCH

#include "taptaudio_impl.h"

/**\file taptaudio_paimpl.h
 * Header for portaudio implementation of taptaudio
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

#ifndef PAV19
#include "portaudio.h"
#else
#include "portaudio_v19.h"
#endif

/* paNonInterleaved is only in v19 of portaudio */
#ifdef paNonInterleaved
#define PAV19
#endif

/**\def PAV19
 * If this is defined, we will try to use the version 1.9 of portaudio.
 * \note the API is very different -- we will look for the header at
 * portaudio_v19.h, wherever that may be..
 */

/** The portaudio implementation of the audio system */
class PaAudioSystemImpl : public AudioSystemImpl {
#ifndef PAV19
    PortAudioStream *stream;       ///< The portaudio strem handle
    typedef PaTimestamp Timestamp; ///< The type used for time stamps in this version of portaudio
#else
    PaStream *stream;             ///< Portaudio stream handle
    typedef PaTime Timestamp;     ///< Portaudio timestamp type
#endif

public:
    /** Virtual destructor, free the portaudio handles and clear the registry (?) */
    virtual ~PaAudioSystemImpl();

    /** Create an AudioSystemImpl */
    PaAudioSystemImpl(unsigned inputChannels = 1,
                      unsigned outputChannels = 2,
                      double sampleRate = 44100,
                      AUDIO_FORMAT format = AF_Int16,
                      unsigned subhost = AF_PORTAUDIO,
                      int deviceIDin = -1,
                      int deviceIDout = -1);

    virtual void cease();

    static void listDevices();
};

/**
 * A portaudio implementation of AudioSystemMixer that mixes any number of tracks
 * with appropriate clipping.
 */
class PaAudioSystemMixer : public PaAudioSystemImpl, public AudioSystemMixer {
public:
    PaAudioSystemMixer(unsigned inputChannels = 1,
                       unsigned outputChannels = 2,
                       double sampleRate = 44100,
                       AUDIO_FORMAT format = AF_Int16,
                       unsigned ttracks = 32,
                       unsigned rtracks = 8,
                       unsigned subhost = AF_PORTAUDIO,
                       int deviceIDin = -1,
                       int deviceIDout = -1);
    virtual ~PaAudioSystemMixer() {}
};

/**
 * Templatized portaudio version of AudioSystemMixerT
 */
template <class T>
class PaAudioSystemMixerT : public virtual PaAudioSystemImpl, public AudioSystemMixerT<T> {
public:

INHERIT_MIXER_FUNCTIONS

    PaAudioSystemMixerT(unsigned inputChannels = 1,
                        unsigned outputChannels = 2,
                        double sampleRate = 44100,
                        AUDIO_FORMAT format = AF_Int16,
                        unsigned ttracks = 32,
                        unsigned rtracks = 8,
                        unsigned subhost = AF_PORTAUDIO,
                        int deviceIDin = -1,
                        int deviceIDout = -1)
        :

PaAudioSystemImpl(inputChannels, outputChannels, sampleRate, format, subhost, deviceIDin, deviceIDout)
    ,
        AudioSystemMixer(this, format, ttracks, rtracks)
        ,
        AudioSystemMixerT<T>(this, format, ttracks, rtracks)
    {
    }
};

#endif
