# Use -l/path/to/lib instead of -L/path/to -llib when full path is known. This
# prevents ambiguity when adding -lanotherlib (with no path).
cmake_policy(SET CMP0003 NEW)

FIND_PATH(SNDFILE_INCLUDE_DIR sndfile.h PATHS ${mypaths} PATH_SUFFIXES)
FIND_LIBRARY(SNDFILE_LIBRARY sndfile PATHS ${mypaths} PATH_SUFFIXES lib64 lib)

IF (TaptAudio/Audio)
  add_subdirectory(threadman)
ENDIF ()

SET (common_sources 
     taptdebug.cpp taptdebug.h record_options.cpp
)

SET (enabled_sources
     ${common_sources}
     taptaudio.cpp sample.h sample.cpp
     taptaudio_impl.h taptaudio_impl.cpp taptaudio_generic.cpp
     ringbuffer/pa_ringbuffer.h ringbuffer/pa_ringbuffer.c
     ringbuffer/ringbuf.h ringbuffer/ringbuf.cpp
     streaming.cpp streaming.h
)

SET (disabled_sources ${common_sources} taptaudio_disabled.cpp)
SET (portaudio_sources taptaudio_paimpl.h taptaudio_paimpl.cpp)
SET (jack_sources taptaudio_jackimpl.h taptaudio_jackimpl.cpp)
SET (resample_sources taptresample.h taptresample.cpp)

add_definitions(-DHAVE_CONFIG_H)
SET (base_includes "${PROJECT_BINARY_DIR}/config" ${taptaudio_SOURCE_DIR}/include)
SET (base_libs ${SDL_LIBRARY})

IF (TaptAudio/Audio)
  SET (base_includes ${base_includes} ${SDL_INCLUDE_DIR} ${SNDFILE_INCLUDE_DIR})
  SET (base_sources ${enabled_sources})
  SET (base_libs ${base_libs} threadman ${SNDFILE_LIBRARY})
  IF (TaptAudio/JACK)
    FIND_LIBRARY(JACK_LIBRARY jack PATHS ${mypaths} PATH_SUFFIXES lib64 lib)
    SET (base_sources ${base_sources} ${jack_sources})
    SET (base_libs ${base_libs} ${JACK_LIBRARY})
  ENDIF ()
  IF (TaptAudio/PortAudio)
    find_path (PORTAUDIO_INCLUDE_DIR portaudio.h PATHS ${mypaths} PATH_SUFFIXES include)
    FIND_LIBRARY(PORTAUDIO_LIBRARY portaudio PATHS ${mypaths} PATH_SUFFIXES lib64 lib)
    if (PORTAUDIO_INCLUDE_DIR AND PORTAUDIO_LIBRARY)
        mark_as_advanced (PORTAUDIO_INCLUDE_DIR PORTAUDIO_LIBRARY)
    endif ()
    SET (base_includes ${base_includes} ${PORTAUDIO_INCLUDE_DIR})
    SET (base_sources ${base_sources} ${portaudio_sources})
    SET (base_libs ${base_libs} ${PORTAUDIO_LIBRARY})
  ENDIF ()
  if (TaptAudio/Resample)
    find_path (Libresample_INCLUDE_DIR libresample.h PATHS ${taptaudio_SOURCE_DIR}/ext/libresample/include)
    find_library (Libresample_LIBRARY resample)
    SET (base_includes ${base_includes} ${Libresample_INCLUDE_DIR})
    SET (base_sources ${base_sources} ${resample_sources})
    SET (base_libs ${base_libs} ${Libresample_LIBRARY})
  ENDIF ()
ELSE()
  SET (base_sources ${disabled_sources})
ENDIF ()


INCLUDE_DIRECTORIES (${base_includes})

IF(NOT MSVC)
  ADD_DEFINITIONS(-Drestrict=__restrict)
ENDIF(NOT MSVC)

add_library (taptaudio SHARED ${base_sources})
target_link_libraries(taptaudio ${base_libs})
SET_TARGET_PROPERTIES(taptaudio PROPERTIES 
                      SOVERSION 0.1.0
                      COMPILE_FLAGS "-fPIC")
IF (APPLE)
  IF (TaptAudio/RpathIsPrefix)
    SET_TARGET_PROPERTIES(taptaudio PROPERTIES
                          INSTALL_NAME_DIR "@rpath/lib")
  ENDIF ()
ELSE (APPLE)
  SET_TARGET_PROPERTIES(taptaudio PROPERTIES
                        LINK_FLAGS -Wl,--no-undefined)
ENDIF (APPLE)

if (TaptAudio/InstallDevFiles)
    set (archive_dest ARCHIVE DESTINATION sdk/lib)
else ()
    set (archive_dest)
endif ()

INSTALL(TARGETS taptaudio 
        LIBRARY DESTINATION lib
        RUNTIME DESTINATION bin
        ${archive_dest})

#EXTRA_DIST = $(enabled_sources) taptaudio_disabled.cpp $(portaudio_sources) $(jack_sources) $(resample_sources)
