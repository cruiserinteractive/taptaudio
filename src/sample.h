/* $Id$ $URL$ */
#ifndef SAMPLE_DOT_AITCH
#define SAMPLE_DOT_AITCH

#ifndef __USE_STRING_INLINES
#define __USE_STRING_INLINES
#endif
#include <string.h>

#include "taptaudio.h"

/**\file sample.h
 * Decs for PCM Audio Samples
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

#include <SDL.h>
#include <limits>
#include <string>
#include <typeinfo>

#include "sndfile.h"
#include "taptdebug.h"

#ifdef ENABLE_RESAMPLE
#include "taptresample.h"
#endif

static const unsigned FORMAT_SIZE[] = {
    0,//AF_Default,     ///< Default format (Int16)
    4,//AF_Float32,     ///< 32-bit floats
    2,//AF_Int16,       ///< 16-bit signed ints
    4,//AF_Int32,       ///< 32-bit signed ints
    3,//AF_Int24,       ///< NFI - not supported
    3,//AF_PackedInt24, ///< NFI - not supported
    1,//AF_Int8,        ///< 8-bit signed ints
    1,//AF_UInt8,       ///< 8-bit unsigned ints (not supported)
    0,//AF_CustomFormat ///< Some custom format (not supported)

};

/** An ASSample using Pulse Code Modulation (PCM) */
class PCMSample : public ASSample {
    //PCMSample (const PCMSample&);
    PCMSample& operator=(const PCMSample&); //not defined
public:
    static float updateVU(const float *buffer, unsigned long buf_siz, float old_level);

protected:
    sf_count_t writen(SNDFILE *sf, void* bdata, unsigned long nbytes) const;
    SNDFILE* open(const std::string &path, FILE_ENCODING enc = AF_ENC_WAV, float quality = 2.0, int mode = SFM_RDWR) const;
public:
    void *data;               ///< The audio data
    int channels;             ///< The number of channels in the sample
private: //must be set with tellSize
    unsigned long bytes_size; ///< The size of \a data
    unsigned long nframes;    ///< Number of frames in the entire sample
protected:
    void tellSize(unsigned long newsize) {
        bytes_size = newsize;
        nframes = newsize / (channels*data_sz);
    }
public:
    unsigned long bSize() const {
        return bytes_size;
    }
    unsigned long frames() const {
        return nframes;
    }
    double rate;              ///< The sample rate
    AUDIO_FORMAT fmt;         ///< The audio format
    unsigned data_sz;         ///< The size of the audio format

    float vu;                 ///< The current VU measure

    /** Protected constructor for subclasses */
    PCMSample(AUDIO_FORMAT format = AudioSystem::DEFAULT_AUDIO_FORMAT)
        :
    data(0), channels(0), bytes_size(0), nframes(0), rate(0),
        fmt(format), data_sz(FORMAT_SIZE[fmt]), vu(0) {}

public:
    PCMSample(void* data_, unsigned channels_, unsigned long bytes_size_, double rate_, AUDIO_FORMAT fmt_)
        :
    data(data_), channels(channels_),
        bytes_size(bytes_size_), nframes(bytes_size / (channels * data_sz)),
        rate(rate_), fmt(fmt_),
        data_sz(FORMAT_SIZE[fmt]), 
        vu(0)
    {}

    virtual void inherit_sfinfo(const SF_INFO &info, unsigned auto_sz, AUDIO_FORMAT forceformat = AudioSystem::DEFAULT_AUDIO_FORMAT);
    virtual bool split(unsigned long where, ASSample **start, ASSample **end) const;
    virtual ASSample *destructive_split(unsigned long where);

    virtual unsigned getChannels() const {return channels;}
    virtual AUDIO_FORMAT getFormat() const {return fmt;}
    virtual unsigned long numBytes() const {return bytes_size;}
    virtual unsigned long numFrames() const {return bytes_size / (data_sz * channels);}
    virtual const void* getBytes() const {return data;}
    virtual const void* getOffBytes(unsigned long offset, unsigned long &available) const {
        long remain = numBytes() - offset;
        if (remain > 0) {
            available = remain;
            return static_cast<const Uint8*>(getBytes()) + offset;
        } else {
            available = 0;
            return 0;
        }
    }
    virtual double getSamRate() const {return rate;}
    virtual bool resample(double /*newrate*/, bool /*highquality*/ = false) {return false;}
    virtual bool must_register() const {return true;}

    virtual float getVU() {return vu;}
    virtual void mixed(unsigned long frames,
                       void* handle = 0,
                       int channelOffset = -1,
                       const void *data = 0);

    static void mono2stereo(void *newdata,
                            const void *olddata,
                            unsigned long olddata_bytes,
                            unsigned oldframesize);

    static void stereo2mono(Uint8 *newdata,
                            const Uint8 *olddata,
                            unsigned long olddata_bytes,
                            unsigned framesize);

    static inline void interleave(Uint8 *outp,
                                  const Uint8 *inp,
                                  const unsigned long num_bytes,
                                  const unsigned data_sz,
                                  const unsigned channels,
                                  const unsigned channelOffset) {

        const unsigned outskip = channels*data_sz;
        const Uint8 * const end = inp + num_bytes;

        Uint8 *p = outp + channelOffset*data_sz;

        for  (; inp < end; inp += data_sz, p += outskip)
            memcpy(p, inp, data_sz);
    }

    virtual bool save(const std::string &path, FILE_ENCODING enc = AF_ENC_WAV, float quality = 2.0);
    virtual long remove_silence(float threshold, unsigned window);

protected:
    template <class T> static inline T upper_limit(const float &threshold) {
        return static_cast<T>(std::numeric_limits<T>::max() * threshold + 0.499);
    }
    template <class T>
        static long remove_silence(T** dp, PCMSample *s, float threshold, unsigned window);
};

template <> inline float PCMSample::upper_limit(const float &threshold) {
    return threshold;
}
template <> inline double PCMSample::upper_limit(const float &threshold) {
    return threshold;
}

/** A PCMSample loaded from a file (e.g. .wav)
 * Will always load the format that is native to the sound <em>file</em>.
 */
class FileSample : public PCMSample {
protected:
    virtual void load_sndfile(SNDFILE* sf, SF_INFO &sfinfo, bool forcestereo);
    virtual ~FileSample();
    FileSample(AUDIO_FORMAT fmt_) : PCMSample(fmt_) {}
public:
    FileSample (const std::string& path, bool forcestereo = false);
//    FileSample (const Uint8* data, const Uint32 bytes);
};

/** A FileSample that reads from the file using the specified type (i.e. not
 * the native type for the file).
 */
template <class T>
class FileSampleT : public FileSample {
protected:
    virtual ~FileSampleT();
    virtual void loadcon_sndfile(SNDFILE* sf, SF_INFO &sfinfo, bool forcestereo, AUDIO_FORMAT force_format = AudioSystem::DEFAULT_AUDIO_FORMAT);
    FileSampleT (AUDIO_FORMAT fmt_) : FileSample(fmt_) {}
public:
    FileSampleT (const std::string& path, bool forcestereo = false, AUDIO_FORMAT force_format = AudioSystem::DEFAULT_AUDIO_FORMAT);
    T** tdatap();// {return reinterpret_cast<T**>(&data);}
    T* tdata();// {return reinterpret_cast<T*>(data);}
    virtual bool resample(double newrate, bool highquality = false);
    virtual long remove_silence(float threshold, unsigned window);
};

template <class T>
T** FileSampleT<T>::tdatap() {return reinterpret_cast<T**>(&data);}

template <class T>
    T* FileSampleT<T>::tdata() {return reinterpret_cast<T*>(data);}

/** A PCMSample recorded from audio input (microphone) */
class Recording : public PCMSample {
protected:
    virtual ~Recording();
    unsigned long reserved;
public:
    Recording(unsigned long initial_size = 28672,
              unsigned chans = 2,
              AUDIO_FORMAT format = AF_Int16,
              double samplerate = 44100);

    /**
     * Append -- note that this cannot be called in the audio thread --
     * it occurs at a kernel interrupt level and we cannot allocate memory there!
     */
    void append(void *more, unsigned long sz);

    virtual bool must_register() const {return false;}
    virtual void fill(const void *more, unsigned long sz, int channelOffset = -1, int bufChannels = 1);
    void skipfill(const void *more, unsigned long sz, int channelOffset = -1);
    virtual void trim();
    virtual void fillSilence(unsigned long sz);

    virtual bool isFull() {return false;}
    virtual ASSample *makePlayable(bool del_if_new = true);
    virtual bool canSaveInThread(bool /*want_to_reuse*/) {return true;}

};

template <class T>
FileSampleT<T>::FileSampleT(const std::string &path, bool forcestereo, AUDIO_FORMAT force_format)
: FileSample(force_format)
{
    //data_sz = FORMAT_SIZE[fmt]; ///\todo shouldn't have to do this! // ineffective here
    SF_INFO sfinfo;
    memset(&sfinfo, 0, sizeof(sfinfo));
    SNDFILE* sf = 0;
	sf = sf_open(path.c_str(), SFM_READ, &sfinfo);
    if (sf) {
        try {
            loadcon_sndfile(sf, sfinfo, forcestereo, force_format);
            DODEBUG(FUNCTION, ("Successfully loaded/converted %s {fmt = %d, data_sz = %d, channels = %d, sz[fmt] = %d}",
                               path.c_str(), (int)fmt, (int)data_sz, (int)channels, (int)FORMAT_SIZE[fmt]));
        } catch (...) {
            DODEBUG(ERROR, ("Unsupported frame encoding format in %s\n\tcurrently we only support signed primitive types",
                            path.c_str()));
            sf_close(sf);
            throw;
        }
        sf_close(sf);
    } else {
        DODEBUG(ERROR, ("Unable to open %s: %s",
                        path.c_str(), sf_strerror(sf)));
        throw "Unable to open file";
    }
}


/**
 * A templatized version of libsndfile's sf_read() -- read sound
 * data from an audio file.
 *
 * \param sndfile the (opened) sound file handle
 * \param data the target buffer to store the data read from the file. Must
 *        have space allocated for at least sizeof(data)*items bytes
 * \param items the number of items to read
 */
template <class TT>
sf_count_t lsf_read(SNDFILE *sndfile, TT** data, sf_count_t items) {
    *data = new TT[items];
    return sf_read_raw(sndfile, *data, sizeof(TT) * items);
}

template <>
    sf_count_t lsf_read(SNDFILE *sndfile, short** data, sf_count_t items);
template <>
    sf_count_t lsf_read(SNDFILE *sndfile, int** data, sf_count_t items);
template <>
    sf_count_t lsf_read(SNDFILE *sndfile, float** data, sf_count_t items);
template <>
    sf_count_t lsf_read(SNDFILE *sndfile, double** data, sf_count_t items);


template <class TT>
inline sf_count_t lsf_aread(SNDFILE *sndfile, TT* data, sf_count_t items) {
    return sf_read_raw(sndfile, data, sizeof(TT) * items);
}
template <>
inline sf_count_t lsf_aread(SNDFILE *sndfile, short* data, sf_count_t items) {
    return sf_read_short(sndfile, data, items);
}
template <>
inline sf_count_t lsf_aread(SNDFILE *sndfile, int* data, sf_count_t items) {
    return sf_read_int(sndfile, data, items);
}
template <>
inline sf_count_t lsf_aread(SNDFILE *sndfile, float* data, sf_count_t items) {
    return sf_read_float(sndfile, data, items);
}
template <>
inline sf_count_t lsf_aread(SNDFILE *sndfile, double* data, sf_count_t items) {
    return sf_read_double(sndfile, data, items);
}

#ifndef CAFSUPPORT
/* old versions of sndfile.h don't declare sf_writef_* with const input buffers */
#define CC(x_, t_) (const_cast<t_*>((x_)))
#else
#define CC(x_, t_) (x_)
#endif


/**
 * A templatized version of libsndfile's sf_write() -- write sound
 * data to an audio file.
 *
 * \param sndfile the (opened) sound file handle
 * \param data the source buffer (of size at least sizeof(data)*items bytes
 * \param items the number of items to read
 * \param channels, ignored except for raw
 */
template <class TT>
sf_count_t lsf_writef(SNDFILE *sndfile, const TT* data, sf_count_t items, int channels) {
    DODEBUG(VINFO, ("\tWriting >RAW< %d frames of %s from memory", (int)items, typeid(*data).name()));
    return sf_write_raw(sndfile, CC(data, TT), sizeof(TT) * items * channels);
}

template <>
    sf_count_t lsf_writef(SNDFILE *sndfile, const short* data, sf_count_t items, int channels);
template <>
    sf_count_t lsf_writef(SNDFILE *sndfile, const int* data, sf_count_t items, int channels);
template <>
    sf_count_t lsf_writef(SNDFILE *sndfile, const float* data, sf_count_t items, int channels);
template <>
    sf_count_t lsf_writef(SNDFILE *sndfile, const double* data, sf_count_t items, int channels);

template <class T>
void FileSampleT<T>::loadcon_sndfile(SNDFILE* sf, SF_INFO &sfinfo, bool forcestereo, AUDIO_FORMAT force_format) {
    T** tdat = tdatap();
    inherit_sfinfo(sfinfo, sizeof(T), force_format);
    if (sizeof(T) != FORMAT_SIZE[fmt]) {
        DODEBUG(ERROR, ("Format size mismatch! sizeof(%s) != FORMAT_SIZE[%d] (%u != %u)",
                        typeid(T).name(), (int)fmt, (unsigned)sizeof(T), (unsigned)FORMAT_SIZE[fmt]));
    }
    sf_count_t numread = lsf_read(sf, tdat, channels*sfinfo.frames);

    if (numread != channels * sfinfo.frames) {
        //error -- do something?
        DODEBUG(WARNING, ("numread != channels * sfinfo.frames == %lu != %d * %lu",
                          (unsigned long)numread, channels, (unsigned long)sfinfo.frames));
    }

    data_sz = FORMAT_SIZE[fmt]; ///\todo shouldn't have to do this here!
    if (channels == 1 && forcestereo) {
        T *newdata = new T[sfinfo.frames * 2];
        mono2stereo(newdata, (void*)(*tdat), bSize(), channels * sizeof(T) /*frame_sz *//* == sizeof(T) */);
        //delete[] tdata;
        *tdat = newdata;
        channels = 2;
        tellSize(bSize() * 2);
    }
    data_sz = FORMAT_SIZE[fmt]; ///\todo shouldn't have to do this here!
}

template <class T>
FileSampleT<T>::~FileSampleT() {
    delete[] static_cast<T*>(data);
    data = 0;
}

template <class T>
bool FileSampleT<T>::resample(double newrate, bool highquality) {
#ifdef ENABLE_RESAMPLE
    //T** tdata = reinterpret_cast<T**>(&data);
    unsigned long newnframes =
        template_resample(tdatap(),
                          channels,
                          numFrames(),
                          rate,
                          newrate,
                          highquality);
    if (newnframes) {
        DODEBUG(FUNCTION, ("Resampled from %fHz to %fHz -- now %lu frames",
                           rate, newrate, newnframes));
        rate = newrate;
        tellSize(data_sz * channels * newnframes);
        return true;
    }
    DODEBUG(WARNING, ("Resample failed -- maybe not supported for type (only float supported)"));
#else
    DODEBUG(NOTICE, ("TappedAudio was not compiled with resampling support"));
#endif
    return false;
}

template <class T>
long FileSampleT<T>::remove_silence(float threshold, unsigned window) {
    return PCMSample::remove_silence(tdatap(), this, threshold, window);
}

template <class T>
long PCMSample::remove_silence(T** dp, PCMSample *s, float threshold, unsigned window) {
    const T upper = upper_limit<T>(threshold);
    const T lower = -upper;
    const unsigned CHAN = s->channels;
    unsigned long n = 0;
    //T** dp = tdatap();
    T* p = *dp;
    T* o = *dp;
    T* const PEND = p + s->frames()*CHAN;
    while (p < PEND) {
        bool silent = true;
        for (unsigned c = 0; silent && c < CHAN; ++c)
            silent = p[c] < upper && p[c] > lower;
        n = silent ? n+1 : 0;
        if (n < window) { /* shift up */
            T* const CEND = p + CHAN;
            while (p < CEND)
                *o++ = *p++;
        } else { /* remove silence (don't copy) */
            p += CHAN;
        }
    }
    if (o != p) { /* resize and replace */
        T* old = *dp;
        s->tellSize((o-old)*sizeof(T));
        //nframes = (o-old) / CHAN;
        *dp = new T[s->frames()*CHAN];
        memcpy(*dp, old, s->bSize());
        delete[] old;
    }
    DODEBUG(FUNCTION, ("Silence removal: removed %lu frames, now %lu", (unsigned long)((p-o)/CHAN), s->frames()));
    return (p-o)/CHAN; /* number of frames removed */
}

#endif
