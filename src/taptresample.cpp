/* $Id$ $URL$ */
#include "taptresample.h"

/**\file taptresample.cpp
 * Definitions for resample function
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Rev$
 * $Date$
 */

#include "libresample.h"

namespace {

const unsigned long PADDING = 15;
const char* lastError = "No Error";
typedef unsigned long frat;

void deinterlace(float *pdest, const float *usrc, const frat frames, const unsigned channels) {
    const float* const end = pdest + frames;
    for (; pdest < end; ++pdest, usrc += channels)
        *pdest = *usrc;
}

void interlace(float *udest, const float *psrc, const frat frames, const unsigned channels) {
    const float* const end = psrc + frames;
    for (; psrc < end; ++psrc, udest += channels)
        *udest = *psrc;
}

frat dochannel(float *in_i,
               float *out_i,
               const unsigned long inframes,
               const unsigned long outframes,
               void *handle,
               const int channels,
               const double factor,
               const bool lastflag = true)
{
    float *in = in_i, *out = out_i;
    if (channels > 1) {
        in = new float[inframes];
        out = new float[outframes];
        deinterlace(in, in_i, inframes, channels);
    }
    int inbufferused;
    frat real_out =
        resample_process(handle,
                         factor,
                         in,
                         inframes,
                         lastflag,
                         &inbufferused,
                         out,
                         outframes);

    if ((long)inbufferused != (long)inframes)
        lastError = "Not enough padding -- not all input used";

    if (channels > 1) {
        interlace(out_i, out, outframes, channels);
        delete[] out;
        delete[] in;
    }
    return real_out;
}

frat doresample(float **data,
                const int channels,
                const unsigned long oldframes,
                const unsigned long newframes,
                void** handle,
                const double factor)
{
    frat f = 0;
    float *in = *data;
    float *out = new float[newframes*channels];
    for (int c = 0; c < channels; ++c) {
        frat nf = dochannel(in+c, out+c,
                      oldframes, newframes,
                      handle[c],
                      channels, factor,
                      true);
        if (f && f != nf) {
            lastError = "Output frames do not match -- using minimum";
            f = f < nf ? f : nf;
        } else {
            f = nf;
        }
    }
    delete[] in;
    *data = out;
    return f;
}
}


unsigned long taptresample(float **data,
                           int channels,
                           unsigned long frames,
                           double oldrate,
                           double newrate,
                           bool highquality)
{
    const double ratio = newrate / oldrate;
    void **handle = new void*[channels];
    for (int i = 0; i < channels; ++i)
        handle[i] = resample_open(highquality, ratio, ratio);
    frat ret = doresample(data, channels, frames,
                          static_cast<unsigned long>(frames * ratio + 0.5) + PADDING,
                          handle,
                          ratio);
    for (int i = 0; i < channels; ++i)
        resample_close(handle[i]);
    delete[] handle;
    return ret;
}

template <>
    unsigned long template_resample(float **data,
                                    int channels,
                                    unsigned long frames,
                                    double oldrate,
                                    double newrate,
                                    bool highquality) {
        return taptresample(data, channels, frames, oldrate, newrate, highquality);
    }

