/* $Id$ $URL$ */
#include "taptaudio.h"

/**\file record_options.cpp
 * RecordOptions Implementation bits
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

const RecordOpts AudioSystem::RO_DEFAULT = {true, true, true, AF_ENC_WAV, 2.0};
const RecordOpts AudioSystem::RO_DISCARD = {false, false, false, AF_ENC_WAV, 0.0};
const RecordOpts AudioSystem::RO_SAVEONLY = {false, true, true, AF_ENC_WAV, 2.0};

void RecordOptions::set (bool kar, bool sav, bool thread, FILE_ENCODING encod, float qual) {
    keep_and_return = kar;
    save = sav;
    save_in_thread = thread;
    enc = encod;
    quality = qual;
}

RecordOptions::RecordOptions (bool kar, bool sav, bool thread, FILE_ENCODING encod, float qual) {
    set(kar, sav, thread, encod, qual);
}

RecordOptions::RecordOptions (FILE_ENCODING encod, bool thread, bool kar, float qual) {
    set(kar, true, thread, encod, qual);
}

RecordOptions::RecordOptions (FILE_ENCODING encod, float qual) {
    set(false, true, true, encod, qual);
}
