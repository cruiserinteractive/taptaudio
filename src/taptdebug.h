/* $Id$ $URL$ */
#ifndef TAPTDEBUG_DOT_AITCH
#define TAPTDEBUG_DOT_AITCH

/**\file taptdebug.h
 * Explicit function-level tracing debug functionality (DebugLogger class)
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

#ifdef _MSC_VER
#define __func__ __FUNCDNAME__
#define __attribute__(x)
#endif

#if _MSC_VER < 1800  // MSVC<12
#define round floor
#endif

#ifdef __cplusplus
#include <stdexcept>
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

//This now works for icc > 9
//#ifdef __INTEL_COMPILER
//#define roundf rint
//#endif


/** Debugging flags */
enum DEBUG_FLAGS {
    DEBUG_NONE,
    DEBUG_ERROR,
    DEBUG_WARNING,
    DEBUG_NOTICE,
    DEBUG_FUNCTION,
    DEBUG_INFO,
    DEBUG_INTWARNINGS,
    DEBUG_INTERRUPT,
    DEBUG_VINFO,
    DEBUG_VINTERRUPT,
    DEBUG_ANY
};

/** The FILE* to output debugging statements to */
extern FILE* taptaudio_debug_file;

#ifndef TAPTDBGLEV
/** The debugging level, if not previously defined */
extern unsigned long TAPTDBGLEV;// = DEBUG_WARNINGS;
#endif

#ifndef NDEBUG

#include <cstdio>

/**
 * Debugging printf function
 */
int taptaudio_debug_printf(const char* fmt, ... /*args*/) __attribute__((format(printf, 1, 2))) ;

#define DODEBUG(level, args) do { if (TAPTDBGLEV >= DEBUG_ ## level) { \
    fprintf(taptaudio_debug_file, "[A%d] ", DEBUG_ ## level ) ; \
    taptaudio_debug_printf args ; \
    fprintf(taptaudio_debug_file, " (%s:%d in %s [%s])\n", __FILE__, __LINE__, __func__, #level ) ; \
    } } while (0)

#define SETDEBUG(level) TAPTDBGLEV = level

#ifndef OVERRIDE_EFENCE
# ifdef EFENCE_DEBUG
#  ifdef __cplusplus
#   include "efencepp.h"
#  else
#   include "efence.h"
#  endif
#  define EFENCE_INIT() static int init_ = fprintf(stderr, "[EF] %s init begin\n", __FILE__)
# else
#  define EFENCE_INIT() enum {}
# endif
#endif

#else

#define DODEBUG(level, args)
#define SETDEBUG(level)

#endif

/**\def DODEBUG
 *
 * Print a debugging statement if the debugging level \a level is enabled.
 * args needs to be in the form '("printf-format", ... / * args * /)' without
 * the single quotes (but with the parentheses).
 */

/**\def EFENCE_INIT
 * Print a message to stderr on static initialisation if EFENCE_DEBUG is enabled
 */

/**\def TAPTDBGLEV
 * If this is defined, use this instead of the default debugging level
 */

/**\def OVERRIDE_EFENCE
 * If defined we will not do the default efence debugging initialisations
 */

/**\def SETDEBUG
 * Set the run-time debugging level to \a level
 */

#endif
