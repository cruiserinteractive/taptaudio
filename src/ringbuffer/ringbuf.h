/* $Id$ */
#ifndef RINGBUF_DOT_AITCH
#define RINGBUF_DOT_AITCH

/**\file ringbuf.h
 * Declaration of RingBuffer class
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Rev$
 * $Date$
 */

#include <stdexcept>
#include <new>

struct PaUtilRingBuffer;
typedef struct PaUtilRingBuffer PaUtilRingBuffer;

/**
 * A Ring Buffer
 */
class RingBuffer {
public:
    typedef size_t size_type;
    typedef char byte;
private:
    RingBuffer(const RingBuffer&);
    RingBuffer& operator=(const RingBuffer&);
    PaUtilRingBuffer *purb;
    void init(byte *buf);
public:
    explicit RingBuffer(size_type sz) throw (std::bad_alloc, std::invalid_argument);

    size_type read(byte* buf, size_type sz) throw();
    size_type write(const byte* buf, size_type sz) throw();
    size_type max_size() const throw();

    size_type read_avail() const throw();
    size_type write_avail() const throw();

    size_type read_advance(size_type n) throw();
    size_type write_advance(size_type n) throw();

    size_type read_regions(byte **b1, size_type *sz1,
                           byte **b2, size_type *sz2,
                           size_type desired) const throw();

    size_type write_regions(byte **b1, size_type *sz1,
                            byte **b2, size_type *sz2,
                            size_type desired) throw();

    const byte* bufferStart() const throw();

    ~RingBuffer();
};

#endif

