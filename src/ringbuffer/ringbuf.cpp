/* $Id$ */
#include "ringbuf.h"

/**\file ringbuf.cpp
 * Definitions for C++ Ring Buffer
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Rev$
 * $Date$
 */

#include <string.h>
#include "pa_ringbuffer.h"

RingBuffer::RingBuffer(const RingBuffer&) {}
RingBuffer& RingBuffer::operator=(const RingBuffer&) {return *this;}

RingBuffer::RingBuffer(RingBuffer::size_type sz) throw (std::bad_alloc, std::invalid_argument)
    : purb(0)
{
    byte *buf = 0;
    try {
        if (((sz-1) & sz) != 0)
            throw std::invalid_argument("RingBuffer size not a power of 2");

        purb = new PaUtilRingBuffer();
        buf = new byte[sz];
        memset(purb, 0, sizeof(*purb));

        if (PaUtil_InitializeRingBuffer(purb, sz, buf) != 0)
            throw std::invalid_argument("InitializeRingBuffer didn't return 0 -- size not a power of 2 not detected?!");

    } catch (...) {
        delete purb;
        delete[] buf;
        throw;
    }
}

RingBuffer::~RingBuffer() {
    delete[] purb->buffer;
    delete purb;
}

RingBuffer::size_type RingBuffer::max_size() const throw() {
    return purb->bufferSize;
}

const RingBuffer::byte* RingBuffer::bufferStart() const throw() {
    return purb->buffer;
}

RingBuffer::size_type RingBuffer::read(byte* buf, RingBuffer::size_type sz) throw() {
    return PaUtil_ReadRingBuffer(purb, buf, sz);
}

RingBuffer::size_type RingBuffer::write(const byte* buf, RingBuffer::size_type sz) throw() {
    return PaUtil_WriteRingBuffer(purb, buf, sz);
}

RingBuffer::size_type RingBuffer::read_avail() const throw() {
    return PaUtil_GetRingBufferReadAvailable(purb);
}

RingBuffer::size_type RingBuffer::write_avail() const throw() {
    return PaUtil_GetRingBufferWriteAvailable(purb);
}

RingBuffer::size_type RingBuffer::read_advance(RingBuffer::size_type n) throw() {
    return PaUtil_AdvanceRingBufferReadIndex(purb, n);
}

RingBuffer::size_type RingBuffer::write_advance(RingBuffer::size_type n) throw() {
    return PaUtil_AdvanceRingBufferWriteIndex(purb, n);
}

RingBuffer::size_type RingBuffer::read_regions(byte **b1, RingBuffer::size_type *sz1,
                                               byte **b2, RingBuffer::size_type *sz2,
                                               RingBuffer::size_type desired) const throw() {
    long s1, s2, res;
    res = PaUtil_GetRingBufferReadRegions(purb, desired,
                                          reinterpret_cast<void**>(b1), &s1,
                                          reinterpret_cast<void**>(b2), &s2);
    *sz1 = s1;
    *sz2 = s2;
    return res;
}

RingBuffer::size_type RingBuffer::write_regions(byte **b1, RingBuffer::size_type *sz1,
                                    byte **b2, RingBuffer::size_type *sz2,
                                    RingBuffer::size_type desired) throw() {
    long s1, s2, res;
    res = PaUtil_GetRingBufferWriteRegions(purb, desired,
                                           reinterpret_cast<void**>(b1), &s1,
                                           reinterpret_cast<void**>(b2), &s2);
    *sz1 = s1;
    *sz2 = s2;
    return res;
}
