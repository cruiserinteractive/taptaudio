/* $Id$ $URL$ */

/**\file taptaudio_jackimpl.cpp
 * Implementation of portaudio implementation of taptaudio
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

#include "taptaudio_jackimpl.h"

namespace {

    int my_callback (jack_nframes_t nframes, void *arg) {
        JackAudioSystemImpl *jasi = (JackAudioSystemImpl*)arg;
        const size_t ips = jasi->input_ports.size();
        const size_t ops = jasi->output_ports.size();

        if (ips <= 1 && ops <= 1) {
            jack_default_audio_sample_t *out = ops
                ? (jack_default_audio_sample_t *) jack_port_get_buffer (jasi->output_ports[0], nframes)
                : 0;

            jack_default_audio_sample_t *in = ips
                ? (jack_default_audio_sample_t *) jack_port_get_buffer (jasi->input_ports[0], nframes)
                : 0;

            jasi->callback(in, out, nframes);
            return 0;
        }
        //fputc('C', stderr);
        //DODEBUG(INTERRUPT, ("Interleaving %u frames from %u input ports and %u output ports",
                            //nframes, ips, ops));
        /* else we need to interleave */
        for (size_t i = 0; i < ops; ++i)
            jasi->callback(0, jack_port_get_buffer(jasi->output_ports[i], nframes), nframes, i);
        for (size_t i = 0; i < ips; ++i)
            jasi->callback(jack_port_get_buffer(jasi->input_ports[i],  nframes), 0, nframes, i);
        return 0;
    }
    /*
    const char* const INPUT_DEVICES[] = {
        "alsa_pcm:capture_1"
    };
    const char* const OUTPUT_DEVICES[] = {
        "alsa_pcm:playback_1"
    };
    const size_t NUM_INPUTD = sizeof(INPUT_DEVICES)/sizeof(*INPUT_DEVICES);
    const size_t NUM_OUTPUTD = sizeof(OUTPUT_DEVICES)/sizeof(*OUTPUT_DEVICES);
    */
}

JackAudioSystemMixer::JackAudioSystemMixer(unsigned inputChannels,
                                           unsigned outputChannels,
                                           double sampleRate,
                                           AUDIO_FORMAT format,
                                           unsigned ttracks,
                                           unsigned rtracks,
                                           int deviceIDin,
                                           int deviceIDout)
        :
JackAudioSystemImpl(inputChannels, outputChannels, sampleRate, format, deviceIDin, deviceIDout),
AudioSystemMixer(this, format, ttracks, rtracks)
{
    DODEBUG(NOTICE, ("Mixing %d tracks into JACK Audio", tracks));
}

JackAudioSystemImpl::~JackAudioSystemImpl()
{
    if (!stopped) {
        DODEBUG(NOTICE, ("JACK System was not stopped before destructor.. trying to cease()"));
        cease();
    }
    if (!stopped) {
        DODEBUG(NOTICE, ("We did not stop. Disaster may ensue"));
    }

    DODEBUG(NOTICE, ("Closing stream (should flush any pending buffers)"));
    jack_client_close(client);
}

void JackAudioSystemImpl::cease() {
    DODEBUG(NOTICE, ("Calling jack_deactivate outside of mutex"));
    jack_deactivate(client);
}

JackAudioSystemImpl::JackAudioSystemImpl(unsigned inputChannels,
                                     unsigned outputChannels,
                                     double sampleRate,
                                     AUDIO_FORMAT format,
                                     int deviceIDin,
                                     int deviceIDout)
:
AudioSystemImpl(inputChannels, outputChannels, sampleRate, AF_Float32, deviceIDin, deviceIDout),
client(0)
{
    if (deviceIDin == 0 && inChannels == 1)
        deviceIDin = 1;

    const char **inports = 0, **outports = 0;
    unsigned ninports = 0, noutports = 0;
    fakeMonoMic = false;
    client = jack_client_open("taptaudio", JackNullOption, NULL);
    if (client == 0)
        throw "JACK server not running?";

    jack_set_process_callback(client, my_callback, this);
    //jack_on_shutdown(client, jack_shutdown, this);
    /*
    if (inputChannels != inChannels)
        DODEBUG(WARNING, ("Cannot override current limitation of %u input channels (with %u) for taptaudio-JACK",
                          inChannels, inputChannels));
    if (outputChannels != outChannels)
        DODEBUG(WARNING, ("Cannot override current limitation of %u output channels (with %u) for taptaudio-JACK",
                           outChannels, outputChannels));
    */
    if (format != AF_Default && format != fmt)
        DODEBUG(WARNING, ("JACK uses AF_Float32 format (and nothing else [except double, but haven't implemented that yet: %d != %d])",
                         (int)format, (int)fmt));

    try {
        for (unsigned i = 0; i < inChannels; ++i) {
            char buf[12];
            jack_port_t *input_port;
            snprintf(buf, 12, "input_%d", i);
            if ((input_port = jack_port_register (client,
                                                  buf, //"input", /*INPUT_DEVICES[deviceIDin],*/
                                                  JACK_DEFAULT_AUDIO_TYPE,
                                                  JackPortIsInput, 0)) == 0)
                throw "Could not register JACK input port";
            input_ports.push_back(input_port);
        }
        for (unsigned i = 0; i < outChannels; ++i) {
            char buf[12];
            jack_port_t *output_port;
            snprintf(buf, 12, "output_%d", i);
            if ((output_port = jack_port_register (client,
                                                   buf, //"output", /*OUTPUT_DEVICES[deviceIDin],*/
                                                   JACK_DEFAULT_AUDIO_TYPE,
                                                   JackPortIsOutput, 0)) == 0)
            throw "Could not register JACK output port";
            output_ports.push_back(output_port);
        }
        if (jack_activate(client))
            throw "Cannot activate client";

        {
            const char **p;

            inports = jack_get_ports (client, NULL, NULL, JackPortIsPhysical|JackPortIsOutput);
            outports = jack_get_ports (client, NULL, NULL, JackPortIsPhysical|JackPortIsInput);
            for (p = inports; p && *p; ++p, ++ninports)
                ;
            for (p = outports; p && *p; ++p, ++noutports)
                ;
        }
        sampRate = jack_get_sample_rate(client);

        if (fabs(sampleRate - sampRate) > 1.0)
            DODEBUG(WARNING, ("Cannot override JACK sample rate of %fHz with %fHz", sampRate, sampleRate));

        if (deviceIDin < 0 || inChannels + deviceIDin > ninports) {
            DODEBUG(WARNING, ("Invalid JACK first input device ID (%d+%d). Max is %u -- using default (0 == %s)",
                              deviceIDin, inChannels, ninports, ninports ? inports[0] : "<<nothing>>"));
            deviceIDin = 0;
        }
        if (deviceIDout < 0 || outChannels + deviceIDout > noutports) {
            DODEBUG(WARNING, ("Invalid JACK first output device ID (%d+%d). Max is %u -- using default (0 == %s)",
                              deviceIDout, outChannels, noutports, noutports ? outports[0] : "<<nothing>>"));
            deviceIDout = 0;
        }

        for (int i = 0; deviceIDin + i < (int)ninports && i < (int)input_ports.size(); ++i) {
            if (jack_connect (client, inports[deviceIDin+i], jack_port_name (input_ports[i])))
                throw "Could not connect to JACK input port";
        }
        for (int i = 0; deviceIDout + i < (int)noutports && i < (int)output_ports.size(); ++i) {
            if (jack_connect (client, jack_port_name(output_ports[i]), outports[deviceIDout+i]))
                throw "Could not connect to JACK output port";
        }
    } catch (...) {
        jack_client_close (client);
        free(inports); free(outports);
        throw;
    }
    /* we are now running... */
    DODEBUG(NOTICE, ("AudioSystem (JACK implementation) running at %fHz for %d input and %d output channels\n"
                     "\ton device %d(%s)/%d(%s) with %u input and %u output ports...",
                     sampRate, inChannels, outChannels,
                     deviceIDin,  ninports ?  inports [deviceIDin]  : "<<nothing>>",
                     deviceIDout, noutports ? outports[deviceIDout] : "<<nothing>>",
                     (unsigned)input_ports.size(), (unsigned)output_ports.size()));
    DODEBUG(INFO, ("fakeMonoMic = %s, iframe_size = %u, oframe_size = %u, sz = ?.",
                   fakeMonoMic ? "true" : "false", iframe_size, oframe_size));

    free(inports); free(outports);
}

void JackAudioSystemImpl::listDevices() {
    const char **ports, **p;
    unsigned i;
    fprintf(stderr, "Getting devices available for JACK\n");
    jack_client_t *dummy = jack_client_open("taptaudio_listdevices", JackNullOption, NULL);
    if (!dummy || jack_activate(dummy)) {
        fprintf(stderr, "\tCouldn't activate a JACK client -- maybe the server is not running\n\n");
        return;
    }
    if ((ports = jack_get_ports (dummy, NULL, NULL, JackPortIsPhysical|JackPortIsOutput)) == NULL || *ports == NULL) {
        fprintf(stderr, "No physical JACK capture ports\n");
    } else {
        for (i = 0, p = ports; *p; ++p, ++i)
            fprintf(stderr, "\t INPUT[%u]: '%s'\n", i, *p);
        free(ports);
    }
    if ((ports = jack_get_ports (dummy, NULL, NULL, JackPortIsPhysical|JackPortIsInput)) == NULL || *ports == NULL) {
        fprintf(stderr, "No physical JACK output ports\n");
    } else {
        for (i = 0, p = ports; *p; ++p, ++i)
            fprintf(stderr, "\tOUTPUT[%u]: '%s'\n", i, *p);
        free(ports);
    }
    fprintf(stderr, "\n");
    jack_client_close(dummy);
}
