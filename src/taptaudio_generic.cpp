/* $Id$ $URL$ */

/**\file taptaudio_generic.cpp
 * Implementation of backend-specific code from taptaudio.cpp that allows
 * run-time backend selection (when multiple backends are configured)
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

#include "taptaudio.h"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <assert.h>

#ifdef ENABLE_AUDIO
#include "taptaudio_impl.h"
#endif

#ifdef ENABLE_PORTAUDIO
#include "taptaudio_paimpl.h"
#endif

#ifdef ENABLE_JACK
#include "taptaudio_jackimpl.h"
#endif

#include "taptdebug.h"

void AudioSystem::listDevices() {
#ifdef ENABLE_PORTAUDIO
    PaAudioSystemImpl::listDevices();
#else
    fprintf(stderr, "TappedAudio was not built with PortAudio support\n");
#endif
#ifdef ENABLE_JACK
    JackAudioSystemImpl::listDevices();
#else
    fprintf(stderr, "TappedAudio was not built with JACK support\n");
#endif

}

namespace {
const AUDIO_BACKEND DEFAULT_BACKENDS[] = {
    AF_JACKAUDIO,
    AF_PORTAUDIO_ALSA,
    AF_PORTAUDIO,
    AF_NOAUDIO
};
}



AudioSystem::AudioSystem(AUDIO_FORMAT format,
                         double sampleRate,
                         int inputChannels,
                         int outputChannels,
                         int deviceIDin,
                         int deviceIDout,
                         const AUDIO_BACKEND backends[]
                        )
:
impl(0)
{
    if (!backends)
        backends = DEFAULT_BACKENDS;

    if (AUDIO_TRACKS == 0) {
        DODEBUG(NOTICE, ("Audio is disabled in the config file"));
        return;
    }
    assert(!instance && "There can only be one audio system!");

#ifdef ENABLE_AUDIO
    //SDL_mutexP(AudioSystemImpl::as_mutex);
#endif

    AUDIO_FORMAT fmt;
    const AUDIO_BACKEND *abe;
    for (abe = backends; !impl && *abe != AF_NOAUDIO; ++abe) {
        fmt = format;

#ifdef ENABLE_PORTAUDIO
        if (*abe & AF_PORTAUDIO) {
            if (fmt == AF_Default)
                fmt = AudioSystem::DEFAULT_AUDIO_FORMAT;
            try {
                if (AUDIO_TRACKS > 1) {
                    switch(fmt) {
//                    case AF_Float64:
                    case AF_Float32:
                        impl = new PaAudioSystemMixerT<float>(inputChannels, outputChannels,
                                                              sampleRate, format,
                                                              AUDIO_TRACKS, RECORD_TRACKS,
                                                              *abe, deviceIDin, deviceIDout);
                        break;
                    case AF_Int32:
                        impl = new PaAudioSystemMixerT<Sint32>(inputChannels, outputChannels,
                                                               sampleRate, format,
                                                               AUDIO_TRACKS, RECORD_TRACKS,
                                                               *abe, deviceIDin, deviceIDout);

                        break;
                    case AF_Int16:
                        impl = new PaAudioSystemMixerT<Sint16>(inputChannels, outputChannels,
                                                               sampleRate, format,
                                                               AUDIO_TRACKS, RECORD_TRACKS,
                                                               *abe, deviceIDin, deviceIDout);

                        break;
                    case AF_Int8:
                        impl = new PaAudioSystemMixerT<Sint8>(inputChannels, outputChannels,
                                                              sampleRate, format,
                                                              AUDIO_TRACKS, RECORD_TRACKS,
                                                              *abe, deviceIDin, deviceIDout);

                        break;
                    default: break;

                    }
                }
                if (!impl) {
                    impl = new PaAudioSystemImpl(inputChannels, outputChannels, sampleRate, format,
                                                 *abe, deviceIDin, deviceIDout);

                }
            } catch (PaError &err) {
                DODEBUG(ERROR, ("Error initialising portaudio system: %s",
                                Pa_GetErrorText(err)));
#ifdef PAV19
                if (err == paUnanticipatedHostError) {
                    const PaHostErrorInfo *hosterr = Pa_GetLastHostErrorInfo();
                    DODEBUG(ERROR, ("Original audio host error: %s",
                                    hosterr->errorText));
                }
#endif
                impl = 0;
            } catch (...) {
                DODEBUG(ERROR, ("Unknown error initialising portaudio system"));
            }
        }
#endif
#ifdef ENABLE_JACK
        if (*abe == AF_JACKAUDIO) {
            if (fmt == AF_Default)
                fmt = AudioSystem::DEFAULT_AUDIO_FORMAT;
            try {
                if (AUDIO_TRACKS > 1) {
                    impl = new JackAudioSystemMixerT<float>(inputChannels, outputChannels,
                                                            sampleRate, format,
                                                            AUDIO_TRACKS, RECORD_TRACKS,
                                                            deviceIDin  < 0 ? 0 : deviceIDin,
                                                            deviceIDout < 0 ? 0 : deviceIDout);
                }
                if (!impl) {
                    impl = new JackAudioSystemImpl(inputChannels, outputChannels, sampleRate, format,
                                                   deviceIDin  < 0 ? 0 : deviceIDin,
                                                   deviceIDout < 0 ? 0 : deviceIDout);
                }
            } catch (const char* err) {
                DODEBUG(ERROR, ("Error initialising JACK audio system: %s",
                                err));
                impl = 0;
            } catch (...) {
                DODEBUG(ERROR, ("Unknown error initialising JACK audio system"));
            }
        }
#endif
    } //end for loop

    /* we leave impl == 0 for the disabled version */
    AudioSystem::instance = this;
#ifdef ENABLE_AUDIO
    //SDL_mutexV(AudioSystemImpl::as_mutex);
#endif
}
