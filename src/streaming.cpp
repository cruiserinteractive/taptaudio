/* $Id$ $URL$ */
#include "streaming.h"

/**\file streaming.cpp
 * Implementation of Streaming Sample classes
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <assert.h>

#include "threadman/threadman.h"
#include "taptdebug.h"

namespace {
    enum {MAX_WARNINGS = 3, BUFINFO_INTERVAL = 1000};
    //ThreadMan::Runner *doflush = ThreadMan::runner(&StreamedRecording::flush);
    int start_flusher(void* sr) {
        return static_cast<StreamedRecording*>(sr)->flush();
    }
}

StreamedRecording::StreamedRecording(const StreamedRecording&) : Recording(), rb(32) {} //private

void StreamedRecording::doWrite() {
    if (!sfile && !(sfile = open(path, encoding, quality, overwrite ? SFM_WRITE : SFM_RDWR))) {
        flushstep.stop();
        DODEBUG(ERROR, ("Unable to open %s for streamed writing. Bail in %p",
                        path.c_str(), (void*)this));
        return;
    }
    RingBuffer::byte *b1, *b2;
    RingBuffer::size_type s1, s2;
    (void)rb.read_regions(&b1, &s1, &b2, &s2, rb.read_avail());
    sf_count_t numwritten = writen(sfile, b1, s1);
    if (s2)
        numwritten += writen(sfile, b2, s2);
    rb.read_advance(s1 + s2);
}

int StreamedRecording::flush() {
    unsigned iter = 1;
    RingBuffer::size_type accum = 0;
    RingBuffer::size_type worst = 0;
    DODEBUG(INFO, ("Streamer %p waiting for first buffer..", (void*)this));
    while (flushstep) {
        RingBuffer::size_type avail = rb.read_avail();
        if (avail) {
            accum += avail;
            if (avail > worst)
                worst = avail;
            if (iter == BUFINFO_INTERVAL) {
                DODEBUG(INFO, ("RingBuffer: %u flushs, %lu bytes total, avg. %.1f%% capacity, worst %u (%.1f%%)",
                               (unsigned)BUFINFO_INTERVAL,(unsigned long)accum,
                               100.0*accum/(BUFINFO_INTERVAL * rb.max_size()),
                               (unsigned)worst, 100.0*worst/rb.max_size()));
                worst = 0;
                accum = 0;
                iter = 1;
            } else {
                ++iter;
            }
            doWrite();
        }
    }
    /** Close the file here, while we are in a critical section */
    close();
    DODEBUG(INFO, ("Streamer has finished flushing -- ready to destroy."));
    destroy.ready_to_destroy();
    return 0;
}

StreamedRecording::StreamedRecording(const std::string &ppath,
                                     unsigned long ringbuffer_size,
                                     unsigned chans,
                                     AUDIO_FORMAT format,
                                     double samplerate,
                                     FILE_ENCODING enc,
                                     float qual,
                                     bool foverwrite)
: Recording(0, chans, format, samplerate), rb(ringbuffer_size),
sfile(0), flusher(0), path(ppath), warnings(MAX_WARNINGS),
encoding(enc), quality(qual), overwrite(foverwrite)
{
    destroy.alternate_started();
#if SDL_VERSION_ATLEAST(2, 0, 0)
    flusher = SDL_CreateThread(&start_flusher, "taptaudio_srec_flusher", this);
#else
    flusher = SDL_CreateThread(&start_flusher, this);
#endif
}

void StreamedRecording::close() {
    DODEBUG(INFO, ("Closing off the StreamedRecording"));
    if (sfile) {
        SNDFILE *todel = sfile;
        sfile = 0;
        sf_close(todel);
    }
}

void StreamedRecording::complete() {
    DODEBUG(INFO, ("Waiting for the streamer to finish flushing"));
    destroy.wants_to_destroy(flushstep, &flusher);
}

StreamedRecording::~StreamedRecording() {
    complete();
}

void StreamedRecording::fill(const void *more, unsigned long sz, int channelOffset, int bufChannels) {
    if (!flushstep.running())
        return; /* streaming has stopped *for some reason* */
    RingBuffer::byte *b1, *b2;
    RingBuffer::size_type s1, s2;
    const unsigned long total_expected =
        channelOffset >= 0 ? sz*channels*data_sz : sz;

    if (channelOffset <= 0 && total_expected > rb.write_avail()) {
        if (warnings) {
            const unsigned long left = total_expected - rb.write_avail();
            --warnings;
            DODEBUG(WARNING, ("Buffer Underrun: No room in ring buffer to write %lu bytes\n"
                              "\tWill write %lu and warn %d more time%s (%lu bytes will be lost)",
                              total_expected, (unsigned long)rb.write_avail(), warnings, warnings == 1 ? "" : "s", left
                             ));
        }
    }

    if (channelOffset >= 0) { /* more not interleaved; so interleave it; sz == nframes */
        const unsigned long num_bytes =
            total_expected > rb.write_avail() ?
            rb.write_avail()/channels     : /* this is how many bytes are left for each channel */
            sz * data_sz;                   /* otherwise go to the end of more */
        if (num_bytes == 0)
            return; /* full: bail */
        const unsigned long total_avail = num_bytes * channels;

        /* we want a write region for total_avail */
        (void)rb.write_regions(&b1, &s1, &b2, &s2, total_avail);

        assert(s1 + s2 == total_avail);
        assert(s1/channels + s2/channels == num_bytes);

        interleave(reinterpret_cast<Uint8*>(b1),
                   static_cast<const Uint8*>(more),
                   s1/channels,
                   data_sz,
                   channels,
                   channelOffset);

        if (total_avail > s1) {
            interleave(reinterpret_cast<Uint8*>(b2),
                       static_cast<const Uint8*>(more) + s1/channels,
                       s2/channels,
                       data_sz,
                       channels,
                       channelOffset);
        }

        if (channelOffset + 1 == channels) {
            /* last channel -- advance write region*/
            rb.write_advance(total_avail);
            tellSize(bSize() + total_avail);
        }
    } else if (bufChannels == channels) {
        const unsigned long num_bytes =
            total_expected > rb.write_avail() ? rb.write_avail() : total_expected;
        if (num_bytes == 0)
            return; /* full: bail */
        (void)rb.write_regions(&b1, &s1, &b2, &s2, num_bytes);
        assert(s1 + s2 == num_bytes);
        memcpy(b1, more, s1);
        if (num_bytes > s1)
            memcpy(b2, static_cast<const Uint8*>(more) + s1, s2);
        rb.write_advance(num_bytes);
        tellSize(bSize() + num_bytes);
    } else {
        if (warnings) {
            --warnings;
            DODEBUG(WARNING, ("No support for interleaved audio input with mismatched channel numbers"
                              "\t(Will warn %d more time%s, %lu bytes will be lost)",
                              warnings, warnings == 1 ? "" : "s", total_expected
                             ));
        }
    }
    //ThreadMan::srun(doflush, this, true); //this could malloc() == bad.
    flushstep.post();
}

bool StreamedRecording::save(const std::string &/*pth*/, FILE_ENCODING /*enc*/, float /*qual*/) {
    complete();
    return false;
}

void StreamedRecording::fillSilence(unsigned long sz) {
    RingBuffer::byte *b1, *b2;
    RingBuffer::size_type s1, s2;

    if (sz > rb.write_avail()) {
        if (warnings) {
            const unsigned long left = sz - rb.write_avail();
            --warnings;
            DODEBUG(WARNING, ("Buffer Underrun: No room in ring buffer to write %lu bytes [OF NOTHING!]\n"
                              "\tWill write %lu and warn %d more time%s (%lu bytes will be lost)",
                              sz, (unsigned long)rb.write_avail(), warnings, warnings == 1 ? "" : "s", left
                             ));
        }
        sz = rb.write_avail();
    }
    if (!sz)
        return; /* full: bail */
    (void)rb.write_regions(&b1, &s1, &b2, &s2, sz);
    assert(s1 + s2 == sz);
    memset(b1, 0, s1);
    if (sz > s1)
        memset(b2, 0, s2);
    rb.write_advance(sz);
}

void StreamedRecording::trim() {

}

const void* StreamedRecording::getBytes() const {
    return rb.bufferStart();
#if 0
    RingBuffer::byte *b1, *b2;
    RingBuffer::size_type s1, s2;
    (void)rb.read_regions(&b1, &s1, &b2, &s2, rb.read_avail());
    return b1;
#endif
}

unsigned long StreamedRecording::numBytes() const {
    if (rb.max_size() < bSize())
        return rb.max_size();
    return bSize();
#if 0
    RingBuffer::byte *b1, *b2;
    RingBuffer::size_type s1, s2;
    (void)rb.read_regions(&b1, &s1, &b2, &s2, rb.read_avail());
    return s1;
#endif
}

const void* StreamedRecording::getOffBytes(unsigned long offset, unsigned long &available) const {
    return Recording::getOffBytes(offset, available);
}

void StreamedRecording::mixed(unsigned long xframes,
                              void* handle,
                              int channelOffset,
                              const void *dat) {
    if (channelOffset < 0 || static_cast<unsigned>(channelOffset) + 1 == getChannels()) {
        super::mixed(xframes, handle, channelOffset, dat);
    }
}

ASSample *StreamedRecording::makePlayable(bool del_if_new) {
    ASSample *ret = 0;
    if (!destroy.hasDestroyed()) {
        DODEBUG(WARNING, ("Want to makePlayable before writer has closed the file.. Waiting."));
        complete();
        DODEBUG(INFO, ("Ready to makePlayable (hasDestroyed() == %s, val = %u)",
                       destroy.hasDestroyed() ? "true" : "false", (unsigned)destroy.value()));
    }
    try {
        switch (fmt) {
        case AF_Default: assert(0 && "Default format not assigned -- not valid here!");
        case AF_Int16:   ret = new StreamedFile<Sint16>(path, AudioSystem::RINGBUFFER_BYTES, fmt); break;
        case AF_Float32: ret = new StreamedFile<float> (path, AudioSystem::RINGBUFFER_BYTES, fmt); break;
        case AF_Int32:   ret = new StreamedFile<Sint32>(path, AudioSystem::RINGBUFFER_BYTES, fmt); break;
        case AF_Int8:    ret = new StreamedFile<Sint8> (path, AudioSystem::RINGBUFFER_BYTES, fmt); break;
        default:
            break;
        }
    } catch (...) {
        DODEBUG(ERROR, ("Unable to make %s playable\n", path.c_str()));
    }

    if (this != ret && del_if_new)
        delete this;
    return ret;
}
template <class T>
StreamedFile<T>::StreamedFile(const StreamedFile<T>&) {} //private
