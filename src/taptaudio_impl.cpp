/* $Id$ $URL$ */

/**\file taptaudio_impl.cpp
 * Implementation of the audio system parts common to all backends
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

#include "taptaudio_impl.h"

#include <functional>
#include <assert.h>
#include <stdexcept>

#include "taptdebug.h"
#include "streaming.h"

//SDL_mutex *AudioSystemImpl::as_mutex = 0;//SDL_CreateMutex();
//SDL_mutex *AudioSystemImpl::registry_mutex = 0;//SDL_CreateMutex();

namespace {

    SDL_sem *stop_sem = 0; //SDL_CreateSemaphore(0);

    /** Info passed into thread-saver */
    struct SaveInfo {
        Recording *r;      ///< PCM Sample data
        std::string f;     ///< File name
        FILE_ENCODING enc; ///< File encoding
/*        AUDIO_FORMAT fmt;  ///< Format of PCM sample data */
        float quality;     ///< Encoding Quality, where relevant

        SaveInfo(Recording *rr, const std::string &file, FILE_ENCODING eenc = AF_ENC_WAV, float qquality = 2.0)
            :
        r(rr), f(file), enc(eenc), quality(qquality) {}
    };

    int my_saver(void *data) {
        SaveInfo * si= static_cast<SaveInfo*>(data);
        int ret = si->r->save(si->f, si->enc, si->quality);
        delete si;
        return ret;
    }

    void kill_mutex(SDL_mutex **m) {
        if (!*m)
            return;
        SDL_mutexP(*m);
        SDL_mutex *d = *m;
        *m = 0;
        SDL_mutexV(d);
        SDL_DestroyMutex(d);
    }
}

/****************************************************/
/* AudioSystemImpl class implementation starts here */
/****************************************************/

AudioSystemImpl::AudioSystemImpl(const AudioSystemImpl&) {} //private

void AudioSystemImpl::stop() {
    if (stopped) {
        DODEBUG(NOTICE, ("We seem to be stopped already -- not stopping"));
    } else {
        DODEBUG(INFO, ("Asking callback to stop"));
        stopping = true;
    }
}

void AudioSystemImpl::waitstop() {
    //all these state bools are mainly to give meaningful debugging
    //the real mutex/concurrency safety comes from the stop semaphore
    bool lstop = stopping;
    if (stopped) {
        if (stopcollect) {
            DODEBUG(INFO, ("We are stopped already, collecting semaphore [should not block]"));
            SDL_SemWait(stop_sem);
            stopcollect = false;
        } else {
            DODEBUG(INFO, ("We seem to be stopped already -- not waiting or stopping"));
        }
    } else {
        if (!lstop) {
            DODEBUG(NOTICE, ("We don't seem to be trying to stop.. asking callback to stop"));
            stop();
        }
        if (!stopcollect) {
            DODEBUG(NOTICE, ("Waiting for callback to post to stop semaphore"));
        } else {
            DODEBUG(NOTICE, ("Wow - we got a race condition, doesn't matter - we will wait on the semaphore, but it won't block"));
        }
        SDL_SemWaitTimeout(stop_sem, 1000);
        stopping = stopcollect = false;
        DODEBUG(INFO, ("OK. We are stopped"));
    }
}

int AudioSystemImpl::callback(const void *inputBuffer,
                              void *outputBuffer,
                              unsigned long framesPerBuffer,
                              int channelOffset)
{
    //fputc('c', stderr);
    if (stopping) {
        stopcollect = true;
        stopped = true; //callback will not be called again
        SDL_SemPost(stop_sem);
        return 1; //this is PortAudioSpecific -- returning 1 stops the stream.
                  //for other implementations, make sure you override stop() and/or waitstop() appropriately
    }
    SDL_mutexP(as_mutex);
    stopped = false;
    //if (cur_rec && !cur_rec_paused && inputBuffer)
    doRecord(inputBuffer, framesPerBuffer, channelOffset);
    if (outputBuffer)
        doPlay(outputBuffer, framesPerBuffer, channelOffset);
    SDL_mutexV(as_mutex);
    return 0;
}

AudioSystemImpl::~AudioSystemImpl() {
    DODEBUG(INFO, ("Not locking to close AudioSystem..."));
    //SDL_mutexP(as_mutex);
    //SDL_mutexP(registry_mutex);

    DODEBUG(NOTICE, ("Clearing registry..."));
    for (REGISTRY::iterator it = registry.begin(); it != registry.end(); ++it)
        delete it->second;

    //SDL_mutexV(as_mutex);
    //SDL_mutexV(registry_mutex);
    audio_threadman.stopAsync();
    kill_mutex(&as_mutex);
    kill_mutex(&registry_mutex);
    kill_mutex(&hookMutex);
    //if (stop_sem)
        //SDL_DestroySemaphore(stop_sem);
    DODEBUG(NOTICE, ("Done."));
}

AudioSystemImpl::AudioSystemImpl(unsigned inputChannels,
                                 unsigned outputChannels,
                                 double sampleRate,
                                 AUDIO_FORMAT format,
                                 int /*deviceIDin*/,
                                 int /*deviceIDout*/)
:
as_mutex(0),
registry_mutex(0),
stopping(false),
stopped(true),
stopcollect(false),
inChannels(inputChannels),
outChannels(outputChannels),
sampRate(sampleRate == 0.0 ? 44100 : sampleRate),
fmt(format == AF_Default ? AudioSystem::DEFAULT_AUDIO_FORMAT : format),
cur_rec(0),
all_rec_paused(false),
record_after_play(0),
cur_play(0),
vu_level(0.0f)
{
    if (!as_mutex)
        as_mutex = SDL_CreateMutex();
    if (!registry_mutex)
        registry_mutex = SDL_CreateMutex();
    if (!stop_sem)
        stop_sem = SDL_CreateSemaphore(0);
    hookMutex = SDL_CreateMutex();

    int sz = 0;
    switch (fmt) {
    case AF_Float32:
    case AF_Int32: sz++;
    case AF_PackedInt24 :
    case AF_Int24: sz++;
    case AF_Int16: sz++;
    case AF_Int8:
    case AF_UInt8: sz++;
    default: break;
    }
    iframe_size = inChannels * sz;
    oframe_size = outChannels * sz;

    //fakeMonoMic = inChannels == 1 && outChannels == 2;

}

void AudioSystemImpl::doRecord(const void *inputBuffer,
                               unsigned long framesPerBuffer,
                               int channelOffset)
{
    const unsigned long insz = channelOffset < 0 ? iframe_size * framesPerBuffer : framesPerBuffer;
    if (cur_rec && !all_rec_paused) {
        if (inputBuffer) {
            cur_rec->fill(inputBuffer, insz, channelOffset, inChannels);
        } else if (channelOffset < 0) { //fill with silence
            cur_rec->fillSilence(insz);
        }
    }
    vu_level = PCMSample::updateVU(static_cast<const float*>(inputBuffer),
                                   insz/sizeof(float),
                                   vu_level);
}

void AudioSystemImpl::doPlay(void *outputBuffer,
                             unsigned long framesPerBuffer,
                             int channelOffset) {
    if (outputBuffer) {
        memset(outputBuffer, 0, oframe_size * framesPerBuffer);
        if (cur_play) {
//            putc('b', stderr);
            unsigned long nb = cur_play->numBytes();
            if (play_offset < nb) {
                unsigned long sz = framesPerBuffer * oframe_size;
                if (sz > nb - play_offset)
                    sz = nb - play_offset;

                if  (cur_play->getChannels() == 1 && outChannels == 2) {
                    sz /= 2;
                    PCMSample::mono2stereo(static_cast<Uint8*>(outputBuffer),
                                           static_cast<const Uint8*>(cur_play->getBytes()) + play_offset,
                                           sz,
                                           oframe_size/2);
                } else {
                    memcpy(outputBuffer,
                           static_cast<const Uint8*>(cur_play->getBytes()) + play_offset,
                           sz);
                }
                play_offset += sz;
            }
            if (play_offset >= nb - 1) {
                //play_offset = 0;
                delref(&cur_play);
                if (record_after_play) {
                    Recording** rec = getRecordHolder();
                    if (rec) {
                        *rec = record_after_play;
                        record_after_play = 0;
                    }
                }
            }
        }
    }
}

bool AudioSystemImpl::map(const std::string &name, ASSample *sam) {
    if (!sam)
        return false;
    std::string nm(name);
    if (name.size() == 0) {
        unsigned long x = reinterpret_cast<unsigned long>(sam);
        nm += '0';
        for (unsigned p = 0; p < sizeof(sam) * 8 / 3; ++p) {
            nm += '0' + (x & 07);
            x >>= 3;
        }
    }
    bool ret = false;
    SDL_mutexP(registry_mutex);
    REGISTRY::iterator it = registry.find(nm);
    if (it == registry.end()) {
        registry.insert(REGISTRY::value_type(nm, sam));
        ret = true;
    }
    SDL_mutexV(registry_mutex);
    DODEBUG(FUNCTION, ("%s %s %p",
                       nm.c_str(),
                       ret ? "->" : "TAKEN: cannot point to",
                       (void*)sam));
    return ret;
}

ASSample* AudioSystemImpl::loadSample(const std::string &name, bool stream) {
    ASSample *ret = 0;
    //note we only need to take out a registry mutex for this
    SDL_mutexP(registry_mutex);

    REGISTRY::iterator it = registry.find(name);
    if (it == registry.end()) {
        try {
            ret = new FileSample(name/*, outChannels == 2*/);

            if   (fmt == ret->getFormat() &&
                  outChannels >= ret->getChannels() &&
                  sampRate == ret->getSamRate()) {

                registry.insert(REGISTRY::value_type(name, ret));

            } else {

                DODEBUG(WARNING, ("The format of %s is not compatible with the current AudioSystem.",
                                  name.c_str()));
                DODEBUG(WARNING, ("(file = [%d]fmt %d ch @ %fHz != [%d]fmt %d ch @ %fHz = AudioSystem)",
                                  static_cast<int>(ret->getFormat()), ret->getChannels(), ret->getSamRate(),
                                  static_cast<int>(fmt), outChannels, sampRate));

                delete ret;
                ret = 0;

            }
        } catch (const char *err) {
            DODEBUG(ERROR, ("Exception caught trying to load %s: %s",
                            name.c_str(), err));
        } catch (std::bad_alloc &) {
            DODEBUG(ERROR, ("Not enough memory to load %s",
                            name.c_str()));
        } catch (...) {
            DODEBUG(ERROR, ("Couldn't load %s (unknown reason)",
                            name.c_str()));
        }
    } else {
        ret = it->second;
    }
    SDL_mutexV(registry_mutex);
    return ret;
}

AudioSystemImpl::REGISTRY::iterator AudioSystemImpl::linearSearch(ASSample *samp) {
    REGISTRY::iterator it = registry.begin();
    for (; it != registry.end() && it->second != samp; ++it)
        ;
    return it;
}

bool AudioSystemImpl::checkSample(ASSample *samp, bool lockreg) {
    if (samp == cur_rec)
        return true;
    bool ret = true;
    if (lockreg) SDL_mutexP(registry_mutex);
    if (samp->must_register() && linearSearch(samp) == registry.end()) {
        DODEBUG(ERROR, ("Sample %p is not in the registry. Cowardly refusing to continue because this will probably segfault.",
                        (void*)samp));
        //assert(!"Bailing Out -- invalid sample");
        ret = false;
    }
    if (lockreg) SDL_mutexV(registry_mutex);
    return ret;
}

bool AudioSystemImpl::freeSample(ASSample* samp, bool ignore_nonexistence) {
    /* \todo -- use some RAII, this looks crap */
    bool ret = false;
    SDL_mutexP(as_mutex);
    if (samp->refs) {
        SDL_mutexV(as_mutex);
        DODEBUG(WARNING, ("Couldn't free sample -- maybe it is still playing"));
        return false;
    }
    SDL_mutexP(registry_mutex);

    REGISTRY::iterator it = linearSearch(samp);

    if (it == registry.end()) {
        if (ignore_nonexistence)  {
            delete samp;
            ret = true;
        } else {
             DODEBUG(ERROR, ("Couldn't find sample in registry -- not freeing"));
        }
    } else {
        registry.erase(it);
        delete samp;
        DODEBUG(INFO, ("Sample removed from registry and deleted"));
        ret = true;
    }
    SDL_mutexV(registry_mutex);
    SDL_mutexV(as_mutex);
    return ret;
}

ASSample* AudioSystemImpl::loadRawSample(const std::string &name, void* data, unsigned long size) {
    ASSample *ret = 0;
    //note we only need to take out a registry mutex for this
    SDL_mutexP(registry_mutex);

    REGISTRY::iterator it = registry.find(name);
    if (it != registry.end()) {
        registry.erase(it);
    }
    ret = new PCMSample(data, outChannels, size, sampRate, fmt);
    registry.insert(REGISTRY::value_type(name, ret));
    SDL_mutexV(registry_mutex);
    return ret;
}


ASSample** AudioSystemImpl::getSampleHolder(bool loop, float /*vol*/, unsigned* trackno) {
    if (trackno)
        *trackno = 0;
    if (!loop && !cur_play) {
        play_offset = 0;
        return &cur_play;
    }
    return 0;
}

Recording** AudioSystemImpl::getRecordHolder(unsigned* trackno) {
    if (trackno)
        *trackno = 0;
    return &cur_rec;
    return 0;
}

unsigned AudioSystemImpl::stopSample(ASSample *samp, int /*trackno*/, bool lock) {
    if (cur_play == samp) {
        if (lock) SDL_mutexP(as_mutex);
        delref(&cur_play);
        if (lock) SDL_mutexV(as_mutex);
        return 1;
    }
    return 0;
}

bool AudioSystemImpl::isPlaying(ASSample *samp, int /* trackno */) {
    return cur_play == samp;
}

unsigned AudioSystemImpl::pauseSample(ASSample * /*samp*/, int /*trackno*/, bool /*setpause*/) {
    return 0; /* non-mixer doesn't pause */
}

void AudioSystemMixer::RMix::reset() {
    delref(&rec);
    paused = false;
}

AudioSystemMixer::AudioSystemMixer(const AudioSystemMixer&) : tracks(0), record_tracks(0) {} //private

ASSample** AudioSystemMixer::getSampleHolder(bool loop, float vol, unsigned* trackno) {
    for (unsigned i = 0; i < tracks; ++i) {
        if (!track[i].sam) {
            track[i].vol = vol;
            track[i].looping = loop;
            if (trackno)
                *trackno = i;
            DODEBUG(INFO, ("Will mix into track %u", i));
            return &(track[i].sam);
        }
    }
    return 0;
}

Recording** AudioSystemMixer::getRecordHolder(unsigned* trackno) {
    for (unsigned i = 0; i < record_tracks; ++i) {
        if (!rtrack[i].rec) {
            if (trackno)
                *trackno = i;
            DODEBUG(INFO, ("Will record into rtrack %u", i));
            return &(rtrack[i].rec);
        }
    }
    return 0;
}

unsigned AudioSystemMixer::stopSample(ASSample *samp, int trackno, bool lock) {
    return apply(samp, trackno, std::mem_fun(&Mix::reset), lock);
}

namespace { void noop(void*) {} }


bool AudioSystemMixer::isPlaying(ASSample *samp, int trackno) {
    return apply(samp, trackno, noop);
}

unsigned AudioSystemMixer::setVolume(ASSample *samp, float vol, int trackno) {
    return apply(samp, trackno, std::bind2nd(std::mem_fun(&Mix::setVol), vol));
}

unsigned AudioSystemMixer::pauseSample(ASSample *samp, int trackno, bool setpause) {
    return apply(samp, trackno, std::bind2nd(std::mem_fun(&Mix::setPause), setpause));
}

AudioSystemMixer::~AudioSystemMixer() {
    //Can't access virtuals from the destructor, so we can't check this!
    //if (!impl()->stopped) {
    //    DODEBUG(NOTICE, ("Audio is not stopped!! We can't free the mixer yet!!"));
    //} else {
    DODEBUG(INFO, ("Deleting mixing tracks"));

    for (unsigned i = 0; i < tracks; ++i)
        if (track[i].sam)
            delref(&track[i].sam);
    delete[] track;

    for (unsigned i = 0; i < record_tracks; ++i) {
        Recording *rec = rtrack[i].rec;
        if (rec) {
            delref(&rtrack[i].rec);
            if (rec->refs) {
                DODEBUG(WARNING, ("Refs for Recording %p == %u -- AudioSystemMixer disclaiming responsibility", (void*)rec, rec->refs));
                continue;
            }
            DODEBUG(INFO, ("Refs for Recording %p == 0 -- AudioSystemMixer claiming responsibility", (void*)rec));
            imp->freeSample(rec, true);
        }
    }
    delete[] rtrack;

    //}
}

bool AudioSystemImpl::mixSample(ASSample *samp, bool record_after, double record_size, bool loop, float vol, unsigned* trackno) {
    bool success = false;
    SDL_mutexP(as_mutex);
    if (TAPTDBGLEV > DEBUG_WARNING)
        if (!checkSample(samp))
            return false;
    ASSample **holder = getSampleHolder(loop, vol, trackno);
    if (holder) {
        addref(holder, samp);
        success = true;
        if (record_after && !record_after_play) {
            all_rec_paused = false;
            record_after_play = new Recording(static_cast<unsigned long>(record_size * sampRate * iframe_size),
                                              inChannels,
                                              fmt,
                                              sampRate);
        }
        DODEBUG(FUNCTION, ("%sixing %lu bytes, %d channels @ %fHz",
                           (*holder)->getChannels() == outChannels ? "M" : "Upm",
                           (*holder)->numBytes(), (*holder)->getChannels(), (*holder)->getSamRate()));
    }
    SDL_mutexV(as_mutex);
    return success;
}

bool AudioSystemImpl::loopSample(ASSample *samp, float vol, unsigned* trackno) {
    return mixSample(samp, false, 0.0, true, vol, trackno);
}

bool AudioSystemImpl::startRec(double max_seconds, unsigned *rtrackno, ASSample **recref) {
    unsigned dummy_trackno = 0;
    if (!rtrackno)
        rtrackno = &dummy_trackno;
    bool success = false;

    SDL_mutexP(as_mutex);
    Recording **rp = getRecordHolder(rtrackno);
    if (rp) {
        try {
            all_rec_paused = false;
            Recording *rec = new Recording(static_cast<unsigned long>(max_seconds * sampRate * iframe_size),
                                           inChannels,
                                           fmt,
                                           sampRate);
            addref(rp, rec);
            if (recref)
                *recref = rec;
            DODEBUG(FUNCTION, ("Recording %d channels at %.0fHz for up to %.2f seconds in track #%u located as %p at %p",
                               inChannels, sampRate, max_seconds, *rtrackno, (void*)*rp, (void*)rp));
            success = true;
        } catch (std::bad_alloc&) {
        }
    } else {
        DODEBUG(WARNING, ("All recording tracks are taken."));
    }
    SDL_mutexV(as_mutex);
    return success;
}

bool AudioSystemImpl::streamRec(const std::string &file, const AudioSystem::RecordOpts &opts, unsigned *rtrackno, ASSample **recref) {
    unsigned dummy_trackno = 0;
    if (!rtrackno)
        rtrackno = &dummy_trackno;
    bool success = false;

    SDL_mutexP(as_mutex);
    Recording **rp = getRecordHolder(rtrackno);
    if (rp) {
        try {
            //play_offset = 0;
            all_rec_paused = false; //\todo maybe not appropriate
            StreamedRecording *rec = new StreamedRecording(file,
                                                           AudioSystem::RINGBUFFER_RECORDBYTES,
                                                           inChannels, fmt, sampRate,
                                                           opts.enc, opts.quality);
            DODEBUG(FUNCTION, ("Streaming to %s: %d channels at %.0fHz in track #%u located as %p at %p",
                               file.c_str(), inChannels, sampRate, *rtrackno, (void*)rec, (void*)rp));
            addref(rp, rec);
            if (recref)
                *recref = *rp;
            success = true;
        } catch (std::bad_alloc &) {
        } catch (std::invalid_argument &ia) {
            DODEBUG(ERROR, ("Invalid argument in setup: %s", ia.what()));
        }
    } else {
        DODEBUG(WARNING, ("All recording tracks are taken."));
    }
    SDL_mutexV(as_mutex);
    return success;
}

bool AudioSystemImpl::pauseRec(ASSample *rec, int trackno, bool setpause) {
    bool *pausevar = 0;
    if (findRec(&rec, &trackno, &pausevar)) {
        DODEBUG(FUNCTION, ("Pause for Recording %p (was %spaused will %spaused)...",
                           (void*)rec, *pausevar ? "" : "UN", setpause ? "" : "UN"));
        if (*pausevar != setpause) {
            *pausevar = setpause;
            return true;
        }
    }
    return false;
}

bool AudioSystemImpl::resumeRec(ASSample *rec, int trackno) {
    return pauseRec(rec, trackno, false);
}

bool AudioSystemImpl::previewRec(float vol, unsigned* trackno, int record_trackno, ASSample *rec) {
    bool *pausevar = 0;
    ASSample *lrec = 0;
    if (findRec(&lrec, &record_trackno, &pausevar)) {
        DODEBUG(FUNCTION, ("Previewing recording %p...", (void*)lrec));
        mixSample(lrec, false, 0, false, vol, trackno);
        return true;
    }
    return false;
}

ASSample* AudioSystemImpl::stopRec(const std::string &name, ASSample *arec, int trackno,
                                   const AudioSystem::RecordOpts &opts) {
    ASSample *ret = 0;
    Recording **record_to_zap = 0;
    Recording *rec = 0;
    SDL_mutexP(as_mutex);
    if (findRec(&arec, &trackno, 0, &record_to_zap)) {
        rec = *record_to_zap;
        DODEBUG(FUNCTION, ("Stopping recording %p in track #%d at %p...",
                           (void*)rec, trackno, (void*)record_to_zap));
    } else {
        DODEBUG(WARNING, ("Couldn't find a recording to stop (eek!). Asked for %p in track %d.",
                          (void*)rec, trackno));
    }
    if (rec) {
        delref(record_to_zap);
        rec->trim();
        DODEBUG(INFO, ("Size is %lu bytes",
                       rec->numBytes()));

        if (opts.save) {
            if (opts.save_in_thread && rec->canSaveInThread(opts.keep_and_return)) {
                audio_threadman.run(my_saver, new SaveInfo(rec, name, opts.enc, opts.quality));
            } else {
                SDL_mutexV(as_mutex);
                rec->save(name, opts.enc, opts.quality);
                SDL_mutexP(as_mutex);
            }
        }

        if (opts.keep_and_return) { //register
            ret = rec->makePlayable(true);
            /* rec might be deleted -- set it to NULL */
            rec = 0;
            SDL_mutexP(registry_mutex);
            REGISTRY::iterator it = registry.find(name);
            if (it != registry.end()) {
                delete it->second;
                it->second = ret;
            } else {
                registry.insert(REGISTRY::value_type(name, ret));
            }
            SDL_mutexV(registry_mutex);
        } else {
            freeSample(rec, true);
        }
    }
    SDL_mutexV(as_mutex);
    return ret;
}

bool AudioSystemImpl::findRec(ASSample **rec, int *trackno, bool **pausevar, Recording ***rechome) {
    assert(rec);
    if (*rec && *rec != cur_rec) {
        DODEBUG(WARNING, ("Requested recording is not current. Returning current anyway."));
    }
    if (trackno && *trackno > 0) {
        DODEBUG(WARNING, ("Track number (%d) is ignored here", *trackno));
    }
    if (pausevar)
        *pausevar = &all_rec_paused;
    if (trackno)
        *trackno = 0;
    *rec = cur_rec;
    if (rechome)
        *rechome = &cur_rec;
    return true;
}

bool AudioSystemMixer::findRec(ASSample **rec, int *trackno, bool **pausevar, Recording ***rechome) {
    assert(rec);
    int dummy_track = 0;
    if (*rec == 0 && (!trackno || *trackno < 0)) {
        DODEBUG(WARNING, ("No sample or track number specified -- using track = 0"));
        trackno = &dummy_track;
    }
    RMix * rmx = 0;
    if (trackno && *trackno >= 0) {
        if (static_cast<unsigned>(*trackno) >= record_tracks) {
            DODEBUG(ERROR, ("Invalid track number requested (%d) > max tracks (%u)",
                            *trackno, record_tracks));
            return false;
        } else {
            rmx = &rtrack[*trackno];
        }
    } else if (*rec) {
        if (!trackno)
            trackno = &dummy_track;
        /* search for \a *rec */
        for (unsigned i = 0; i < record_tracks; ++i) {
            if (rtrack[i].rec == *rec) {
                rmx = &rtrack[i];
                *trackno = i;
                break;
            }
        }
    } else {
        DODEBUG(ERROR, ("EVIL: Should never reach here!!!"));
        return false;
    }
    if (rmx) {
        if (!*rec || *rec != rmx->rec) {
            if (*rec) {
                DODEBUG(WARNING, ("Sample in track #%d (%p) does not match requested sample (%p). Favouring track.",
                                  *trackno, (void*)rmx->rec, (void*)rec));
            }
            *rec = rmx->rec;
        }
        if (pausevar)
            *pausevar = &rmx->paused;
        if (rechome)
            *rechome = &rmx->rec;
        return true;
    } else {
        DODEBUG(WARNING, ("Unable to find sample (%p) for recording", (void*)*rec));
        return false;
    }
}

