/* $Id$ $URL$ */
#ifndef TAPTAUDIO_IMPL_DOT_AITCH
#define TAPTAUDIO_IMPL_DOT_AITCH

#if defined DEBUG_STL && defined __GNUC__ && __GNUC__ >= 4
#undef _GLIBCXX_VECTOR
#include <debug/vector>
#include <debug/map>
#if __GNUC_MINOR__ >= 2
namespace stl = std::__debug;
#else
namespace stl = __gnu_debug_def;
#endif
#else
#include <vector>
#include <map>
#include <list>
namespace stl = std;
#endif

#include "taptaudio.h"

/**\file taptaudio_impl.h
 * Header for common implementation of taptaudio
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <SDL_thread.h>
#include <typeinfo>
#include <limits>
#include <math.h>

#include "taptdebug.h"
#include "sample.h"
#include "streaming.h"
#include "threadman/threadman.h"

#include <assert.h>

template <class T, class R>
inline void addref(T** dest, R* sam) {
    assert(dest && !*dest);
    ++sam->refs;
    *dest = sam;
}

template <class T>
inline void delref(T** dest) {
    assert(dest && *dest);
    --(*dest)->refs;
    *dest = 0;
}

/** The portaudio implementation of the audio system */
class AudioSystemImpl {
private:
    AudioSystemImpl(const AudioSystemImpl&);
    AudioSystemImpl& operator=(const AudioSystemImpl&) {return *this;}
public:
    typedef stl::map<std::string, ASSample*> REGISTRY; ///< Type for WAV file registry
    SDL_mutex *as_mutex;                        ///< Audio System mutex
    SDL_mutex *registry_mutex;                  ///< Registry mutex
    ThreadMan audio_threadman;                  ///< Thread manager
    //protected:
public:
    REGISTRY registry;             ///< The registy of loaded WAV files
    bool stopping;        ///< Are we trying to stop the audio system
    bool stopped;         ///< Is the audiosystem stopped?
    bool stopcollect;     ///< Have we collected the stop semaphore post?
    bool fakeMonoMic;     ///< Are we "faking" a Mono input stream by averaging stereo channels
    unsigned inChannels;  ///< Number of input channels
    unsigned outChannels; ///< Number of output channels
    double sampRate;      ///< The sample rate of the audio stream
    AUDIO_FORMAT fmt;     ///< The audio format

    unsigned iframe_size; ///< The size (in bytes) of an input frame
    unsigned oframe_size; ///< The size (in bytes) of an output frame

private:
    Recording *cur_rec;           ///< The current recording (e.g. we are recording if this is non-null)
public:
    bool all_rec_paused;          ///< Are all recordings paused?
    Recording *record_after_play; ///< The recording we will start as soon as no samples are playing

    ASSample *cur_play;           ///< For non-tracked implementations, the currently playing sample
    unsigned long play_offset;    ///< The current offset into \a cur_play
    float vu_level;               ///< The current VU level (exponential backoff)

    /** Perform the "record" portion of the callback */
    virtual void doRecord(const void *inputBuffer,
                          unsigned long framesPerBuffer,
                          int channelOffset);

    /** Perform the "play" portion of the callback */
    virtual void doPlay(void *outputBuffer,
                        unsigned long framesPerBuffer,
                        int channelOffset);

public:
    /** Virtual destructor, free the portaudio handles and clear the registry (?) */
    virtual ~AudioSystemImpl();
protected:
    /** Create an AudioSystemImpl */
    AudioSystemImpl(unsigned inputChannels = 1,
                    unsigned outputChannels = 2,
                    double sampleRate = 44100,
                    AUDIO_FORMAT format = AF_Int16,
                    int deviceIDin = 0,
                    int deviceIDout = 0);

    /** Do a linear search for a sample */
    REGISTRY::iterator linearSearch(ASSample *samp);

public:
    /** Check a sample for validity */
    bool checkSample(ASSample *samp, bool lockreg = true);

    /** Generic callback
     * \note occurs at interrupt level so we CAN NOT ALLOCATE MEMORY
     */
    virtual int callback(const void *inputBuffer,
                         void *outputBuffer,
                         unsigned long framesPerBuffer,
                         int channelOffset = -1);
    /**
     * Get a holder (e.g. a track reference) in which to load a sample.
     */
    virtual ASSample** getSampleHolder(bool loop = false, float vol = 1.0f, unsigned* trackno = 0);

    /**
     * Get a holder in which to start a Recording
     */
    virtual Recording** getRecordHolder(unsigned* trackno = 0);

    /** Load a "known" sample, or a file */
    virtual ASSample* loadSample(const std::string &name, bool stream);

    bool map(const std::string &name, ASSample *sam);

    //ASSample* loadSample_raw(const std::string &name);
    /** Load a sample from raw data (assumes format is same as current AudioSystem) */
    ASSample* loadRawSample(const std::string &name, void* data, unsigned long size);

    /** mix a sample into the output stream */
    bool mixSample(ASSample *samp, bool record_after, double record_size, bool loop = false, float vol = 1.0f, unsigned* trackno = 0);
    /** mix a sample into the output, looping it until we stop */
    bool loopSample(ASSample *samp, float vol = 1.0f, unsigned* trackno = 0);

    /** Release resources (delete) a sample that is no longer needed */
    bool freeSample(ASSample *samp, bool ignore_nonexistence = false);
    virtual unsigned stopSample(ASSample *samp, int trackno = -1, bool lock = true);
    virtual bool isPlaying(ASSample *samp, int trackno);
    virtual unsigned pauseSample(ASSample *samp, int trackno = -1, bool setpause = true);
    virtual unsigned setVolume(ASSample * /*samp*/, float /*vol*/ = 1.0f, int /*trackno*/ = -1) {return 0;}

    bool streamRec(const std::string& file, const AudioSystem::RecordOpts &opts = AudioSystem::RO_DEFAULT, unsigned *rtrackno = 0, ASSample **recref = 0);
    bool startRec(double max_seconds, unsigned *rtrackno = 0, ASSample **recref = 0);

    /**
     * The same recording sample can not be in two tracks simultaneously -- that makes no sense --
     * so do a more restrained search than apply.
     */
    virtual bool findRec(ASSample **rec, int *trackno = 0, bool **pausevar = 0, Recording ***rechome = 0);
    bool pauseRec(ASSample *rec = 0, int trackno = -1, bool setpause = true);
    bool resumeRec(ASSample *rec = 0, int trackno = -1);
    bool previewRec(float vol, unsigned* trackno = 0, int record_trackno = 0, ASSample *rec = 0);

    /** Return true if we are currently recording */

    bool isRecording() { return cur_rec; }
    ASSample* stopRec(const std::string &name, ASSample *rec = 0, int trackno = -1,
                      const AudioSystem::RecordOpts &opts = AudioSystem::RO_DEFAULT);
    virtual void stop();
    virtual void waitstop();

    /**
     * Try and abort the stream, without flushing buffers. Return true if supported and successful
     */
    virtual bool abort() {return false;}

    /**
     * Cease is an alias for stop that can be overrided by an implementation to cause an immediate
     * stop of the audio system (without waiting for the callback to be called)
     */
    virtual void cease() {waitstop();}

    /**
     * Input VU meter
     */
    float inputVU() {return vu_level;}

#if __cplusplus >= 201103L
    typedef std::forward_list<AudioSystemHook *> HookList;
#else
    typedef std::list<AudioSystemHook *> HookList;
#endif
    SDL_mutex *hookMutex;
    HookList callbackHooks;
    void addCallbackHook(AudioSystemHook *hook) { // TODO: Move to .cpp.
        SDL_LockMutex(hookMutex);
        bool exists = false;
        for (HookList::const_iterator it = callbackHooks.begin(); it != callbackHooks.end(); ++it) {
            if (*it == hook) {
                DODEBUG(WARNING, ("AudioSystemHook already in list; ignoring."));
                exists = true;
                break;
            }
        }
        if (!exists)
            callbackHooks.push_back(hook);
        SDL_UnlockMutex(hookMutex);
    }
    void removeCallbackHook(AudioSystemHook *hook) {
        SDL_LockMutex(hookMutex);
        for (HookList::iterator it = callbackHooks.begin(); it != callbackHooks.end(); ++it) {
            if (*it == hook) {
                callbackHooks.erase(it);
                break;
            }
        }
        SDL_UnlockMutex(hookMutex);
    }
};

/**
 * An implementation of AudioSystemImpl that mixes any number of tracks
 * with appropriate clipping.
 */
class AudioSystemMixer {
    AudioSystemMixer(const AudioSystemMixer&);
    AudioSystemMixer& operator=(const AudioSystemMixer &) {return *this;}
public:
    AudioSystemImpl *imp;

protected:
    const unsigned tracks; /* todo: this should be a struct */
    const unsigned record_tracks;
    AUDIO_FORMAT mix_format;

    /** Account keeping each track that we are mixing */
    struct Mix {
        ASSample *sam; ///< The sample we are mixing
        unsigned off;  ///< The offset in the sample
        float vol;     ///< The volume we are mixing at
        bool looping;  ///< Whether we are looping
        bool paused;   ///< Whether we are currently paused

        void reset() {
            delref(&sam);
            off = 0;
            vol = 1.0;
            looping = paused = false;
        }
        void setVol(float v) {vol = v;}
        void setPause(bool pause) {paused = pause;}

        Mix()
            :
        sam(0), off(0), vol(1.0), looping(false), paused(false)
        {}
    };
    struct RMix {
        Recording *rec;
        bool paused;
        RMix *next;
        void setPause(bool pause) {paused = pause;}
        RMix()
            :
        rec(0), paused(false), next(0)
        {}
        void reset(); 
    };

    Mix *track;
    RMix *rtrack;

    virtual void doPlay(void *outputBuffer,
                        unsigned long framesPerBuffer,
                        int channelOffset) = 0;

    virtual void doRecord(const void *inputBuffer,
                          unsigned long framesPerBuffer,
                          int channelOffset) = 0;

    AudioSystemMixer(AudioSystemImpl *iimp, AUDIO_FORMAT format, unsigned ttracks, unsigned rec_tracks)
        :
    imp(iimp), tracks(ttracks), record_tracks(rec_tracks), mix_format(format ? format : AudioSystem::DEFAULT_AUDIO_FORMAT), track(0), rtrack(0) {
        track = new Mix[tracks];
        rtrack = new RMix[record_tracks];
    }
protected:
    template <class PRED>
        unsigned apply(ASSample *samp, int trackno, PRED p, bool lock = false);
public:

    virtual AudioSystemImpl* impl() = 0;
    virtual ~AudioSystemMixer();
    virtual ASSample** getSampleHolder(bool loop = false, float vol = 1.0f, unsigned* trackno = 0);

    virtual Recording** getRecordHolder(unsigned* trackno = 0);
    virtual bool findRec(ASSample **rec, int *trackno = 0, bool **pausevar = 0, Recording ***rechome = 0);

    virtual unsigned stopSample(ASSample *samp, int trackno = -1, bool lock = true);
    virtual bool isPlaying(ASSample *samp, int trackno);
    virtual unsigned pauseSample(ASSample *samp, int trackno = -1, bool setpause = true);
    virtual unsigned setVolume(ASSample *samp, float vol = 1.0f, int trackno = -1);
};

template <class PRED>
unsigned AudioSystemMixer::apply(ASSample *samp, int trackno, PRED p, bool lock) {
    unsigned ctr = 0;
    unsigned i = (trackno < 0) ? 0 : trackno;
    //AudioSystemImpl *imp = impl();
    const unsigned max = (trackno < 0) ? tracks : trackno+1;
    if (TAPTDBGLEV > DEBUG_WARNING)
        impl()->checkSample(samp);
    DODEBUG(VINFO, ("Looking in track range [%u, %u) for sample %p to do %s",
                    i, max, (void*)samp, typeid(p).name()));
    if (lock) SDL_mutexP(imp->as_mutex);
    for (; i < max; ++i) {
        if (track[i].sam == samp) {
            p(track + i);
            ctr++;
        }
    }
    if (lock) SDL_mutexV(imp->as_mutex);
    return ctr;
}

/**
 * Templatized version of AudioSystemMixer, that knows how each audio format
 * can be mixed
 */
template <class T>
class AudioSystemMixerT : public virtual AudioSystemMixer {
protected:
    virtual void doPlay(void *outputBuffer,
                        unsigned long framesPerBuffer,
                        int channelOffset);

    virtual void doRecord(const void *inputBuffer,
                          unsigned long framesPerBuffer,
                          int channelOffset);

    /** Add \a val to \a accum, clipping if it overflows */
    void clipadd(T& accum, const T& val);
    AudioSystemMixerT(AudioSystemImpl *iimp, AUDIO_FORMAT format, unsigned ttracks, unsigned rec_tracks)
        :
    AudioSystemMixer(iimp, format, ttracks, rec_tracks) {}
public:
    virtual ASSample* loadSample(const std::string &name, bool stream);
};

#define INHERIT_MIXER_FUNCTIONS virtual AudioSystemImpl* impl() { return this; } \
    virtual void doRecord(const void *outputBuffer, unsigned long framesPerBuffer, int channelOffset) { \
        return AudioSystemMixerT<T>::doRecord(outputBuffer, framesPerBuffer, channelOffset); \
    } \
    virtual void doPlay(void *outputBuffer, unsigned long framesPerBuffer, int channelOffset) { \
        return AudioSystemMixerT<T>::doPlay(outputBuffer, framesPerBuffer, channelOffset); \
    } \
    virtual bool findRec(ASSample **rec, int *trackno = 0, bool **pausevar = 0, Recording ***rechome = 0) { \
        return AudioSystemMixerT<T>::findRec(rec, trackno, pausevar, rechome); \
    } \
    virtual ASSample* loadSample(const std::string &name, bool dostream) { \
        return AudioSystemMixerT<T>::loadSample(name, dostream); \
    } \
    virtual ASSample** getSampleHolder(bool loop = false, float vol = 1.0f, unsigned* trackno = 0) { \
        return AudioSystemMixerT<T>::getSampleHolder(loop, vol, trackno); \
    } \
    virtual Recording** getRecordHolder(unsigned* trackno = 0) { \
        return AudioSystemMixerT<T>::getRecordHolder(trackno); \
    } \
    virtual unsigned stopSample(ASSample *samp, int trackno = -1, bool lock = true) { \
        return AudioSystemMixerT<T>::stopSample(samp, trackno, lock); \
    } \
    virtual unsigned pauseSample(ASSample *samp, int trackno = -1, bool lock = true) { \
        return AudioSystemMixerT<T>::pauseSample(samp, trackno, lock); \
    } \
    virtual unsigned setVolume(ASSample *samp, float vol = 1.0f, int trackno = -1) { \
        return AudioSystemMixerT<T>::setVolume(samp, vol, trackno); \
    } \
    virtual bool isPlaying(ASSample *samp, int trackno) { \
        return AudioSystemMixerT<T>::isPlaying(samp, trackno); \
    }


template <class T>
inline void AudioSystemMixerT<T>::clipadd(T& accum, const T& val) {
    //if (std::numeric_limits<T>::is_integer) {
    const T tmp = accum + val;
    accum = (((accum & val & ~tmp) | (~accum & ~val & tmp)) < 0) ?
        (accum < 0 ?                  /* under/overflow/clipping */
         std::numeric_limits<T>::min()  :
         std::numeric_limits<T>::max()) :
        tmp;
}

template <>
inline void AudioSystemMixerT<float>::clipadd(float& accum, const float& val) {
    accum += val;
}

template <>
inline void AudioSystemMixerT<double>::clipadd(double& accum, const double& val) {
    accum += val;
}

template <class T>
void AudioSystemMixerT<T>::doRecord(const void *inputBuffer,
                                    unsigned long framesPerBuffer,
                                    int channelOffset)
{
    AudioSystemImpl *ai = impl();
    const unsigned long insz = channelOffset < 0 ? ai->iframe_size * framesPerBuffer : framesPerBuffer;

    if (!ai->all_rec_paused) for (unsigned i = 0; i < record_tracks; ++i) {
        Recording *r = rtrack[i].rec;
        if (!rtrack[i].paused && r) {
            if (inputBuffer) {
                r->fill(inputBuffer, insz, channelOffset, ai->inChannels);
            } else if (channelOffset < 0) { //fill with silence
                r->fillSilence(insz);
            }
        }
    }
    ai->vu_level = PCMSample::updateVU(static_cast<const float*>(inputBuffer),
                                       insz/sizeof(float),
                                       ai->vu_level);
}

template <class T>
void AudioSystemMixerT<T>::doPlay(void *outputBuffer,
                                  unsigned long framesPerBuffer,
                                  int channelOffset) {
    if (!outputBuffer)
        return;
    AudioSystemImpl *ai = impl();

    T * __restrict opb = static_cast<T*>(outputBuffer);
    unsigned nmixed = 0;
    const unsigned outChan = channelOffset >= 0 ? 1 : ai->outChannels;
    const unsigned chanOff = channelOffset >= 0 ? channelOffset : 0;
    const unsigned ofz = ai->oframe_size / (channelOffset >= 0 ? ai->outChannels : 1);

    //DODEBUG(INTERRUPT, ("In doPlay()"));
    //fprintf(stderr, "in doplay\n");
    //fputc('P', stderr);

    memset(outputBuffer, 0, ofz * framesPerBuffer);

    for (unsigned i = 0; i < tracks; ++i) {
        Mix &m = track[i];
        if (!m.sam || m.paused)
            continue;

        const bool v = m.vol != 1.0f;
        const unsigned ich = m.sam->getChannels(); // number of channels in sample/input
        const unsigned icht = ich * sizeof(T);     // size in bytes of each input frame

        /* looping, volume adjust, (frame) offset */
        unsigned &offset = m.off;  // Frame offset into sample

        unsigned long byteoff = offset * icht;     // byte offset into input sample
        unsigned long bytes_avail;                 // bytes remaining in current input sample buffer
        // pointer to current input sample buffer
        const T* __restrict ipb = static_cast<const T*>(m.sam->getOffBytes(byteoff, bytes_avail));
        unsigned long frames_avail = bytes_avail / icht;    // frames remaining in sample input buffer

        if (!m.looping && (!ipb || frames_avail <= 0)) {
            // we are not looping and the input has run out.. time to reset/stop
            // no chance of redemption, because the last call would have been to ASSample::mixed()
            m.reset();
            continue;
        }

        unsigned opb_offset = 0; // frame offset into the output buffer

        do { //while output buffer has room, and we haven't run out of input
            if (m.looping && frames_avail <= 0) {
                // if we are looping and nothing is left, try to rewind
                offset = 0;
                ipb = static_cast<const T*>(m.sam->getOffBytes(0, bytes_avail));
                if (!ipb || bytes_avail <= 0) {
                    m.reset();
                    break; /* loop failed -- sample not loopable*/
                }
                frames_avail = bytes_avail / icht;
            }

            /* how many frames are left in the sample stream */
            // max number of frames that can fit into output buffer
            const unsigned long nmax = framesPerBuffer - opb_offset;
            // number of frames we will put into output buffer this round
            const unsigned long n = (frames_avail > nmax) ? nmax : frames_avail;

            if  (ich == 1) {
                /* mix mono -> N channels */
                for (unsigned f = opb_offset; f < n; ++f) {
                    /* this is nested backwards so unless we unroll manually, it won't get vectorised */
                    for (unsigned c = 0; c < outChan; ++c) {
                        clipadd(opb[outChan*f + c],
                                v ? static_cast<T>(ipb[f] * m.vol)
                                : ipb[f]);
                    }
                }
                m.sam->mixed(n, track + i, channelOffset, ipb);
                if (channelOffset < 0 || chanOff + 1 == ai->outChannels)
                    offset += n;
            } else if (ich == outChan) {
                /* this is the most important, and it typically gets vectorized - yay */
                for (unsigned f = opb_offset; f < n*ich; ++f) {
                    clipadd(opb[f],
                            v ? static_cast<T>(ipb[f] * m.vol)
                            : ipb[f]);
                }
                m.sam->mixed(n, track + i, -1, ipb);
                offset += n;
            } else if (outChan == 1) {
                if (chanOff < ich) {
                    /* just mix first track, with the offset */
                    /* this guy can't vectorize because we are effectively "striping" data out of the input buffer */
                    for (unsigned f = opb_offset; f < n; ++f) {
                        clipadd(opb[f],
                                v ? static_cast<T>(ipb[f*2 + chanOff] * m.vol)
                                : ipb[f*2 + chanOff]);
                    }
                    m.sam->mixed(n, track + i, channelOffset, ipb);
                }
                if (chanOff + 1 == ai->outChannels)
                    offset += n;
            } else {
                fprintf(stderr, "skipped %u -> %u channel conversion not supported \n", ich, ai->outChannels);
                /* not supported -- skip */
                offset += n;
            }

            opb_offset += n; /* move the output buffer along */

            if (!m.looping && opb_offset < framesPerBuffer) {
                /* see if we can get more data */
                byteoff = offset * icht;
                ipb = static_cast<const T*>(m.sam->getOffBytes(byteoff, bytes_avail));
                /* if bytes_avail == 0 at this point we cannot continue
                 * because we have already called mixed(), so if there was more
                 * then the sample would tell us now.. UNLESS we are looping
                 * which we deal with at the top of the loop anyway
                 */
                frames_avail = bytes_avail / icht;
            }
        } while (ipb && opb_offset < framesPerBuffer && (m.looping || frames_avail > 0));
        ++nmixed;

        if (!m.looping && frames_avail <= 0) {
            m.reset();
        }
    }


    if (nmixed > 0) {
        DODEBUG(VINTERRUPT, ("Mixed %d samples into %s output", nmixed, typeid(T).name()));
    }
    if (nmixed == 0 && ai->record_after_play) {
        Recording **rp = getRecordHolder();
        *rp = ai->record_after_play;
        ai->record_after_play = 0;
    }
    //fprintf(stderr, "%d\n", nmixed);

    SDL_LockMutex(imp->hookMutex);
    for (AudioSystemImpl::HookList::const_iterator it = imp->callbackHooks.begin(); it != imp->callbackHooks.end(); ++it)
        (*it)->hook(outputBuffer, framesPerBuffer);
    SDL_UnlockMutex(imp->hookMutex);
} /*FOLD00*/


template <class T>
ASSample* AudioSystemMixerT<T>::loadSample(const std::string &name, bool dostream) { /*FOLD00*/
    ASSample *ret = 0;
    //AudioSystemImpl *imp = impl();
    //note we only need to take out a registry mutex for this
    SDL_mutexP(imp->registry_mutex);

    AudioSystemImpl::REGISTRY::iterator it = imp->registry.find(name);
    if (it == imp->registry.end()) {
        try {
            if (dostream) {
                ret = new StreamedFile<T>(name, AudioSystem::RINGBUFFER_BYTES, mix_format);
            } else {
                ret = new FileSampleT<T>(name, false /*imp->outChannels == 2*/, mix_format);
            }

            /* sample rates still should match, but no huge dramas if they don't --
             * we will just play it slower/faster.
             */
            if (imp->sampRate != ret->getSamRate()) {
                DODEBUG(WARNING, ("Warning: the sample rate of %s (%fHz) differs from the current AudioSystem (%fHz)\n"
                                  "\tattempting a resample..",
                                  name.c_str(), ret->getSamRate(), imp->sampRate));
                if (!ret->resample(imp->sampRate))
                    DODEBUG(WARNING, ("No resampling joy"));
            }

            imp->registry.insert(AudioSystemImpl::REGISTRY::value_type(name, ret));
        } catch (const char *err) {
            DODEBUG(WARNING, ("Exception caught at loadSample: %s",
                              err));
        } catch (std::bad_alloc &) {
            DODEBUG(ERROR, ("Not enough memory to load %s\n",
                            name.c_str()));
        }
    } else {
        ret = it->second;
    }
    SDL_mutexV(imp->registry_mutex);
    return ret;
} /*FOLD00*/


#endif
