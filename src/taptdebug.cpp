/* $Id$ $URL$ */
#include "taptdebug.h"

/**\file taptdebug.cpp
 * Debugging functions
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

#include <stdarg.h>
#include <stdio.h>

FILE* taptaudio_debug_file = stderr;
unsigned long TAPTDBGLEV = DEBUG_INFO;

/** Printf function for debugging -- outpus to debug_file */
int taptaudio_debug_printf(const char* fmt, ... /*args*/) {
    int ret;
    va_list ap;
    va_start(ap, fmt);
    ret = vfprintf(taptaudio_debug_file, fmt, ap);
    va_end(ap);
    return ret;
}

