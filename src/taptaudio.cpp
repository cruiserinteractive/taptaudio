/* $Id$ $URL$ */

/**\file taptaudio.cpp
 * Implementation of the Audio subsystem (common parts)
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

#include "taptaudio_impl.h"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "taptdebug.h"

unsigned AudioSystem::AUDIO_TRACKS = 32;
unsigned AudioSystem::RECORD_TRACKS = 8;
unsigned long AudioSystem::RINGBUFFER_BYTES = 262144;
AUDIO_FORMAT AudioSystem::DEFAULT_AUDIO_FORMAT = AF_Float32;
bool AudioSystem::NEVER_DELETE_IMPL = false;


// this is OK for streaming midway through app but loadtimes at startup
// need a bigger buffer. so for current uses, keep it big by default.
//unsigned long AudioSystem::RINGBUFFER_RECORDBYTES = 32768;
unsigned long AudioSystem::RINGBUFFER_RECORDBYTES =  262144;
AudioSystem* AudioSystem::instance = 0;

AudioSystem::~AudioSystem() {
    if (impl) {
        impl->stop();
        impl->waitstop();
        if (NEVER_DELETE_IMPL) {
            fprintf(stderr, "Safe exits are enabled -- not deleting impl\n");
        } else {
            fprintf(stderr, "Deleting AudioSystem impl\n");
            delete impl;
        }
    }
    AudioSystem::instance = 0;
}

AudioSystem* AudioSystem::get() {return instance;}
bool AudioSystem::isRunning() {return instance;}
AudioSystem::AudioSystem(const AudioSystem&) {}
AudioSystem& AudioSystem::operator=(const AudioSystem&) {return *this;}

ASSample* AudioSystem::loadSample(const std::string &name, bool stream) {
    return (this && impl) ? impl->loadSample(name, stream) : 0;
}

bool AudioSystem::map(const std::string &name, ASSample *sam) {
    return (this && impl) ? impl->map(name, sam) : false;
}

bool AudioSystem::map(ASSample *sam) {
    return (this && impl) ? impl->map("", sam) : false;
}

ASSample* AudioSystem::loadRawSample(const std::string &name, void* data, unsigned long size) {
    return (this && impl && data) ? impl->loadRawSample(name, data, size) : 0;
}

bool AudioSystem::mixSample(ASSample *samp, bool record_after, double record_size, float vol, unsigned* trackno) {
    return (this && impl && samp) ? impl->mixSample(samp, record_after, record_size, false, vol, trackno) : false;
}

bool AudioSystem::mixSample(ASSample *samp, float vol, unsigned* trackno) {
    return (this && impl && samp) ? impl->mixSample(samp, false, 0, false, vol, trackno) : false;
}

bool AudioSystem::loopSample(ASSample *samp, float vol, unsigned* trackno) {
    return (this && impl && samp) ? impl->loopSample(samp, vol, trackno) : false;
}

bool AudioSystem::freeSample(ASSample *samp) {
    return (this && impl && samp) ? impl->freeSample(samp) : false;
}

unsigned AudioSystem::stopSample(ASSample *samp, int trackno) {
    return (this && impl && samp) ? impl->stopSample(samp, trackno) : false;
}

bool AudioSystem::isPlaying(ASSample *samp, int trackno) const {
    return (this && impl && samp) ? impl->isPlaying(samp, trackno) : false;
}

bool AudioSystem::startRec(double secondsMax, unsigned *rtrackno, ASSample **recref) {
    return (this && impl) ? impl->startRec(secondsMax, rtrackno, recref) : false;
}

bool AudioSystem::startRec(unsigned *rtrackno, ASSample **recref) {
    return startRec(30.0, rtrackno, recref);
}

bool AudioSystem::streamRec(const std::string &file, FILE_ENCODING enc, float quality, unsigned *rtrackno, ASSample **recref) {
    RecordOptions opts(enc, quality);
    return streamRec(file, opts, rtrackno, recref);
}
bool AudioSystem::streamRec(const std::string &file, FILE_ENCODING enc, unsigned *rtrackno, ASSample **recref) {
    return streamRec(file, enc, 2.0f, rtrackno, recref);
}
bool AudioSystem::streamRec(const std::string &file, const RecordOpts &opts, unsigned *rtrackno, ASSample **recref) {
    return (this && impl) ? impl->streamRec(file, opts, rtrackno, recref) : false;
}

bool AudioSystem::pauseRec(int rtrackno, ASSample *rec) {
    return (this && impl) ? impl->pauseRec(rec, rtrackno) : false;
}
bool AudioSystem::resumeRec(int rtrackno, ASSample *rec) {
    return (this && impl) ? impl->resumeRec(rec, rtrackno) : false;
}

bool AudioSystem::previewRec(float vol, unsigned* trackno, int rtrackno, ASSample *rec) {
    return (this && impl) ? impl->previewRec(vol, trackno, rtrackno, rec) : false;
}

ASSample* AudioSystem::stopRec(const std::string &name, ASSample *rec, int trackno, const AudioSystem::RecordOpts &opts) {
    return (this && impl) ? impl->stopRec(name, rec, trackno, opts) : 0;
}

ASSample* AudioSystem::stopRec(const std::string &name, ASSample *rec, int trackno, bool save, bool save_in_thread, FILE_ENCODING enc, float quality) {
    RecordOptions opts(true, save, save_in_thread, enc, quality);
    return (this && impl) ? impl->stopRec(name, rec, trackno, opts) : 0;
}
ASSample* AudioSystem::stopRec(const std::string &name, bool save, bool save_in_thread, FILE_ENCODING enc, float quality) {
    RecordOptions opts(true, save, save_in_thread, enc, quality);
    return (this && impl) ? impl->stopRec(name, 0, -1, opts) : 0;
}

bool AudioSystem::isRecording() {
    return (this && impl) ? impl->isRecording() : false;
}

unsigned AudioSystem::setVolume(ASSample *samp, float vol, int trackno) {
    return (this && impl && samp) ? impl->setVolume(samp, vol, trackno) : 0;
}
unsigned AudioSystem::pauseSample(ASSample *samp, int trackno, bool setpause) {
    return (this && impl && samp) ? impl->pauseSample(samp, trackno, setpause) : 0;
}

unsigned AudioSystem::unpauseSample(ASSample *samp, int trackno) {
    return (this && impl && samp) ? impl->pauseSample(samp, trackno, false) : 0;
}

void AudioSystem::stop() {
    if (this && impl)
        impl->stop();
}

void AudioSystem::waitstop() {
    if (this && impl)
        impl->waitstop();
}

ASSample* AudioSystem::loadSample(const char* name, bool stream) {
    return loadSample(std::string(name), stream);
}

bool AudioSystem::map(const char* name, ASSample *sam) {
    return map(std::string(name), sam);
}

ASSample* AudioSystem::loadRawSample(const char* name, void* data, unsigned long size) {
    return loadRawSample(std::string(name), data, size);
}

ASSample* AudioSystem::stopRec(const char* name, bool save, bool save_in_thread, FILE_ENCODING enc, float quality) {
    return stopRec(std::string(name), save, save_in_thread, enc, quality);
}

ASSample* AudioSystem::stopRec(const char* name, ASSample *rec, int trackno, bool save, bool save_in_thread, FILE_ENCODING enc, float quality) {
    return stopRec(std::string(name), rec, trackno, save, save_in_thread, enc, quality);
}

ASSample* AudioSystem::stopRec(const char* name, ASSample *rec, int trackno, const RecordOpts &opts) {
    return stopRec(std::string(name), rec, trackno, opts);
}

bool AudioSystem::streamRec(const char *file, FILE_ENCODING enc, float quality, unsigned *rtrackno, ASSample **recref) {
    return streamRec(std::string(file), enc, quality, rtrackno, recref);
}

bool AudioSystem::streamRec(const char* file, FILE_ENCODING enc, unsigned *rtrackno, ASSample **recref) {
    return streamRec(std::string(file), enc, 2.0f, rtrackno, recref);
}
bool AudioSystem::streamRec(const char* file, const RecordOpts &opts, unsigned *rtrackno, ASSample **recref) {
    return streamRec(std::string(file), opts, rtrackno, recref);
}

void AudioSystem::setDebugLevel(unsigned level) {
    SETDEBUG(level);
}

void AudioSystem::setDebugFile(void* FILESTAR) {
    taptaudio_debug_file = static_cast<FILE*>(FILESTAR);
}

float AudioSystem::inputVU() const {
    return (this && impl) ? impl->inputVU() : 0.0f;
}

bool AudioSystem::canPlay() const {
    return (this && impl);
}

bool AudioSystem::canRecord() const {
    return (this && impl);
}

void AudioSystem::addCallbackHook(AudioSystemHook *hook) {
    if (this && impl)
        impl->addCallbackHook(hook);
}

void AudioSystem::removeCallbackHook(AudioSystemHook *hook) {
    if (this && impl)
        impl->removeCallbackHook(hook);
}


void ASSample::mixed(unsigned long, // frames,
                     void*, // handle = 0,
                     int, // channelOffset = -1,
                     const void* // data = 0)
                    ) {
}

float ASSample::getVU() {
    return 0.0f;
}

long ASSample::msDuration() const {
    return static_cast<long>(1000.0 * numFrames() / getSamRate() + 0.49999);
}
double ASSample::getDuration() const {
    return numFrames() / getSamRate();
}

bool ASSample::split(unsigned long /*where*/, ASSample ** /*start*/, ASSample ** /*end*/) const {
    return false;
}

ASSample* ASSample::destructive_split(unsigned long /*where*/) {
    return 0;
}
bool ASSample::save(const char* path, FILE_ENCODING enc, float quality) {
    return save(std::string(path), enc, quality);
}

bool ASSample::save(const std::string &/*path*/, FILE_ENCODING /*enc*/, float /*quality*/) {
    return false;
}

long ASSample::remove_silence(float /*threshold*/, unsigned /*window*/) {
    return -1;
}

/**\file taptaudio.py
 * This file is automatically generated by SWIG, from taptaudio.i and taptaudio.h
 */

