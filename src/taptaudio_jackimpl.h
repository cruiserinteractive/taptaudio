/* $Id$ $URL$ */
#ifndef TAPTAUDIO_JACKIMPL_DOT_AITCH
#define TAPTAUDIO_JACKIMPL_DOT_AITCH

#include "taptaudio_impl.h"

/**\file taptaudio_jackimpl.h
 * Header for JACK implementation of taptaudio
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

#include <jack/jack.h>

/** The JACK implementation of the audio system */
class JackAudioSystemImpl : public AudioSystemImpl {
public: //needed by callback

    jack_client_t *client; ///< Client handle
    stl::vector<jack_port_t*> input_ports;  ///< Input ports
    stl::vector<jack_port_t*> output_ports; ///< Output ports

public:
    /** Virtual destructor, free the portaudio handles and clear the registry (?) */
    virtual ~JackAudioSystemImpl();

    /** Create an AudioSystemImpl */
    JackAudioSystemImpl(unsigned inputChannels = 1,
                        unsigned outputChannels = 2,
                        double sampleRate = 44100,
                        AUDIO_FORMAT format = AF_Int16,
                        int deviceIDin = 0,
                        int deviceIDout = 0);

    virtual void cease();

    static void listDevices();
};

/**
 * A JACK implementation of AudioSystemMixer that mixes any number of tracks
 * with appropriate clipping.
 */
class JackAudioSystemMixer : public JackAudioSystemImpl, public AudioSystemMixer {
public:
    virtual AudioSystemImpl* impl() { return this; }
    JackAudioSystemMixer(unsigned inputChannels = 1,
                         unsigned outputChannels = 2,
                         double sampleRate = 44100,
                         AUDIO_FORMAT format = AF_Int16,
                         unsigned ttracks = 32,
                         unsigned rtracks = 8,
                         int deviceIDin = 0,
                         int deviceIDout = 0);
};

/**
 * Templatized portaudio version of AudioSystemMixerT
 */
template <class T>
class JackAudioSystemMixerT : public virtual JackAudioSystemImpl, public AudioSystemMixerT<T> {
public:

    INHERIT_MIXER_FUNCTIONS

    JackAudioSystemMixerT(unsigned inputChannels = 1,
                          unsigned outputChannels = 2,
                          double sampleRate = 44100,
                          AUDIO_FORMAT format = AF_Float32,
                          unsigned ttracks = 32,
                          unsigned rtracks = 8,
                          int deviceIDin = 0,
                          int deviceIDout = 0)
        :
    JackAudioSystemImpl(inputChannels, outputChannels, sampleRate, format, deviceIDin, deviceIDout)
        ,
        AudioSystemMixer(this, AF_Float32, ttracks, rtracks)
        ,
        AudioSystemMixerT<T>(this, AF_Float32, ttracks, rtracks)
    {
    }
};

#endif
