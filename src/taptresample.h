/* $Id$ $URL$ */
#ifndef TAPTRESAMPLE_DOT_AITCH
#define TAPTRESAMPLE_DOT_AITCH

/**\file taptresample.h
 * Declaration of resample function
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Rev$
 * $Date$
 */


/** Resample float data */
unsigned long taptresample(float **data,
                           int channels,
                           unsigned long frames,
                           double oldrate,
                           double newrate,
                           bool highquality = false);

template <class T>
    unsigned long template_resample(T ** /*data*/,
                                    int /*channels*/,
                                    unsigned long /*frames*/,
                                    double /*oldrate*/,
                                    double /*newrate*/,
                                    bool /*highquality*/ = false) {
        return 0;
    }

template <>
    unsigned long template_resample(float **data,
                                    int channels,
                                    unsigned long frames,
                                    double oldrate,
                                    double newrate,
                                    bool highquality);

const char* taptresample_lastError();

#endif

