/* $Id$ $URL$ */
#ifndef STREAMING_DOT_AITCH
#define STREAMING_DOT_AITCH

/**\file stremaing.h
 * Decs for Streaming Samples
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

#include "sample.h"
#include "ringbuffer/ringbuf.h"
#include "threadman/semwaiter.h"
#include <SDL_version.h>


/** A Recording with a ringbuffer that streams to disk in a separate thread */
class StreamedRecording : public Recording {
    StreamedRecording(const StreamedRecording&);
    StreamedRecording& operator=(const StreamedRecording&) {return *this;}
    typedef Recording super;
protected:
    RingBuffer rb;
    SNDFILE *sfile;
    SDL_Thread *flusher;
    SemWaiter flushstep;
    DestroyWaiter destroy;
    std::string path;
    int warnings;

    FILE_ENCODING encoding;
    float quality;
    bool overwrite;

    void doWrite();
    void close();
    void complete();

public:
    int flush();
    StreamedRecording(const std::string &ppath,
                      unsigned long ringbuffer_size,
                      unsigned chans = 2,
                      AUDIO_FORMAT format = AF_Int16,
                      double samplerate = 44100,
                      FILE_ENCODING enc = AF_ENC_WAV,
                      float qual = 2.0,
                      bool foverwrite = true);

    ~StreamedRecording();

    virtual void fill(const void *more, unsigned long sz, int channelOffset = -1, int bufChannels = 1);
    virtual bool save(const std::string &path, FILE_ENCODING enc = AF_ENC_WAV, float quality = 2.0);
    virtual void fillSilence(unsigned long sz);
    virtual void trim();

    /* callers of these expect contiguous memory, so return info about "oldest" bit of ring buffer */
    virtual const void* getBytes() const;
    virtual const void* getOffBytes(unsigned long offset, unsigned long &available) const;

    virtual unsigned long numBytes() const;

    virtual void mixed(unsigned long frames,
                       void* handle = 0,
                       int channelOffset = -1,
                       const void *data = 0);

    virtual ASSample *makePlayable(bool del_if_new = true);
    virtual bool canSaveInThread(bool /*want_to_reuse*/) {return false; /*!want_to_reuse;*/}
};

template <class T>
class StreamedFile : public FileSampleT<T> {
    StreamedFile(const StreamedFile&);
    StreamedFile& operator= (const StreamedFile&) {return *this;}
    typedef FileSampleT<T> super;
protected:
    RingBuffer rb;
    SNDFILE *file;
    SDL_Thread *streamer;
    SemWaiter streamstep;
    DestroyWaiter destroy;
    std::string path;
    int warnings;

    void close();

    static int startFillThread(void* data) {
        return static_cast<StreamedFile<T>* >(data)->fillThread();
    }

public:

    int fillThread();

    StreamedFile (const std::string& path, unsigned long ringbuffer_size, AUDIO_FORMAT force_format = AudioSystem::DEFAULT_AUDIO_FORMAT);
    virtual ~StreamedFile();
    virtual bool resample(double /*newrate*/, bool /*highquality*/ = false) { return false; }

    /* callers of these expect contiguous memory, so return info about "oldest" bit of ring buffer */
    virtual const void* getBytes() const {return rb.bufferStart();}
    virtual const void* getOffBytes(unsigned long offset, unsigned long &available) const;

    /* this is linked with getBytes(), so return max_size unless the sample fits completely in the buffer */
    virtual unsigned long numBytes() const {
        return PCMSample::numBytes() < rb.max_size() ? PCMSample::numBytes() : rb.max_size();
    }

    virtual void mixed(unsigned long frames,
                       void* handle = 0,
                       int channelOffset = -1,
                       const void *data = 0);
};

template <class T>
StreamedFile<T>::StreamedFile(const std::string &ppath, unsigned long ringbuffer_size, AUDIO_FORMAT force_format)
: super(force_format), rb(ringbuffer_size), file(0), streamer(0),
streamstep(), destroy(),
path(ppath), warnings(0)
{
    SF_INFO sfinfo;
    memset(&sfinfo, 0, sizeof(sfinfo));
    file = sf_open(path.c_str(), SFM_READ, &sfinfo);
    if (!file) {
        DODEBUG(ERROR, ("Unable to open %s: %s",
                        path.c_str(), sf_strerror(file)));
        throw "Unable to open file";
    }

    try {
        PCMSample::inherit_sfinfo(sfinfo, sizeof(T), force_format);
        PCMSample *me = this;
        int l_channels = me->channels;
        //unsigned long l_bytes_size = me->bytes_size;
        //unsigned long l_nframes = me ->nframes;
        //double l_rate = me->rate;
        //AUDIO_FORMAT l_fmt = me->fmt;
        //unsigned l_data_sz = me->data_sz;
        //float l_vu = me->vu;

        /* fill the buffer, to begin */

        RingBuffer::byte *b1, *b2;
        RingBuffer::size_type s1, s2;
        (void)rb.write_regions(&b1, &s1, &b2, &s2, rb.write_avail());
        if (s2) {
            DODEBUG(NOTICE, ("Weirdness (shouldn't happen): buffer write pos offset at start"));
        }

        sf_count_t items_avail = s1 / sizeof(T);
        /* check if whole sample fits in buffer */
        if (l_channels * sfinfo.frames < items_avail)
            items_avail = l_channels*sfinfo.frames;
        DODEBUG(INFO, ("%u bytes in buffer, %u items available, %u frames in file and %d channels (try to read %u items)",
                       (unsigned)s1, (unsigned)(s1/sizeof(T)), (unsigned)sfinfo.frames, l_channels, (unsigned)items_avail));

        sf_count_t numread = lsf_aread(file, reinterpret_cast<T*>(b1), items_avail);
        rb.write_advance(items_avail * sizeof(T));

        if (numread != items_avail) {
            DODEBUG(WARNING, ("Only %u of a desired %u initial items were read from %s: %.2f%% of the file, filling the buffer to %.2f%%",
                              (unsigned)numread, (unsigned)items_avail, path.c_str(),
                              100.0*numread/(sfinfo.frames*l_channels), 100.0*numread/(s1 / sizeof(T)) ));
        } else {
            DODEBUG(FUNCTION, ("%u initial items were read from %s: %.2f%% of the file, filling the buffer to %.2f%%",
                               (unsigned)numread, path.c_str(),
                               100.0*numread/(sfinfo.frames*l_channels), 100.0*numread/(s1 / sizeof(T)) ));
        }

        /* start the thread */
        destroy.alternate_started();
#if SDL_VERSION_ATLEAST(2, 0, 0)
        streamer = SDL_CreateThread(&startFillThread, "taptaudio_streamer", this);
#else
        streamer = SDL_CreateThread(&startFillThread, this);
#endif

    } catch (...) {
        DODEBUG(ERROR, ("Unsupported frame encoding format in %s\n\tcurrently we only support signed primitive types",
                        path.c_str()));
        sf_close(file);
        throw;
    }
}

template <class T>
const void* StreamedFile<T>::getOffBytes(unsigned long offset, unsigned long &available) const {
    if (!streamstep.running())
        return 0; /* streaming has stopped *for some reason* */

    RingBuffer::byte *b1, *b2;
    RingBuffer::size_type s1, s2;
    /* Me::mixed moves the read position, so ignores offset and always reads from the start position */
    (void)offset;
    /* if the sample has ended read_avail will be 0 */
    (void)rb.read_regions(&b1, &s1, &b2, &s2, rb.read_avail());
    available = s1;
    return b1;
}

template <class T>
void StreamedFile<T>::mixed(unsigned long xframes,
                            void* handle,
                            int channelOffset,
                            const void *ldata) {
    int l_channels = PCMSample::getChannels();
    if (channelOffset < 0 || channelOffset + 1 ==  l_channels /* impl()->outChannels*/) {
        rb.read_advance(xframes*l_channels*sizeof(T));
        streamstep.post();
        super::mixed(xframes, handle, channelOffset, ldata);
    }
}

template <class T>
StreamedFile<T>::~StreamedFile() {
    destroy.wants_to_destroy(streamstep, &streamer);
    if (file) {
        sf_close(file);
        file = 0;
    }
}

template <class T>
int StreamedFile<T>::fillThread() {
    enum {BUFINFO_INTERVAL = 1000};
    unsigned iter = 1;
    RingBuffer::size_type accum = 0;
    RingBuffer::size_type worst = 0;
    DODEBUG(INFO, ("FILL Streamer %p waiting for first buffer..", (void*)this));
    while (streamstep) {
        //fprintf(stderr, "!");
        RingBuffer::size_type avail = rb.write_avail();

        if (avail) {
            accum += avail;
            if (avail > worst)
                worst = avail;

            RingBuffer::byte *b1, *b2;
            RingBuffer::size_type s1, s2;
            (void)rb.write_regions(&b1, &s1, &b2, &s2, avail);

#if 0
            sf_count_t items_room = avail / sizeof(T);

            /* check if rest of sample fits in buffer */
            if (l_channels * sfinfo.frames < items_avail)
                items_avail = l_channels*sfinfo.frames;
#endif

            sf_count_t numread = lsf_aread(file, reinterpret_cast<T*>(b1), s1/sizeof(T));
            if (static_cast<unsigned long>(numread) == s1/sizeof(T))
                numread += lsf_aread(file, reinterpret_cast<T*>(b2), s2/sizeof(T));

            rb.write_advance(numread * sizeof(T));

            if (static_cast<unsigned long>(numread) < avail/sizeof(T)) {
                DODEBUG(NOTICE, ("Entire file has been streamed (read %u but there's room for %u). Leaving.",
                                 (unsigned)numread, (unsigned)(avail/sizeof(T))));
                break;
            }

            if (iter == BUFINFO_INTERVAL) {
                DODEBUG(INFO, ("RingBuffer: %u fills, %lu bytes total, avg. %.1f%% capacity, worst %u (%.1f%%)",
                               (unsigned)BUFINFO_INTERVAL, (unsigned long)accum,
                               100.0 - 100.0*accum/(BUFINFO_INTERVAL * rb.max_size()),
                               (unsigned)worst, 100.0 - 100.0*worst/rb.max_size()));
                worst = 0;
                accum = 0;
                iter = 1;
            } else {
                ++iter;
            }
        }
    }
    destroy.ready_to_destroy();
    return 0;
}

#endif
