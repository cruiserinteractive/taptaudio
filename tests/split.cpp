//$Id$ $URL$

#include "taptaudio.h"

#include <SDL.h>
#include <iostream>

using namespace std;

#ifndef WAVPATH
#define WAVPATH "."
#endif

int main(int argc, char *argv[]) {
    AudioSystem::setDebugLevel(5);
    int samplerate = 0;
    if (argc > 2 && atoi(argv[2]))
        samplerate = atoi(argv[2]);
    AudioSystem as(AF_Default, samplerate);
    unsigned track;
    ASSample *s;
    if (argc > 1)
        s = as.loadSample(argv[1]);
    else
        s = as.loadSample(WAVPATH "/The_Stranglers_-_Golden_Brown.flac");

    cerr << "Splitting at 2000 milliseconds" << endl;
    ASSample *rhs = s->destructive_split(2000);
    as.map(rhs);
    as.map(rhs);
    cerr << "Playing for 3 seconds (should stop at 2)" << endl;
    as.mixSample(s);
    SDL_Delay(5000);
    cerr << "Playing split part for 5 seconds" << endl;
    as.mixSample(rhs);
    SDL_Delay(5000);

    return 0;
}

