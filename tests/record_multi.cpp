//$Id$ $URL$

#include "taptaudio.h"

#include <SDL.h>

AudioSystem as (AF_Default, 44100, 1 , 2);

void startrec(unsigned * where, Uint32 sec, int pos) {
    ASSample *t;
    printf("Starting a recording in %d seconds. ", sec);
    fflush(stdout);
    for (; sec; sec--) {
        printf(".");
        fflush(stdout);
        SDL_Delay(1000);
    }
    printf("TRACK %d STARTING\n", pos);
    fflush(stdout);
    as.startRec(where, &t);
    printf("\tGot put in track #%u (samp is %p).\n", *where, t);
    fflush(stdout);
}

void stoprec(int which, Uint32 sec, int pos) {
    printf("TRACK %d STOPPING in track %d in %d seconds. ", pos, which, sec);
    fflush(stdout);
    for (; sec; sec--) {
        printf(".");
        fflush(stdout);
        SDL_Delay(1000);
    }
    printf("Stopping\n");
    fflush(stdout);
    char buf[100];
    snprintf(buf, 100, "recording-%d.wav", pos);
    as.stopRec(buf, 0, which, true, true);
}

int main(int argc, char* argv[]) {
    //AudioSystem::listDevices();
    AudioSystem::setDebugLevel(6);
    unsigned tracks[5];
    ASSample *samps[5];
    printf("Streaming background recording to stream.wav in ADPCM\n");
    as.streamRec("stream.wav", AF_ENC_ADPCM, &tracks[0], &samps[0]);
    printf("\tGot put in track #%u (samp is %p).\n", tracks[0], (void*)samps[0]);

    startrec(tracks + 1, 4, 1);
    startrec(tracks + 2, 6, 2);
    stoprec(tracks[1], 4, 1);
    startrec(tracks + 3, 2, 3);
    stoprec(tracks[2], 4, 2);
    stoprec(tracks[3], 4, 3);

    as.stopRec("stream.wav" /* ignored */, NULL, (int)tracks[0]);

    printf("Stopped stream. Waiting for a sec\n");
    fflush(stdout);
    SDL_Delay(1000);

    samps[0] = as.loadSample("stream.wav");
    samps[1] = as.loadSample("recording-1.wav");
    samps[2] = as.loadSample("recording-2.wav");
    samps[3] = as.loadSample("recording-3.wav");
    samps[4] = as.loadSample("recording-4.wav");

    for (unsigned i = 0; i < 5; ++i) {
        if (samps[i]) {
            printf("Playing sample at index %u\n", i);
            fflush(stdout);
            as.mixSample(samps[i]);
            double duration = samps[i]->numFrames() / samps[i]->getSamRate();
            Uint32 delay = static_cast<Uint32>(duration * 1000 + 0.5);
            printf("Duration is %.3f seconds. Sleeping for %ums\n", duration, delay);
            fflush(stdout);
            SDL_Delay(delay);
            printf("Cleaning sample at index %u in 0.5 seconds\n", i);
            SDL_Delay(500);
            fflush(stdout);
            as.freeSample(samps[i]);
            samps[i] = 0;
        } else {
            printf("Sample at index %u is NULL\n", i);
        }
        fflush(stdout);
    }
    return 0;
}

