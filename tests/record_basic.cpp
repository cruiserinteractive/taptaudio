//$Id$ $URL$

#include "taptaudio.h"

#include <SDL.h>

int main(int argc, char* argv[]) {
    AudioSystem as;
    fprintf(stderr, "Sleeping for 1 second while we record...\n");
    as.startRec();
    SDL_Delay(1000);
    ASSample *rec = as.stopRec("recordbasic.wav", true);
    if (!rec)
        return 1;
    fprintf(stderr, "Sleeping for 1 second while we play...\n");
    as.mixSample(rec);
    SDL_Delay(1000);
    return 0;
}

