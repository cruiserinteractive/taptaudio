//$Id$ $URL$
#include "taptaudio.h"

#include <SDL.h>

void vp(ASSample* s, float vol, Uint32 delay) {
    fprintf(stderr, "Playing at %f%% volume, then delaying %ums\n", vol*100, delay);
    AudioSystem::get()->loopSample(s, vol);
    SDL_Delay(delay);
}

int main(int argc, char* argv[]) {
    AudioSystem as;
    ASSample *s = as.loadSample(WAVPATH "/mix.wav");
    if (!s)
        return 1;
    vp(s, 1.0, 200);
    vp(s, 0.5, 300);
    vp(s, 2.0, 400);
    vp(s, 1.5, 500);
    vp(s, 0.25, 600);
    vp(s, 0.1, 700);
    fprintf(stderr, "Looping mix for 8 seconds\n");
    SDL_Delay(8000);
    fprintf(stderr, "Stopping.\n");
    as.stopSample(s);
    vp(s, 3.0, 800);
    vp(s, 4.0, 900);
    vp(s, 5.0, 1000);
//    vp(s, 6.0);
//    vp(s, 7.0);
//    vp(s, 8.0);
//    vp(s, 9.0);
//    vp(s, 10.0);
    SDL_Delay(8000);
    fprintf(stderr, "Looping mix for 8 seconds\n");
    return 0;
}

