//$Id$ $URL$

#include "taptaudio.h"

#include <SDL.h>

int main(int argc, char* argv[]) {
    //AudioSystem::listDevices();
    AudioSystem::setDebugLevel(6);
    AudioSystem as (AF_Default, 44100, 1 , 2);
    as.startRec();
    SDL_Delay(3000);
    ASSample *rec = as.stopRec("record.wav", true);
    if (!rec)
        return 1;
    as.mixSample(rec);
    SDL_Delay(4000);
    return 0;
}

