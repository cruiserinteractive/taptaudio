//$Id$ $URL$

#include "taptaudio.h"

#include <SDL.h>

int main(int argc, char* argv[]) {
    {
        AudioSystem as;
        fprintf(stderr, "Sleeping for 3 seconds while we record...\n");
        as.startRec();
        SDL_Delay(3000);
        ASSample *rec = as.stopRec("oggrecord.ogg", true, false, AF_ENC_OGG);
        if (!rec)
            return 1;
        fprintf(stderr, "Sleeping for 3 seconds while we play...\n");
        as.mixSample(rec);
        SDL_Delay(3000);
    }
    {
        AudioSystem as;
        fprintf(stderr, "Loading back from file...\n");
        ASSample *rec = as.loadSample("oggrecord.ogg");
        if (!rec)
            return 1;
        fprintf(stderr, "Sleeping for 3 seconds while we play...\n");
        as.mixSample(rec);
        SDL_Delay(3000);
    }
    return 0;
}

