//$Id$ $URL$

#include "taptaudio.h"

#include <SDL.h>

void vp(ASSample* s, float vol) {
    fprintf(stderr, "Playing for 1 second at %f%% volume\n", vol*100);
    AudioSystem::get()->mixSample(s, vol);
    SDL_Delay(1000);
}

int main(int, char *[]) {
    AudioSystem::setDebugLevel(5);
    AudioSystem as;
    ASSample *s = as.loadSample(WAVPATH "/hellow.wav");
    if (!s)
        return 1;

    vp(s, 1.0);
    vp(s, 0.5);
    vp(s, 2.0);
    vp(s, 1.5);
    vp(s, 0.25);
    vp(s, 0.1);
    vp(s, 3.0);
    vp(s, 4.0);
    vp(s, 5.0);
    vp(s, 6.0);
    vp(s, 7.0);
    vp(s, 8.0);
    vp(s, 9.0);
    vp(s, 10.0);
    return 0;
}

