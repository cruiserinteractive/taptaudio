//$Id$ $URL$

#include "taptaudio.h"

#include <SDL.h>
#include <iostream>

using namespace std;

#ifndef WAVPATH
#define WAVPATH "."
#endif

int main(int argc, char* argv[]) {
    //AudioSystem::listDevices();
    int samplerate = 0;
    if (argc > 2 && atoi(argv[2]))
        samplerate = atoi(argv[2]);
    AudioSystem::setDebugLevel(7);
    AudioSystem as (AF_Default, samplerate, 1 , 2);
    /*
    as.streamRec("stream-defaults.wav");
    SDL_Delay(3000);
    ASSample *rec1 = as.stopRec("ignored");
    if (!rec1)
        return 1;
    */
    as.streamRec("stream-2min.wav", AF_ENC_ADPCM);

    fprintf(stderr, "RECORDING in the background. Let's start streaming the file\n");

    unsigned track;
    ASSample *s;
    if (argc > 1)
        s = as.loadSample(argv[1], true);
    else
        s = as.loadSample(WAVPATH "/The_Stranglers_-_Golden_Brown.flac", true);
    cerr << "Playing for 5 seconds then stopping for 2 seconds, then resuming" << endl;
    as.mixSample(s);
    SDL_Delay(3000);
    as.stopSample(s);
    SDL_Delay(2000);
    as.mixSample(s);

    cerr << "Waiting 12 seconds" << endl;

    SDL_Delay(12000);
    ASSample *rec2 = as.stopRec("ignored");
    if (!rec2)
        return 1;

    //as.mixSample(rec1);
    //SDL_Delay(4000);
    cerr << "Mixing in the recording for 12 seconds" << endl;
    as.mixSample(rec2);
    SDL_Delay(12100);

    cerr << "Playing out" << endl;
    //SDL_Delay(1*60*1000);

    return 0;
}

