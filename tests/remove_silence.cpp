//$Id$ $URL$
#include "taptaudio.h"

#include <SDL.h>

void doremove(float threshold = 0, unsigned limit = 0) {
    fprintf(stderr, "Removing silence threshold = %f, limit = %u\n", threshold, limit);
    ASSample *s = AudioSystem::get()->loadSample(WAVPATH "/mix.wav");
    if (!limit) {
        s->remove_silence();
    } else {
        s->remove_silence(threshold, limit);
    }
    fprintf(stderr, "Looping adjusted for 5 seconds\n");
    AudioSystem::get()->loopSample(s);
    SDL_Delay(5000);
    AudioSystem::get()->stopSample(s);
    AudioSystem::get()->freeSample(s);
}

int main(int argc, char* argv[]) {
    AudioSystem as;
    ASSample *s = as.loadSample(WAVPATH "/mix.wav");
    if (!s)
        return 1;
    as.loopSample(s);
    fprintf(stderr, "Looping original for 5 seconds\n");
    SDL_Delay(5000);
    as.stopSample(s);
    doremove();
    doremove(0.05, 1000);
    doremove(0.01, 1000);
    return 0;
}

