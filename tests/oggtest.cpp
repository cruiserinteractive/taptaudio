//$Id: volumes.cpp 195 2005-08-23 05:41:49Z tapted $ $URL: svn+ssh://pc-g33-9.it.usyd.edu.au/var/svn/pub/taptaudio/trunk/tests/volumes.cpp $

#include "taptaudio.h"
#include <sndfile.h>
extern "C" {
#include "sndfile/utils.h"
}

#include <SDL.h>

int main(int argc, char *argv[]) {
    AudioSystem::setDebugLevel(50);
    AudioSystem as;
    ASSample *s;
    if (argc > 1)
        s = as.loadSample(argv[1]);
    else
        s = as.loadSample(WAVPATH "/The_Stranglers_-_Golden_Brown.ogg");
    if (!s) {
        dump_log_buffer(NULL);
        return 1;
    }
    as.mixSample(s);
    SDL_Delay(3*60*1000);
    return 0;
}

