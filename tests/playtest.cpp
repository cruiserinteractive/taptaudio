//$Id$ $URL$

#include "taptaudio.h"

#include <SDL.h>
#include <iostream>

using namespace std;

#ifndef WAVPATH
#define WAVPATH "."
#endif

int main(int argc, char *argv[]) {
    AudioSystem::setDebugLevel(5);
    int samplerate = 0;
    if (argc > 2 && atoi(argv[2]))
        samplerate = atoi(argv[2]);
    AudioSystem as(AF_Default, samplerate);
    unsigned track;
    ASSample *s;
    if (argc > 1)
        s = as.loadSample(argv[1]);
    else
        s = as.loadSample(WAVPATH "/The_Stranglers_-_Golden_Brown.flac");
    cerr << "Playing for 5 seconds then stopping for 2 seconds, then resuming" << endl;
    as.mixSample(s);
    SDL_Delay(5000);
    as.stopSample(s);
    SDL_Delay(2000);
    cerr << "Playing for 5 seconds, then pausing for 2 seconds, then resuming" << endl;
    as.mixSample(s);
    SDL_Delay(5000);
    as.pauseSample(s);
    SDL_Delay(2000);
    cerr << "Playing and starting a second mix in 5 seconds at 80% volume" << endl;
    as.unpauseSample(s);
    SDL_Delay(5000);
    as.mixSample(s, 0.8, &track);
    cerr << "Playing second mix in track" << track << ". Setting volume to 50% in 5 seconds" << endl;
    SDL_Delay(5000);
    as.setVolume(s, 0.5, track);
    cerr << "Halved. Stopping second mix in 5 seconds" << endl;
    SDL_Delay(5000);
    as.stopSample(s, track);
    cerr << "Playing out" << endl;
    SDL_Delay(1*60*1000);
    return 0;
}

