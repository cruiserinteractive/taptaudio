// $Id$ $URL$ */

%module taptaudio
%{
#include "taptaudio.h"
%}

%include taptaudio.h

%ignore AudioSystem::stopRec(std::string const &,bool,bool);
%ignore AudioSystem::loadSample(std::string const &);
%ignore AudioSystem::loadRawSample(std::string const &,void*,unsigned long);
