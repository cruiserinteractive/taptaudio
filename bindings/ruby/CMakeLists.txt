
########### install files ###############




#original Makefile.am contents follow:

### $Id: Makefile.am 290 2006-07-05 09:39:55Z tapted $ $URL: svn+ssh://pc-g33-9/var/svn/pub/taptaudio/trunk/bindings/ruby/Makefile.am $
#
#include $(top_srcdir)/config/Make-rules
#
#SUBDIRS = .
#
#AM_LDFLAGS = -no-fast-install -no-undefined -module -avoid-version
#
#if RUBY
#sprblib_LTLIBRARIES = libtaptaudio_ruby.la
#libtaptaudio_ruby_la_CXXFLAGS = $(SWIG_RUBY_CPPFLAGS) $(SWIG_RUBY_CFLAGS) -DSWIG_MUST_PEEK
#libtaptaudio_ruby_la_LDFLAGS = $(SWIG_RUBY_LFLAGS)
#libtaptaudio_ruby_la_SOURCES = taptaudio_ruby.cpp
#libtaptaudio_ruby_la_LIBADD = $(top_builddir)/src/libtaptaudio.la
#
#install-data-hook:
#	test -e $(sprblibdir)/taptaudio.@LIBRARY_EXTENSION@ \
#	    || $(LN_S) $(sprblibdir)/libtaptaudio_ruby.@LIBRARY_EXTENSION@ $(sprblibdir)/taptaudio.@LIBRARY_EXTENSION@ \
#	    || true
#else
#install-data-hook:
#endif
#
